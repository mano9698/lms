<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::get('/', 'UI\UsersController@login');

Route::get('/register', 'UI\UsersController@register');

Route::get('/error', 'UI\UsersController@error');

Route::post('/add_users', 'UI\UsersController@add_users');

Route::post('/login', 'UI\UsersController@admin_login');

Route::get('/auth_login', 'UI\UsersController@auth_login');

Route::get('/teacher_logout', 'UI\UsersController@teacher_logout');

Route::get('/student_logout', 'UI\UsersController@student_logout');

Route::get('/affiliate_logout', 'UI\UsersController@affiliate_logout');

Route::get('/user_logout', 'UI\UsersController@user_logout');

// Route::post('/video', 'UI\UploadsController@store');

Route::get('/admin_logout', 'UI\UsersController@admin_logout');


    Route::group(['prefix' => '/admin'], function () {

        Route::get('/dashboard', 'UI\AdminController@dashboard');

        Route::get('/teachers_list', 'UI\AdminController@teachers_list');

        Route::get('/affiliate_list', 'UI\AdminController@affiliate_list');

        Route::get('/all_users_list', 'UI\AdminController@all_users_list');

        Route::get('/teachers_details/{id}', 'UI\AdminController@teachers_details');

        Route::post('/changeStatus', 'UI\AdminController@ChangeStatus');

        Route::post('/ChangeUserType', 'UI\AdminController@ChangeUserType');

        Route::get('/students_list', 'UI\AdminController@students_list');

        Route::get('/student_details/{id}', 'UI\AdminController@student_details');

        // Route::post('/delete_fees', 'UI\UsersController@delete_fees');

        // Route::post('/delete_tuition', 'UI\UsersController@delete_tuition');

        // Route::post('/delete_info', 'UI\UsersController@delete_info');


        Route::get('/add_teachers/{id}', 'UI\AdminController@add_teachers');

        Route::post('/store_teachers_list', 'UI\AdminController@store_teachers_list');

        Route::post('/delete_teachers_list', 'UI\AdminController@delete_teachers_list');

        Route::get('/delete_users_list/{id}', 'UI\AdminController@delete_users_list');

        Route::get('/inquiries_list', 'UI\AdminController@inquiries_list');

        Route::get('/partners_list', 'UI\AdminController@partners_list');

        Route::get('/classes_list', 'UI\AdminController@classes_list');
    });


    Route::group(['prefix' => '/user_type'], function () {

        Route::get('/list', 'UI\UserTypesController@user_types_list');

        Route::get('/add_user_types', 'UI\UserTypesController@add_user_types');

        Route::get('/edit_user_types/{id}', 'UI\UserTypesController@edit_user_types');

        Route::get('/view_user_types/{id}', 'UI\UserTypesController@view_user_types');

        Route::post('/store_user_type', 'UI\UserTypesController@store_user_type');

        Route::post('/store_permissions', 'UI\UserTypesController@store_permissions');

        Route::post('/update_permissions', 'UI\UserTypesController@update_permissions');

        Route::post('/update_affiliate_profile', 'UI\AffiliateController@update_affiliate_profile');

        Route::post('/getpermission/{id}', 'UI\UserTypesController@getpermission');

        Route::get('/delete_user_type/{id}', 'UI\UserTypesController@delete_user_type');

        Route::get('/add_permission/{id}', 'UI\UserTypesController@add_permission');

        Route::get('/delete_permission/{id}', 'UI\UserTypesController@delete_permission');
    });


    Route::group(['prefix' => '/affiliate'], function () {

        Route::get('/dashboard', 'UI\AffiliateController@dashboard');

        Route::get('/list', 'UI\AffiliateController@payment_collection_list');

        Route::get('/payment_transferred_list', 'UI\AffiliateController@payment_transferred_list');

        Route::get('/edit_profile', 'UI\AffiliateController@edit_profile');

        Route::post('/update_affiliate_profile', 'UI\AffiliateController@update_affiliate_profile');
    });


    Route::group(['prefix' => '/home_work'], function () {

        Route::get('/list', 'UI\HomeWorkController@home_work_list');

        Route::get('/add_home_work', 'UI\HomeWorkController@add_home_work');

        Route::get('/student_home_work_list', 'UI\HomeWorkController@student_home_work_list');

        Route::post('/store_home_work', 'UI\HomeWorkController@store_home_work');

        Route::get('/edit_home_work/{id}', 'UI\HomeWorkController@edit_home_work');

        Route::post('/update_home_work', 'UI\HomeWorkController@update_home_work');

        Route::get('/delete_home_work/{id}', 'UI\HomeWorkController@delete_home_work');
    });


    Route::group(['prefix' => '/fee_collection'], function () {

        Route::get('/list', 'UI\FeeController@fees_list');

        Route::get('/add_fees', 'UI\FeeController@add_fees');

        Route::post('/store_fees', 'UI\FeeController@store_fees');

        Route::get('/edit_fees/{id}', 'UI\FeeController@edit_fees');

        Route::post('/update_fees', 'UI\FeeController@update_fees');

        Route::get('/delete_fees/{id}', 'UI\FeeController@delete_fees');
    });



    Route::group(['prefix' => '/certificates'], function () {

        Route::get('/add_certificates', 'UI\CertificateController@add_certificates');

        Route::get('/certificates_list', 'UI\CertificateController@certificates_list');

        Route::get('/view_certificate/{id}', 'UI\CertificateController@view_certificate');

        Route::post('/store_certificates', 'UI\CertificateController@store_certificates');

        Route::get('/payment_transferred_list', 'UI\AffiliateController@payment_transferred_list');
    });


Route::group(['prefix' => '/teachers'], function () {

    Route::get('/dashboard', 'UI\TeachersController@dashboard');

    Route::get('/edit_profile', 'UI\UsersController@edit_profile');

    Route::post('/update_profile', 'UI\UsersController@update_profile');

    Route::get('/admin_logout', 'UI\AuthendicationController@admin_logout');

    Route::post('/delete_fees', 'UI\UsersController@delete_fees');

    Route::post('/delete_tuition', 'UI\UsersController@delete_tuition');

    Route::post('/delete_info', 'UI\UsersController@delete_info');

    Route::get('/students_list', 'UI\TeachersController@students_list');
});

Route::group(['prefix' => '/courses'], function () {

    Route::get('/list', 'UI\CoursesController@courses_list');

    Route::get('/add_courses', 'UI\CoursesController@add_courses');

    Route::get('/edit_courses/{id}', 'UI\CoursesController@edit_courses');

    Route::post('/store_course', 'UI\CoursesController@store_course');

    Route::post('/update_course', 'UI\CoursesController@update_course');

    Route::post('/delete_course', 'UI\CoursesController@delete_course');

    Route::get('/add_students/{id}', 'UI\CoursesController@add_students');

    Route::post('/store_students_list', 'UI\CoursesController@store_students_list');

    Route::post('/delete_students_list', 'UI\CoursesController@delete_students_list');

    Route::get('/students_list/{id}', 'UI\CoursesController@students_list');

});


Route::group(['prefix' => '/groups'], function () {

    Route::get('/list', 'UI\GroupsController@groups_list');

    Route::get('/add_group', 'UI\GroupsController@add_group');

    Route::get('/edit_group/{id}', 'UI\GroupsController@edit_group');

    Route::post('/store_groups', 'UI\GroupsController@store_groups');

    Route::post('/update_groups', 'UI\GroupsController@update_groups');

    Route::post('/delete_groups', 'UI\GroupsController@delete_groups');


    Route::get('/add_students/{id}', 'UI\GroupsController@add_students');

    Route::post('/store_students_list', 'UI\GroupsController@store_students_list');

    Route::post('/delete_students_list', 'UI\GroupsController@delete_students_list');

    Route::get('/students_list/{id}', 'UI\GroupsController@students_list');

});


Route::group(['prefix' => '/teachers/uploads'], function () {

    Route::get('/list', 'UI\UploadsController@uploads_list');

    Route::get('/add_uploads', 'UI\UploadsController@add_uploads');

    Route::get('/edit_uploads/{id}', 'UI\UploadsController@edit_uploads');

    Route::post('/store_uploads', 'UI\UploadsController@store_uploads');

    Route::post('/update_uploads', 'UI\UploadsController@update_uploads');

    Route::post('/delete_uploads', 'UI\UploadsController@delete_uploads');


    Route::get('/add_students/{id}', 'UI\UploadsController@add_students');

    Route::post('/store_students_list', 'UI\UploadsController@store_students_list');

    Route::post('/delete_students_list', 'UI\UploadsController@delete_students_list');

    Route::get('/students_list/{id}', 'UI\UploadsController@students_list');

    Route::get('/add_groups/{id}', 'UI\UploadsController@add_groups');

    Route::post('/update_group_of_students', 'UI\UploadsController@update_group_of_students');

});



Route::group(['prefix' => '/teachers/conference'], function () {

    Route::get('/list', 'UI\ConferenceController@conference_list');

    Route::get('/add_conference', 'UI\ConferenceController@add_conference');

    Route::get('/edit_conference/{id}', 'UI\ConferenceController@edit_conference');

    Route::post('/store_conference', 'UI\ConferenceController@store_conference');

    Route::post('/update_conference', 'UI\ConferenceController@update_conference');

    Route::post('/delete_conference', 'UI\ConferenceController@delete_conference');


    Route::get('/add_students/{id}', 'UI\ConferenceController@add_students');

    Route::post('/store_students_list', 'UI\ConferenceController@store_students_list');

    Route::post('/delete_students_list', 'UI\ConferenceController@delete_students_list');

    Route::get('/students_list/{id}', 'UI\ConferenceController@students_list');

    Route::get('/add_groups/{id}', 'UI\ConferenceController@add_groups');

    Route::post('/update_group_of_students', 'UI\ConferenceController@update_group_of_students');


});




Route::group(['prefix' => '/forum'], function () {

    Route::get('/list', 'UI\ForumController@forum_list');

    Route::get('/add_forum', 'UI\ForumController@add_forum');

    Route::post('/store_questions', 'UI\ForumController@store_questions');

    Route::get('/edit_forum/{id}', 'UI\ForumController@edit_forum');

    Route::get('/delete_forum/{id}', 'UI\ForumController@delete_forum');

    Route::post('/update_questions', 'UI\ForumController@update_questions');

    Route::post('/delete_fees', 'UI\UsersController@delete_fees');

    Route::post('/delete_tuition', 'UI\UsersController@delete_tuition');

    Route::post('/delete_info', 'UI\UsersController@delete_info');

    Route::get('/delete_comments/{id}', 'UI\ForumController@delete_comments');

    Route::get('/students_list', 'UI\TeachersController@students_list');

    Route::get('/comments_list/{id}', 'UI\ForumController@comments_list');
});



Route::group(['prefix' => '/payments/collection'], function () {

    Route::get('/list', 'UI\PaymentsController@payment_collection_list');

    Route::get('/add_payment_collection', 'UI\PaymentsController@add_payment_collection');

    Route::post('/store_payment_collection', 'UI\PaymentsController@store_payment_collection');

    Route::get('/edit_payment_collection/{id}', 'UI\PaymentsController@edit_payment_collection');

    Route::post('/update_payment_collection', 'UI\PaymentsController@update_payment_collection');

    Route::get('/delete_payment_collection/{id}', 'UI\PaymentsController@delete_payment_collection');
});

Route::group(['prefix' => '/payments/transferred'], function () {

    Route::get('/list', 'UI\PaymentsController@payment_transferred_list');

    Route::get('/add_payment_transferred', 'UI\PaymentsController@add_payment_transferred');

    Route::post('/store_payment_transferred', 'UI\PaymentsController@store_payment_transferred');

    Route::get('/edit_payment_transferred/{id}', 'UI\PaymentsController@edit_payment_transferred');

    Route::post('/update_payment_transferred', 'UI\PaymentsController@update_payment_transferred');

    Route::get('/delete_payment_transferred/{id}', 'UI\PaymentsController@delete_payment_transferred');
});



// Students
Route::group(['prefix' => '/students'], function () {

    Route::get('/dashboard', 'UI\Students\StudentsController@dashboard');

    Route::get('/course_list', 'UI\Students\StudentsController@course_list');

    Route::get('/videos_list', 'UI\Students\StudentsController@videos_list');

    Route::get('/files_list', 'UI\Students\StudentsController@files_list');

    Route::get('/conference_list', 'UI\Students\StudentsController@conference_list');

    Route::post('/update_student_profile', 'UI\Students\StudentsController@update_student_profile');

    Route::get('/edit_profile', 'UI\Students\StudentsController@edit_profile');

    Route::post('/file_download', 'UI\Students\StudentsController@file_download');

    Route::get('/view_videos/{id}', 'UI\Students\StudentsController@view_videos');

    Route::get('/groups_list', 'UI\Students\StudentsController@groups_list');

    Route::get('/students_list/{id}', 'UI\Students\StudentsController@students_list');
});



Route::group(['prefix' => '/testimonials'], function () {

    Route::get('/list', 'UI\TestimonialController@testimonials_list');

    Route::get('/add_testimonials', 'UI\TestimonialController@add_testimonials');

    Route::get('/edit_testimonials/{id}', 'UI\TestimonialController@edit_testimonials');

    Route::post('/store_testimonials', 'UI\TestimonialController@store_testimonials');

    Route::post('/update_testimonials', 'UI\TestimonialController@update_testimonials');

    Route::get('/delete_testimonials/{id}', 'UI\TestimonialController@delete_testimonials');

});


Route::group(['prefix' => '/sample_videos'], function () {

    Route::get('/list', 'UI\VideosController@videos_list');

    Route::get('/add_sample_videos', 'UI\VideosController@add_sample_videos');

    Route::get('/edit_sample_videos/{id}', 'UI\VideosController@edit_sample_videos');

    Route::post('/store_sample_videos', 'UI\VideosController@store_sample_videos');

    Route::post('/update_sample_videos', 'UI\VideosController@update_sample_videos');

    Route::get('/delete_sample_videos/{id}', 'UI\VideosController@delete_sample_videos');

});



// Auth0

Route::get('/auth0/callback', [Auth0Controller::class, 'callback'])->name('auth0-callback');

// Route::get('/auth_login', 'Auth\Auth0IndexController@login');

// Route::get('/login', [Auth0IndexController::class, 'login'])->name('login');
// Route::get('/logout', [Auth0IndexController::class, 'logout'])->name('logout');
// Route::get('/profile', [Auth0IndexController::class, 'profile'])->name('profile');
