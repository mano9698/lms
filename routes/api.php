<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::group(['prefix'=>'v1'], function () {
    Route::post('/store_courses', 'Api\CourseController@store_courses');

    Route::post('/update_courses/{id}', 'Api\CourseController@update_courses');
});


Route::get('/course_list/{id}', 'Api\CourseController@course_list');

Route::get('/course_details/{id}', 'Api\CourseController@course_details');

Route::post('/store_classes', 'Api\CourseController@store_classes');



Route::post('/store_request_call_back', 'Api\CourseController@store_request_call_back');

Route::post('/store_partners', 'Api\CourseController@store_partners');

Route::post('/store_inquiry', 'Api\CourseController@store_inquiry');


Route::get('/forum_list/{id}', 'Api\ForumController@forum_list');

Route::get('/forum_details/{id}', 'Api\ForumController@forum_details');

Route::post('/add_comments', 'Api\ForumController@add_comments');

Route::post('/add_likes', 'Api\ForumController@add_likes');

Route::get('/comments_list/{id}', 'Api\ForumController@comments_list');

Route::get('/likes_list/{id}', 'Api\ForumController@likes_list');

Route::get('/teachers_list', 'Api\TeachersController@teachers_list');

Route::get('/teachers_details/{id}', 'Api\TeachersController@teachers_details');

Route::get('/testimonials_details/{id}', 'Api\TeachersController@testimonials_details');

Route::get('/course_list_basedon_teachers/{id}', 'Api\TeachersController@course_list_basedon_teachers');

Route::get('/videos_list/{id}', 'Api\TeachersController@videos_list');

Route::get('/search_course/{name}', 'Api\CourseController@search_course');
