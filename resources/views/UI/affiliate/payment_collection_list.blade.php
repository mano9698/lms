@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Dashboard</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Affiliate Partner Payment Collection</li>
            </ul>
        </div>
        <!-- Card -->
        <div class="row">
            @if(session('message'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! session('message') !!}</li>
                </ul>
            </div>
        @endif
            <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Email ID</th>
                      <th>Contact Number</th>
                      <th>Course Name</th>
                      <th>Amount collected</th>
                      <th>Collection Mode</th>
                      <th>Collection Ref Number</th>
                      <th>Collection Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if($PaymentCollection)
                    @foreach($PaymentCollection as $Payment)
                    <tr>
                      <th>{{$Payment->id}}</th>
                      <td><strong>{{ $Payment->first_name }}</strong></td>
                      <td>{{ $Payment->last_name }}</td>
                      <td>{{ $Payment->email }}</td>
                      <td>{{ $Payment->contact }}</td>
<td>{{ $Payment->course_name }}</td>
                        <td>{{ $Payment->amount_collected }}</td>
                      <td>{{ $Payment->collection_mode }}</td>
                      <td>{{ $Payment->collection_ref_no }}</td>
                      <td>{{ $Payment->collection_date }}</td>
                    </tr>
                    @endforeach
                    @else

                    @endif
                  </tbody>
                </table>

        </div>
        <!-- Card END -->

    </div>
</main>
@endsection
