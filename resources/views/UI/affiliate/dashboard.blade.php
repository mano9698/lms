@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Dashboard</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Affiliate Partner Dashboard</li>
            </ul>
        </div>
        <!-- Card -->
        <div class="row">
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <div class="widget-card widget-bg2">
                    <div class="wc-item">
                        <h4 class="wc-title">
                             My Courses
                        </h4>
                        <span class="wc-des">
                            All Customs Value<br>All Customs Value<br>All Customs Value<br>All Customs Value<br>All Customs Value
                        </span>
                        <span class="wc-stats">
                            <span class="counter">10</span>
                        </span>

                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <div class="widget-card widget-bg3">
                    <div class="wc-item">
                        <h4 class="wc-title">
                            My Groups
                        </h4>
                        <span class="wc-des">
                            Fresh Order Amount<br>Fresh Order Amount<br>Fresh Order Amount
                        </span>
                        <span class="wc-stats counter">
                            4
                        </span>
                    </div>
                </div>
            </div>

        </div>
        <!-- Card END -->

    </div>
</main>
@endsection
