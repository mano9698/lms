@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Dashboard</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Affiliate Partner Payment Collection</li>
            </ul>
        </div>
        <!-- Card -->
        <div class="row">
            @if(session('message'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! session('message') !!}</li>
                </ul>
            </div>
        @endif
            <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Commission Month</th>
                      <th>Total Collection</th>
                      <th>Commission Due Amount</th>
                      <th>Commission Paid Amount</th>
                      <th>Paid Date</th>
                      <th>Payment Mode</th>
                      <th>Payment Ref Number</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($PaymentTransferred)
                      @foreach($PaymentTransferred as $Payment)
                    <tr>
                      <th>{{ $Payment->id }}</th>
                      <td>{{ $Payment->commision_month }}</td>
                      <td>{{ $Payment->total_collection }}</td>
                      <td>{{ $Payment->commission_due_date }}</td>
                      <td>{{ $Payment->commission_paid_amount }}</td>
<td>{{ $Payment->paid_date }}</td>
                      <td>{{ $Payment->payment_mode }}</td>
                      <td>{{ $Payment->payment_ref_no }}</td>

                    </tr>
                    @endforeach
                    @else

                    @endif
                  </tbody>
                </table>

        </div>
        <!-- Card END -->

    </div>
</main>
@endsection
