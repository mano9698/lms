@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">User Profile</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/students/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>User Profile</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>User Profile update</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/affiliate/update_affiliate_profile" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="">
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <h3>Personal Details</h3>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Profile Picture</label>
                                    <div class="col-sm-7">
                                    @if(isset($AffiliateProfile->profile_pic))

                                        <input class="form-control" type="file" id="student_profile_pic" name="profile_pic" value="{{$AffiliateProfile->profile_pic}}">

                                        <div id="" class="image-previewer" data-cropzee="student_profile_pic"></div>
                                    @else
                                        <input class="form-control" type="file" id="student_profile_pic" name="profile_pic" value="" required>

                                        <div id="" class="image-previewer" data-cropzee="student_profile_pic"></div>
                                    @endif
                                    </div>
                                </div>
                                    <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Gender</label>
                                    <div class="col-sm-4">
                                    @if(isset($AffiliateProfile->gender))

                                        <input class="form-control" type="text" name="gender" value="{{$AffiliateProfile->gender}}">
                                    @else
                                        <input class="form-control" type="text" name="gender" value="" required>
                                    @endif

                                    </div>
                                    <label class="col-sm-2 col-form-label">Qualification</label>
                                    <div class="col-sm-4">
                                    @if(isset($AffiliateProfile->qualification))

                                        <input class="form-control" type="text" name="qualification" value="{{$AffiliateProfile->qualification}}">
                                    @else
                                        <input class="form-control" type="text" name="qualification" value="" required>
                                    @endif


                                    </div>
                                </div>





                            </div>
                            <div class="">
                                <div class="">
                                    <div class="row">

                                        <div class="col-sm-7">
                                            <button type="submit" class="btn">Save changes</button>
                                            <button type="reset" class="btn-secondry">Cancel</button>
                                        </div><div class="col-sm-2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
