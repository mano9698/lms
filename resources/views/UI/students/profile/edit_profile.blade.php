@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">User Profile</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/students/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>User Profile</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>User Profile update</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/students/update_student_profile" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="">
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <h3>Personal Details</h3>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Profile Picture</label>
                                    <div class="col-sm-7">
                                    @if(isset($StudentsProfile->profile_pic))

                                        <input class="form-control" type="file" id="student_profile_pic" name="profile_pic" value="{{$StudentsProfile->profile_pic}}">

                                        <div id="" class="image-previewer" data-cropzee="student_profile_pic"></div>
                                    @else
                                        <input class="form-control" type="file" id="student_profile_pic" name="profile_pic" value="" required>

                                        <div id="" class="image-previewer" data-cropzee="student_profile_pic"></div>
                                    @endif
                                    </div>
                                </div>
                                    <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Gender</label>
                                    <div class="col-sm-4">
                                    @if(isset($StudentsProfile->gender))

                                        <input class="form-control" type="text" name="gender" value="{{$StudentsProfile->gender}}">
                                    @else
                                        <input class="form-control" type="text" name="gender" value="" required>
                                    @endif

                                    </div>
                                    <label class="col-sm-2 col-form-label">Date of Birth</label>
                                    <div class="col-sm-4">
                                    @if(isset($StudentsProfile->dob))

                                        <input class="form-control" type="date" name="dob" value="{{$StudentsProfile->dob}}">
                                    @else
                                        <input class="form-control" type="date" name="dob" value="" required>
                                    @endif


                                    </div>
                                </div>
                                    <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Age in Years</label>
                                    <div class="col-sm-4">
                                    @if(isset($StudentsProfile->age))

                                        <input class="form-control" type="text" name="age" value="{{$StudentsProfile->age}}">
                                    @else
                                        <input class="form-control" type="text" name="age" value="" required>
                                    @endif


                                    </div>
                                    <label class="col-sm-2 col-form-label">Grade</label>
                                    <div class="col-sm-4">
                                    @if(isset($StudentsProfile->grade))

                                        <input class="form-control" type="text" name="grade" value="{{$StudentsProfile->grade}}">
                                    @else
                                        <input class="form-control" type="text" name="grade" value="" required>
                                    @endif


                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">School/College Name</label>
                                    <div class="col-sm-4">
                                    @if(isset($StudentsProfile->school_clg_name))

                                        <input class="form-control" type="text" name="school_clg_name" value="{{$StudentsProfile->school_clg_name}}">
                                    @else
                                        <input class="form-control" type="text" name="school_clg_name" value="">
                                    @endif


                                    </div>
                                    <label class="col-sm-2 col-form-label">School Board</label>
                                    <div class="col-sm-4">
                                    @if(isset($StudentsProfile->school_board))

                                        <input class="form-control" type="text" name="school_board" value="{{$StudentsProfile->school_board}}">
                                    @else
                                        <input class="form-control" type="text" name="school_board" value="">
                                    @endif


                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Language known</label>
                                    <div class="col-sm-4">
                                        @if(isset($StudentsProfile->language_known))

                                        <input class="form-control" type="text" name="language_known" value="{{$StudentsProfile->language_known}}">

                                    @else
                                        <input class="form-control" type="text" name="language_known" value="">
                                    @endif


                                    </div>
                                    <label class="col-sm-2 col-form-label">Favorite Subject</label>
                                    <div class="col-sm-4">
                                        @if(isset($StudentsProfile->fav_subject))

                                        <input class="form-control" type="text" name="fav_subject" value="{{$StudentsProfile->fav_subject}}">
                                    @else
                                    <input class="form-control" type="text" name="fav_subject" value="">
                                    @endif



                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Favorite Sport</label>
                                    <div class="col-sm-4">
                                        @if(isset($StudentsProfile->fav_sport))

                                        <input class="form-control" type="text" name="fav_sport" value="{{$StudentsProfile->fav_sport}}">
                                    @else
                                    <input class="form-control" type="text" name="fav_sport" value="">
                                    @endif


                                    </div>
                                    <label class="col-sm-2 col-form-label">Favorite Musical Instrument</label>
                                    <div class="col-sm-4">
                                    @if(isset($StudentsProfile->fav_music))

                                        <input class="form-control" type="text" name="fav_music" value="{{$StudentsProfile->fav_music}}">
                                    @else
                                    <input class="form-control" type="text" name="fav_music" value="">
                                    @endif


                                    </div>
                                </div>

                            </div>
                            <div class="">
                                <div class="">
                                    <div class="row">

                                        <div class="col-sm-7">
                                            <button type="submit" class="btn">Save changes</button>
                                            <button type="reset" class="btn-secondry">Cancel</button>
                                        </div><div class="col-sm-2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
