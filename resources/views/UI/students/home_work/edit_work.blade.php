@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Home Work</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Edit Home Work</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Edit Home Work</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/home_work/update_home_work" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="col-form-label">File Name</label>
                                    <div>
                                        <input class="form-control" type="text" name="file_name" value="{{ $HomeWork->file_name }}">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Edit Home Work</label>
                                    <div>
                                        <input type="hidden" name="id" value="{{ $HomeWork->id }}">
                                        <input class="form-control" type="file" name="file" value="{{ $HomeWork->file }}">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Select Teacher</label>
                                    <div>
                                        <select name="teacher_id">
                                            @foreach($Users as $User)
                                                <option value="{{ $User->id }}" @if($User->id == $HomeWork->teacher_id) selected @endif>{{ $User->first_name }} {{ $User->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                <div class="col-12">
                                    <!--<button type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Item</button>-->
                                    <button type="submit" class="btn">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
