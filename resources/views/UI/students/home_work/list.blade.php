@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Home Work</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Home Work List</li>
            </ul>
        </div>
        <!-- Card -->

        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Home Work</h4>
                    </div>
                    <div class="email-wrapper">

                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/>
                                </div>
<div class="dropdown all-msg-toolbar">
<span class="btn btn-info-icon" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></span>
                                    <ul class="dropdown-menu">
                                        <li><a href="#"><i class="fa fa-arrow-up"></i> Activate</a></li>
                                        <li><a href="#"><i class="fa fa-arrow-down"></i> Deactivate</a></li>
<li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                    </ul>

                                </div>
                                <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                            @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif

                            <table class="table public-user-block block">
                  <thead>
                    <tr>
                        <th>Name</th>
                      <th>File Name</th>
                      <th>Download</th>
                        <th width="15%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($HomeWork)
                      @foreach($HomeWork as $Work)
                      <tr>
                        <td><strong>{{ $Work->first_name }} {{ $Work->last_name }}</strong></td>
                        <td><strong>{{ $Work->file_name }}</strong></td>
                      <td><a href="/UI/home_work/{{ $Work->file }}" download=""><i class="fa fa-download"></i></a></td>
<td><ul class="mailbox-toolbar" style="position: inherit;">

                @if(Auth::guard('super_admin'))

                                       <li data-toggle="tooltip" title="Edit">
                                             <a href="/home_work/edit_home_work/{{ $Work->id }}"><i class="fa fa-edit"></i></a></li>

                                            <li data-toggle="tooltip" title="Delete"><a href="/home_work/delete_home_work/{{$Work->id}}" onclick="return confirm(' Are you sure. You want to delete?');"><i class="fa fa-trash-o"></i></a></li>

                    @elseif(Auth::guard('teacher'))
                        @if($Users->user_main_type == 2)

                        <li data-toggle="tooltip" title="Edit">
                            <a href="/home_work/edit_home_work/{{ $Work->id }}"><i class="fa fa-edit"></i></a></li>

                        <li data-toggle="tooltip" title="Delete"><a href="/home_work/delete_home_work/{{$Work->id}}" onclick="return confirm(' Are you sure. You want to delete?');"><i class="fa fa-trash-o"></i></a></li>
                        @else

                        @endif
                    @endif
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td>No Data Found...</td>
                    </tr>
                    @endif
                  </tbody>
                </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
