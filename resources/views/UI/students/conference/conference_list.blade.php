@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Online Class Link </h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/students/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Online Class Link </li>
            </ul>
        </div>
        <!-- Card -->

        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>View the list of Conference</h4> --}}
                    </div>
                    <div class="email-wrapper">

                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                {{-- <div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div> --}}
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/>
                                </div>

                                {{-- {{-- <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div> --}}
                            </div>
                            <div class="mail-box-list">
                                <div class="table-responsive">
                                <table class="table public-user-block block">
                                    <thead>
                                      <tr>
                                        <th><div class="custom-control custom-checkbox checkbox-st1">
                                                              <input type="checkbox" class="custom-control-input" id="check">
                                                              <label class="custom-control-label" for="check"></label>
                                                          </div>
                                                      </th>
                                        <th>Name</th>
                                        <th>Join</th>
                                        <th>Duration (Minutes)</th>
                                        <th>Date</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($Conference as $Conferences)
                                        <tr>
                                            <th scope="row"><div class="custom-control custom-checkbox checkbox-st1">
                                                                  <input type="checkbox" class="custom-control-input" id="check2">
                                                                  <label class="custom-control-label" for="check2"></label>
                                                              </div></th>
                                            <td><strong>{{$Conferences->name}}</strong></td>
                                            <td><a href="{{$Conferences->meeting_link}}" style="color:#000!important"><i class="fa fa-play" target="_blank"></i></a></td>
                                            <td>{{$Conferences->duration}}</td>
                                            <td>{{date('m/d/Y', strtotime($Conferences->created_at))}}
                                            </td>

                                          </tr>
                                      @endforeach
                                    </tbody>
                                  </table>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
