@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Groups</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/students/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Groups</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>{{$Groups->title}}</h4>
                    </div>
                    <div class="widget-inner">
                        <div class="card-courses-list admin-courses">
                        <div class="card-courses-full-dec">
                                <div class="card-courses-title">
                                    <h4>List Of the Students</h4>
                                </div>
                                <div class="card-courses-list-bx">
                                    <ul class="card-courses-view">
                                        <li class="card-courses-user">
                                            {{-- <div class="card-courses-user-pic">
                                                <img src="../assets/images/testimonials/pic3.jpg" alt="">
                                            </div> --}}
                                            <div class="card-courses-user-info">
                                                <h5>Teacher</h5>
                                                <h4>{{$Groups->first_name}} {{$Groups->last_name}}</h4>
                                            </div>
                                        </li>

                                        <li class="card-courses-review">
                                            <h5>{{count($GroupStudentsList)}} Students</h5>
                                            {{-- <ul class="cours-star">
                                                <li class="active"><i class="fa fa-star"></i></li>
                                                <li class="active"><i class="fa fa-star"></i></li>
                                                <li class="active"><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                                <li><i class="fa fa-star"></i></li>
                                            </ul> --}}
                                        </li>


                                    </ul>
                                </div>
                            <div class="mail-box-list">
                                @if($GroupStudentsList)
                                    @foreach($GroupStudentsList as $Students)
                                    <div class="mail-list-info">
                                        <div class="checkbox-list">
                                            {{-- <div class="custom-control custom-checkbox checkbox-st1">
                                                <input type="checkbox" class="custom-control-input" id="check2">
                                                <label class="custom-control-label" for="check2"></label>
                                            </div> --}}
                                        </div>

                                        <div class="mail-list-title-info">
                                            <p>{{$Students->first_name}} {{$Students->last_name}}</p>
                                        </div>
                                    </div>
                                    @endforeach
                                @else
                                        <h4>No Students Found</h4>
                                @endif
                            </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
