@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Groups List</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/students/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Groups List</li>
            </ul>
        </div>
        <!-- Card -->

        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>My Group</h4> --}}
                    </div>
                    <div class="email-wrapper">

                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <div class="check-all">
                                    {{-- <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div> --}}
                                </div>
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/>
                                </div>
                            </div>
                            @if($Groups)
                            @foreach($Groups as $Group)
                            <div class="mail-box-list">
                                <div class="mail-list-info">
                                    <div class="checkbox-list">
                                        {{-- <div class="custom-control custom-checkbox checkbox-st1">
                                            <input type="checkbox" class="custom-control-input" id="check2">
                                            <label class="custom-control-label" for="check2"></label>
                                        </div> --}}
                                    </div>
                                    <div class="mail-rateing">
                                        <span><i class="fa fa-star-o"></i></span>
                                    </div>
                                    <div class="mail-list-title-info">
                                        <p><a href="/students/students_list/{{$Group->id}}" target="_blank">{{$Group->title}}</a></p>
                                    </div>
                                    {{-- <div class="mail-list-time">
                                        <span>10:59 AM</span>
                                    </div> --}}
                                    {{-- <ul class="mailbox-toolbar">
                                        <a href="/groups/students_list/{{$Group->id}}">
                                            <li data-toggle="tooltip" title="Students List"><i class="fa fa-list"></i></li>
                                        </a>

                                    </ul> --}}
                                </div>
                            </div>
                            @endforeach
                            @else

                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
