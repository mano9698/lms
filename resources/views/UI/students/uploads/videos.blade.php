@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">File/Videos</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/students/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>File/Videos</li>
            </ul>
        </div>
        <!-- Card -->

        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>View the Videos</h4> --}}
                    </div>
                    <div class="email-wrapper">

                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                {{-- <div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div> --}}
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/>
                                </div>

                                {{-- {{-- <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div> --}}
                            </div>
                            <div class="mail-box-list">
                                @if($Videos)
                                    @foreach($Videos as $Video)
                                    <div class="mail-list-info">
                                        <div class="checkbox-list">

                                            <div class="custom-control custom-checkbox checkbox-st1">
                                                <input type="checkbox" class="custom-control-input" id="check4">
                                                <label class="custom-control-label" for="check4"></label>
                                            </div>
                                        </div>
                                        @if($Video->file_type == 1)
                                            <div class="mail-rateing">
                                                <span><i class="fa fa-file-text-o"></i></span>
                                            </div>

                                            <div class="mail-list-title-info">
                                                <p><a class="AddFileDownloads" href="/UI/uploads/{{$Video->files}}" target="_blank" data-id="{{$Video->id}}">{{$Video->file_name}}</a></p>
                                            </div>
                                        @elseif($Video->file_type == 2)
                                            <div class="mail-rateing">
                                                <span><i class="fa fa-video-camera"></i></span>
                                            </div>

                                            <div class="mail-list-title-info">
                                                <p><a href="/students/view_videos/{{$Video->id}}" target="_blank">{{$Video->file_name}}</a></p>
                                            </div>
                                        @endif

                                        {{-- <div class="mail-list-time">
                                            <span>10:59 AM</span>
                                        </div> --}}
                                        {{-- <ul class="mailbox-toolbar">
                                            <li data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></li>
                                            <li data-toggle="tooltip" title="View"><i class="fa fa-search"></i></li>
                                        </ul> --}}
                                    </div>
                                    @endforeach
                                @else
                                    <h5>No Videos Found.</h5>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection

