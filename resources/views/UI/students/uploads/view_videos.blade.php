
@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Dashboard</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Teacher Dashboard</li>
            </ul>
        </div>
        <!-- Card -->
        <div class="row">
            <!--<div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <div class="widget-card widget-bg1">
                    <div class="wc-item">
                        <h4 class="wc-title">
                            Courses in progess
                        </h4>
                        <span class="wc-des">
                            All Customs Value
                        </span>
                        <span class="wc-stats">
                            <span class="counter">14</span>
                        </span>
                        <div class="progress wc-progress">
                            <div class="progress-bar" role="progressbar" style="width: 8%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <span class="wc-progress-bx">
                            <span class="wc-change">
                                Courses Completed
                            </span>
                            <span class="wc-number ml-auto">
                                1%
                            </span>
                        </span>
                    </div>
                </div>
            </div>-->
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <a href="/courses/list"><div class="widget-card widget-bg2">
                    <div class="wc-item">
                        <h4 class="wc-title">
                             <i class="ti-book pr-2"></i> Total Courses
                        </h4>
                        <span class="wc-des">
                            No of Courses
                        </span>
                        <span class="wc-stats counter">
                            {{ count($Courses) }}
                        </span>

                    </div>
                </div></a>
            </div>
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <a href="/groups/list"><div class="widget-card widget-bg3">
                    <div class="wc-item">
                        <h4 class="wc-title">
                            <i class="ti-world pr-2"></i> Total Groups
                        </h4>
                        <span class="wc-des">
                            Depending Upon the courses
                        </span>
                        <span class="wc-stats counter">
                            {{ $Groups }}
                        </span>
                    </div>
                </div></a>
            </div>
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <a href="/teachers/uploads/list"><div class="widget-card widget-bg4">
                    <div class="wc-item">
                        <h4 class="wc-title">
                            <i class="ti-view-grid pr-2"></i> Uploads
                        </h4>
                        <span class="wc-des">
                            No Of Uploads
                        </span>
                        <span class="wc-stats counter">
                            {{ $Uploads }}
                        </span>

                    </div>
                </div></a>
            </div>
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <a href="/teachers/conference/list"><div class="widget-card widget-bg1">
                    <div class="wc-item">
                        <h4 class="wc-title">
                             <i class="ti-camera pr-2"></i> Conference
                        </h4>
                        <span class="wc-des">
                            No of Conferences
                        </span>
                        <span class="wc-stats counter">
                            {{ $Conference }}
                        </span>

                    </div>
                </div></a>
            </div>
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <a href="/teachers/edit_profile"><div class="widget-card widget-bg6">
                    <div class="wc-item">
                        <h4 class="wc-title">
                             <i class="ti-user pr-2"></i> View Profile
                        </h4>
                        <span class="wc-des">
                            Edit your Profile
                        </span>
                        <span class="wc-stats">

                        </span>

                    </div>
                </div></a>
            </div>
        </div>
        <!-- Card END -->

        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>My Courses</h4>
                    </div>
                    <div class="widget-inner">
                        <div class="row">
                            @if($Courses)
                        @foreach($Courses as $Course)
                        <div class="col-lg-4 col-4">
                            <img class="width100" src="/UI/courses/{{ $Course->course_image }}" alt=""/>
                            <div class="card-courses-title">
                                    <h5 class="text-center pt-3">{{ $Course->title }}</h5>
                                </div>
                        </div>

                        @endforeach
                        @endif
                        </div>




                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
