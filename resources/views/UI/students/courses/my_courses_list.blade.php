@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Courses / Events</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/students/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Courses / Events</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>Your Courses</h4> --}}
                    </div>
                    <div class="widget-inner">
                        <div class="table-responsive">
                            <table class="table public-user-block block">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th width="5%">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if($Courses)
                                    @foreach($Courses as $Course)
                                    <tr>
                                        <td><strong>{{$Course->title}}</strong></td>
                                        <td>
                                            <ul class="mailbox-toolbar">
                                                <a href="/courses/students_list/{{$Course->id}}">
                                                <li data-toggle="tooltip" title="Students List"><i class="fa fa-list"></i></li>
                                                </a>
                                                <a href="/courses/add_students/{{$Course->id}}">
                                                <li data-toggle="tooltip" title="Add Students"><i class="fa fa-user-o"></i></li>
                                                </a>
                                                <a href="/courses/edit_courses/{{$Course->id}}">
                                                <li data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></li>
                                                </a>
                                                <a href="javascript:void(0);" class="delete_course" data-id="{{$Course->id}}">
                                                <li data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></li>
                                                </a>
                                            </ul>
                                        </td>
                                    </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td>No Courses Found...</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        {{-- @foreach($Courses as $Course)
                        <div class="card-courses-list admin-courses">
                            <div class="card-courses-media">
                                <img src="/UI/courses/{{$Course->course_image}}" alt=""/>
                            </div>
                            <div class="card-courses-full-dec">
                                <div class="card-courses-title">
                                    <h4>{{$Course->title}}</h4>
                                </div>
                                <div class="card-courses-list-bx">
                                    <ul class="card-courses-view">
                                        <li class="card-courses-user">
                                            <div class="card-courses-user-pic">
                                                <img src="../assets/images/testimonials/pic3.jpg" alt=""/>
                                            </div>
                                            <div class="card-courses-user-info">
                                                <h5>Teacher</h5>
                                                <h4>{{$Course->first_name}} {{$Course->last_name}}</h4>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row card-courses-dec">
                                    <div class="col-md-12">
                                        <h6 class="m-b10">Course Description</h6>
                                        {{$Course->course_description}}
                                    </div>
                                </div>

                            </div>
                        </div>
                        @endforeach --}}
                        {{-- <div class="modal fade review-bx-reply" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Ask Your Question</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <textarea class="form-control" placeholder="Type text"></textarea>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn mr-auto">Submit</button>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
