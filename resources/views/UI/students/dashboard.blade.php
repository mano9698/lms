@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Dashboard</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Students Dashboard</li>
            </ul>
        </div>
        <!-- Card -->
        <div class="row">
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <a href="/students/course_list"><div class="widget-card widget-bg2">
                    <div class="wc-item">
                        <h4 class="wc-title">
                             <i class="ti-book pr-2"></i> My Courses
                        </h4>
                        <span class="wc-des">
                            All Customs Value
                        </span>
                        <span class="wc-stats">
                            <span class="counter">{{ count($Courses) }}</span>
                        </span>

                    </div>
                </div></a>
            </div>
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <a href="/students/videos_list"><div class="widget-card widget-bg3">
                    <div class="wc-item">
                        <h4 class="wc-title">
                             <i class="ti-control-play pr-2"></i> Videos
                        </h4>
                        <span class="wc-des">
                            All Customs Value
                        </span>
                        <span class="wc-stats">
                            <span class="counter">{{ $Videos }}</span>
                        </span>

                    </div>
                </div></a>
            </div>
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <a href="/students/files_list"><div class="widget-card widget-bg4">
                    <div class="wc-item">
                        <h4 class="wc-title">
                            <i class="ti-notepad pr-2"></i>  Files
                        </h4>
                        <span class="wc-des">
                            All Customs Value
                        </span>
                        <span class="wc-stats">
                            <span class="counter">{{ $Files }}</span>
                        </span>

                    </div>
                </div></a>
            </div>
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <a href="/students/conference_list"><div class="widget-card widget-bg1">
                    <div class="wc-item">
                        <h4 class="wc-title">
                            <i class="ti-camera pr-2"></i>  Conference
                        </h4>
                        <span class="wc-des">
                            All Customs Value
                        </span>
                        <span class="wc-stats">
                            <span class="counter">{{ $Conference }}</span>
                        </span>

                    </div>
                </div></a>
            </div>
            <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                <a href="/students/edit_profile"><div class="widget-card widget-bg5">
                    <div class="wc-item">
                        <h4 class="wc-title">
                             <i class="ti-user pr-2"></i> Profile
                        </h4>
                        <span class="wc-des">
                            Edit Profile
                        </span>
                        <span class="wc-stats">

                        </span>

                    </div>
                </div></a>
            </div>
        </div>
        <!-- Card END -->
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>My Courses</h4>
                    </div>
                    <div class="widget-inner">
                        <div class="row">
                        @if($Courses)
                        @foreach($Courses as $Course)
                        <div class="col-lg-4 col-4">
                            <img class="width100" src="/UI/courses/{{ $Course->course_image }}" alt=""/>
                            <div class="card-courses-title">
                                    <h5 class="text-center pt-3">{{ $Course->title }}</h5>
                                </div>
                        </div>

                        @endforeach
                        @endif
                    </div>

                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
