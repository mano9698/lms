@if(Auth::guard('super_admin')->check())
<div class="ttr-sidebar">
    <div class="ttr-sidebar-wrapper content-scroll">
        <!-- side menu logo start -->
        <div class="ttr-sidebar-logo">
            <a href="#"><img alt="" src="{{URL::asset('UI/images/logo.png')}}" width="122" height="27"></a>
            <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
            </div> -->
            <div class="ttr-sidebar-toggle-button">
                <i class="ti-arrow-left"></i>
            </div>
        </div>
        <!-- side menu logo end -->
        <!-- sidebar menu start -->
        <nav class="ttr-sidebar-navi">
            <ul>


                <li>
                    <a href="/admin/dashboard" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-home"></i></span>
                        <span class="ttr-label">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-book"></i></span>
                        <span class="ttr-label">User Types</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                            <li>
                            <a href="/user_type/list" class="ttr-material-button"><span class="ttr-label">View User Types</span></a>
                        </li>
                    <li>
                            <a href="/user_type/add_user_types" class="ttr-material-button"><span class="ttr-label">Add User Types</span></a>
                        </li>
                    </ul>

                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-book"></i></span>
                        <span class="ttr-label">Users</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                            <li>
                                <a href="/admin/all_users_list" class="ttr-material-button"><span class="ttr-label">All Users</span></a>
                            </li>

                            {{-- <li>
                                <a href="/admin/teachers_list" class="ttr-material-button"><span class="ttr-label">View Teachers</span></a>
                            </li>
                            <li>
                                <a href="/admin/students_list" class="ttr-material-button"><span class="ttr-label">View Students</span></a>
                            </li>

                            <li>
                                <a href="/admin/affiliate_list" class="ttr-material-button"><span class="ttr-label">View Affiliates</span></a>
                            </li> --}}
                    </ul>

                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-book"></i></span>
                        <span class="ttr-label">Courses/Events</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                            <li>
                                <a href="/courses/list" class="ttr-material-button"><span class="ttr-label">View Courses/Events</span></a>
                            </li>
                            <li>
                                <a href="/courses/add_courses" class="ttr-material-button"><span class="ttr-label">Add Courses/Events</span></a>
                            </li>
                    </ul>

                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-world"></i></span>
                        <span class="ttr-label">Group</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                        <li>
                            <a href="/groups/list" class="ttr-material-button"><span class="ttr-label">View Group</span></a>
                        </li>
                            <li>
                            <a href="/groups/add_group" class="ttr-material-button"><span class="ttr-label">Add Group</span></a>
                        </li>
                    </ul>

                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-view-grid"></i></span>
                        <span class="ttr-label">Files/Videos</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/teachers/uploads/list" class="ttr-material-button"><span class="ttr-label">View Files/Videos</span></a>
                        </li>
                        <li>
                            <a href="/teachers/uploads/add_uploads" class="ttr-material-button"><span class="ttr-label">Add Files/Videos</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-camera"></i></span>
                        <span class="ttr-label">Online Class Link </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/teachers/conference/list" class="ttr-material-button"><span class="ttr-label">View Class Link</span></a>
                        </li>
                        <li>
                            <a href="/teachers/conference/add_conference" class="ttr-material-button"><span class="ttr-label">Add Class Link</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Leads </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/admin/inquiries_list" class="ttr-material-button"><span class="ttr-label">Inquiries List</span></a>
                        </li>
                        <li>
                            <a href="/admin/partners_list" class="ttr-material-button"><span class="ttr-label">Partners List</span></a>
                        </li>

                        <li>
                            <a href="/admin/classes_list" class="ttr-material-button"><span class="ttr-label">My Leads</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Forum </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/forum/list" class="ttr-material-button"><span class="ttr-label">Questions List</span></a>
                        </li>

                        <li>
                            <a href="/forum/add_forum" class="ttr-material-button"><span class="ttr-label">Add Topic</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-view-grid"></i></span>
                        <span class="ttr-label">Payment Tracker</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/payments/collection/list" class="ttr-material-button"><span class="ttr-label">Payment Collection</span></a>
                        </li>
                        <li>
                            <a href="/payments/collection/add_payment_collection" class="ttr-material-button"><span class="ttr-label">Add Payment Collection</span></a>
                        </li>
                        <li>
                            <a href="/payments/transferred/list" class="ttr-material-button"><span class="ttr-label">Payment Transferred</span></a>
                        </li>
                        <li>
                            <a href="/payments/transferred/add_payment_transferred" class="ttr-material-button"><span class="ttr-label">Add Payment Transferred</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Testimonials </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/testimonials/list" class="ttr-material-button"><span class="ttr-label">Testimonials List</span></a>
                        </li>

                        <li>
                            <a href="/testimonials/add_testimonials" class="ttr-material-button"><span class="ttr-label">Add Testimonials</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Sample Video </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/sample_videos/list" class="ttr-material-button"><span class="ttr-label">Sample Videos List</span></a>
                        </li>

                        <li>
                            <a href="/sample_videos/add_sample_videos" class="ttr-material-button"><span class="ttr-label">Add Sample Videos</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-receipt"></i></span>
                        <span class="ttr-label">Certificate</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                            <li>
                            <a href="/certificates/certificates_list" class="ttr-material-button"><span class="ttr-label">View Certificate</span></a>
                        </li>
                        <li>
                            <a href="/certificates/add_certificates" class="ttr-material-button"><span class="ttr-label">Add Certificate</span></a>
                        </li>
                    </ul>

                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-receipt"></i></span>
                        <span class="ttr-label">Home Work Submission</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                            <li>
                            <a href="/home_work/list" class="ttr-material-button"><span class="ttr-label">View Home Work</span></a>
                        </li>
                        <li>
                            <a href="/home_work/add_home_work" class="ttr-material-button"><span class="ttr-label">Add Home Work</span></a>
                        </li>
                    </ul>

                </li>

                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-receipt"></i></span>
                        <span class="ttr-label">Fee Collection</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                            <li>
                            <a href="/fee_collection/list" class="ttr-material-button"><span class="ttr-label">View Fee Collection</span></a>
                        </li>
                        <li>
                            <a href="/fee_collection/add_fees" class="ttr-material-button"><span class="ttr-label">Add Fee Collection</span></a>
                        </li>
                    </ul>

                </li>
                <li class="ttr-seperate"></li>

            </ul>
            <!-- sidebar menu end -->
        </nav>
        <!-- sidebar menu end -->
    </div>
</div>

@else
<?php
    $UserId = Session::get('TeacherId');
    $Users = DB::table('users')->where('id', $UserId)->first();

    $UserPermissions = DB::table('user_permissions')->where('user_type_id', $Users->user_type)->get();
?>
@if(Session::get('TeacherEmail') == $Users->email)
<div class="ttr-sidebar">
    <div class="ttr-sidebar-wrapper content-scroll">
        <!-- side menu logo start -->
        <div class="ttr-sidebar-logo">
            <a href="#"><img alt="" src="{{URL::asset('UI/images/logo.png')}}" width="122" height="27"></a>
            <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
            </div> -->
            <div class="ttr-sidebar-toggle-button">
                <i class="ti-arrow-left"></i>
            </div>
        </div>
        <!-- side menu logo end -->
        <!-- sidebar menu start -->
        <nav class="ttr-sidebar-navi">
            <ul>


                <li>
                    <a href="/teachers/dashboard" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-home"></i></span>
                        <span class="ttr-label">Dashboard</span>
                    </a>
                </li>

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 12)
                <li>
                    <a href="/teachers/edit_profile" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Teacher Profile </span>
                    </a>
                </li>
                @endif
                @endforeach

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 13)
                <li>
                    <a href="/students/edit_profile" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Student Profile</span>
                    </a>
                </li>
                @endif
                @endforeach

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 14)
                <li>
                    <a href="/affiliate/edit_profile" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Affiliate Profile</span>
                    </a>
                </li>
                @endif
                @endforeach

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 1)
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-book"></i></span>
                        <span class="ttr-label">Courses/Events</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>

                            @if($Permissions->view_only == 1)
                            <li>
                                <a href="/courses/list" class="ttr-material-button"><span class="ttr-label">View Courses/Events</span></a>
                            </li>
                            @endif

                            @if($Permissions->add_only == 2)
                            <li>
                                <a href="/courses/add_courses" class="ttr-material-button"><span class="ttr-label">Add Courses/Events</span></a>
                            </li>
                            @endif
                    </ul>

                </li>
                @endif
                @endforeach

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 2)
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-world"></i></span>
                        <span class="ttr-label">Group</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>

                            @if($Permissions->view_only == 1)
                            <li>
                                <a href="/groups/list" class="ttr-material-button"><span class="ttr-label">View Group</span></a>
                            </li>
                            @endif

                            @if($Permissions->add_only == 2)
                            <li>
                                <a href="/groups/add_group" class="ttr-material-button"><span class="ttr-label">Add Group</span></a>
                            </li>
                            @endif
                        </ul>

                </li>
                @endif
                @endforeach

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 3)
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-view-grid"></i></span>
                        <span class="ttr-label">Files/Videos</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>

                        @if($Permissions->view_only == 1)
                        <li>
                            <a href="/teachers/uploads/list" class="ttr-material-button"><span class="ttr-label">View Files/Videos</span></a>
                        </li>
                        @endif

                            @if($Permissions->add_only == 2)
                        <li>
                            <a href="/teachers/uploads/add_uploads" class="ttr-material-button"><span class="ttr-label">Add Files/Videos</span></a>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif
                @endforeach

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 4)
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-camera"></i></span>
                        <span class="ttr-label">Online Class Link </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>

                        @if($Permissions->view_only == 1)
                        <li>
                            <a href="/teachers/conference/list" class="ttr-material-button"><span class="ttr-label">View Class Link</span></a>
                        </li>
                        @endif

                        @if($Permissions->add_only == 2)
                        <li>
                            <a href="/teachers/conference/add_conference" class="ttr-material-button"><span class="ttr-label">Add Class Link</span></a>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif
                @endforeach

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 5)
                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Leads </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>

                        @if($Permissions->view_only == 1)
                        <li>
                            <a href="/admin/classes_list" class="ttr-material-button"><span class="ttr-label">My Leads</span></a>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif
                @endforeach

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 6)
                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Forum </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>

                        @if($Permissions->view_only == 1)
                        <li>
                            <a href="/forum/list" class="ttr-material-button"><span class="ttr-label">Questions List</span></a>
                        </li>
                        @endif

                        @if($Permissions->add_only == 2)
                        <li>
                            <a href="/forum/add_forum" class="ttr-material-button"><span class="ttr-label">Add Topic </span></a>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif
                @endforeach

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 7)
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-view-grid"></i></span>
                        <span class="ttr-label">Payment Tracker</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>

                        @if($Permissions->view_only == 1)
                        <li>
                            <a href="/payments/collection/list" class="ttr-material-button"><span class="ttr-label">Payment Collection</span></a>
                        </li>
                        @endif

                        @if($Permissions->add_only == 2)
                        <li>
                            <a href="/payments/collection/add_payment_collection" class="ttr-material-button"><span class="ttr-label">Add Payment Collection</span></a>
                        </li>
                        @endif
                        @if($Permissions->view_only == 1)
                        <li>
                            <a href="/payments/transferred/list" class="ttr-material-button"><span class="ttr-label">Payment Transferred</span></a>
                        </li>
                        @endif
                        @if($Permissions->add_only == 2)
                        <li>
                            <a href="/payments/transferred/add_payment_transferred" class="ttr-material-button"><span class="ttr-label">Add Payment Transferred</span></a>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif
                @endforeach

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 8)
                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Testimonials </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>

                        @if($Permissions->view_only == 1)
                        <li>
                            <a href="/testimonials/list" class="ttr-material-button"><span class="ttr-label">Testimonials List</span></a>
                        </li>
                        @endif
                        @if($Permissions->add_only == 2)
                        <li>
                            <a href="/testimonials/add_testimonials" class="ttr-material-button"><span class="ttr-label">Add Testimonials</span></a>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif
                @endforeach

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 9)
                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Sample Video </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>

                        @if($Permissions->view_only == 1)
                        <li>
                            <a href="/sample_videos/list" class="ttr-material-button"><span class="ttr-label">Sample Videos List</span></a>
                        </li>
                        @endif
                        @if($Permissions->add_only == 2)
                        <li>
                            <a href="/sample_videos/add_sample_videos" class="ttr-material-button"><span class="ttr-label">Add Sample Videos</span></a>
                        </li>
                        @endif
                    </ul>
                </li>
                @endif
                @endforeach



                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 10)
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-receipt"></i></span>
                        <span class="ttr-label">Certificate</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>

                        @if($Permissions->view_only == 1)
                            <li>
                                <a href="/certificates/certificates_list" class="ttr-material-button"><span class="ttr-label">View Certificate</span></a>
                            </li>
                        @endif
                        @if($Permissions->add_only == 2)
                        <li>
                            <a href="/certificates/add_certificates" class="ttr-material-button"><span class="ttr-label">Add Certificate</span></a>
                        </li>
                        @endif
                    </ul>

                </li>
                @endif
                @endforeach


                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 15)
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-receipt"></i></span>
                        <span class="ttr-label">Home Work Submission </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>

                            @if($Permissions->view_only == 1)
                            <li>
                                <a href="/home_work/student_home_work_list" class="ttr-material-button"><span class="ttr-label">View Home Work</span></a>
                            </li>
                            @endif
                            @if($Permissions->edit_only == 3)
                            <li>
                                <a href="/home_work/list" class="ttr-material-button"><span class="ttr-label">View Home Work</span></a>
                            </li>
                            @endif
                            @if($Permissions->add_only == 2)
                            <li>
                                <a href="/home_work/add_home_work" class="ttr-material-button"><span class="ttr-label">Add Home Work</span></a>
                            </li>
                            @endif
                        </ul>

                </li>
                @endif
                @endforeach

                @foreach($UserPermissions as $Permissions)
                @if($Permissions->permissions == 11)
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-receipt"></i></span>
                        <span class="ttr-label">Fee Collection</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>

                        @if($Permissions->view_only == 1)
                            <li>
                            <a href="/fee_collection/list" class="ttr-material-button"><span class="ttr-label">View Fee Collection</span></a>
                        </li>
                        @endif
                        @if($Permissions->add_only == 2)
                        <li>
                            <a href="/fee_collection/add_fees" class="ttr-material-button"><span class="ttr-label">Add Fee Collection</span></a>
                        </li>
                        @endif
                    </ul>

                </li>
                @endif
                @endforeach



                <li class="ttr-seperate"></li>

            </ul>
            <!-- sidebar menu end -->
        </nav>
        <!-- sidebar menu end -->
    </div>
</div>
@endif
@endif
{{-- @if(Auth::guard('super_admin')->check())
<div class="ttr-sidebar">
    <div class="ttr-sidebar-wrapper content-scroll">
        <!-- side menu logo start -->
        <div class="ttr-sidebar-logo">
            <a href="#"><img alt="" src="../assets/images/logo.png" width="122" height="27"></a>
            <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
            </div> -->
            <div class="ttr-sidebar-toggle-button">
                <i class="ti-arrow-left"></i>
            </div>
        </div>
        <!-- side menu logo end -->
        <!-- sidebar menu start -->
        <nav class="ttr-sidebar-navi">
            <ul>
                <li>
                    <a href="/admin/dashboard" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-home"></i></span>
                        <span class="ttr-label">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-book"></i></span>
                        <span class="ttr-label">User Types</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                            <li>
                            <a href="/user_type/list" class="ttr-material-button"><span class="ttr-label">View User Types</span></a>
                        </li>
                    <li>
                            <a href="/user_type/add_user_types" class="ttr-material-button"><span class="ttr-label">Add User Types</span></a>
                        </li>
                    </ul>

                </li>

                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-book"></i></span>
                        <span class="ttr-label">Users</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                            <li>
                            <a href="/admin/teachers_list" class="ttr-material-button"><span class="ttr-label">View Teachers</span></a>
                        </li>
                        <li>
                            <a href="/admin/students_list" class="ttr-material-button"><span class="ttr-label">View Students</span></a>
                        </li>

                        <li>
                            <a href="/admin/affiliate_list" class="ttr-material-button"><span class="ttr-label">View Affiliates</span></a>
                        </li>
                    </ul>

                </li>

                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-book"></i></span>
                        <span class="ttr-label">Courses/Events</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                            <li>
                            <a href="/courses/list" class="ttr-material-button"><span class="ttr-label">View Courses/Events</span></a>
                        </li>
                        <li>
                            <a href="/courses/add_courses" class="ttr-material-button"><span class="ttr-label">Add Courses/Events</span></a>
                        </li>
                    </ul>

                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-world"></i></span>
                        <span class="ttr-label">Group</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                        <li>
                            <a href="/groups/list" class="ttr-material-button"><span class="ttr-label">View Group</span></a>
                        </li>
                            <li>
                            <a href="/groups/add_group" class="ttr-material-button"><span class="ttr-label">Add Group</span></a>
                        </li>
                    </ul>

                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-view-grid"></i></span>
                        <span class="ttr-label">Files/Videos</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/teachers/uploads/list" class="ttr-material-button"><span class="ttr-label">View Files/Videos</span></a>
                        </li>
                        <li>
                            <a href="/teachers/uploads/add_uploads" class="ttr-material-button"><span class="ttr-label">Add Files/Videos</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-camera"></i></span>
                        <span class="ttr-label">Conference</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/teachers/conference/list" class="ttr-material-button"><span class="ttr-label">View Conference</span></a>
                        </li>
                        <li>
                            <a href="/teachers/conference/add_conference" class="ttr-material-button"><span class="ttr-label">Add Conference</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Leads </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/admin/inquiries_list" class="ttr-material-button"><span class="ttr-label">Inquiries List</span></a>
                        </li>
                        <li>
                            <a href="/admin/partners_list" class="ttr-material-button"><span class="ttr-label">Partners List</span></a>
                        </li>

                        <li>
                            <a href="/admin/classes_list" class="ttr-material-button"><span class="ttr-label">Classes List</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Forum </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/forum/list" class="ttr-material-button"><span class="ttr-label">Questions List</span></a>
                        </li>

                        <li>
                            <a href="/forum/add_forum" class="ttr-material-button"><span class="ttr-label">Add Questions</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-view-grid"></i></span>
                        <span class="ttr-label">Payment Tracker</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/payments/collection/list" class="ttr-material-button"><span class="ttr-label">Payment Collection</span></a>
                        </li>
                        <li>
                            <a href="/payments/collection/add_payment_collection" class="ttr-material-button"><span class="ttr-label">Add Payment Collection</span></a>
                        </li>
                        <li>
                            <a href="/payments/transferred/list" class="ttr-material-button"><span class="ttr-label">Payment Transferred</span></a>
                        </li>
                        <li>
                            <a href="/payments/transferred/add_payment_transferred" class="ttr-material-button"><span class="ttr-label">Add Payment Transferred</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Testimonials </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/testimonials/list" class="ttr-material-button"><span class="ttr-label">Testimonials List</span></a>
                        </li>

                        <li>
                            <a href="/testimonials/add_testimonials" class="ttr-material-button"><span class="ttr-label">Add Testimonials</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Sample Video </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/sample_videos/list" class="ttr-material-button"><span class="ttr-label">Sample Videos List</span></a>
                        </li>

                        <li>
                            <a href="/sample_videos/add_sample_videos" class="ttr-material-button"><span class="ttr-label">Add Sample Videos</span></a>
                        </li>
                    </ul>
                </li>
                <li class="ttr-seperate"></li>
            </ul>
        </nav>
        <!-- sidebar menu end -->
    </div>
</div>

@elseif(Auth::guard('teacher')->check())
<div class="ttr-sidebar">
    <div class="ttr-sidebar-wrapper content-scroll">
        <div class="ttr-sidebar-logo">
            <a href="#"><img alt="" src="../assets/images/logo.png" width="122" height="27"></a>
            <div class="ttr-sidebar-toggle-button">
                <i class="ti-arrow-left"></i>
            </div>
        </div>
        <nav class="ttr-sidebar-navi">
            <ul>
                <li>
                    <a href="/teachers/dashboard" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-home"></i></span>
                        <span class="ttr-label">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-book"></i></span>
                        <span class="ttr-label">Courses / Events</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                            <li>
                            <a href="/courses/list" class="ttr-material-button"><span class="ttr-label">View Courses / Events</span></a>
                        </li>
                        <li>
                            <a href="/courses/add_courses" class="ttr-material-button"><span class="ttr-label">Add Courses / Events</span></a>
                        </li>
                    </ul>

                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-world"></i></span>
                        <span class="ttr-label">Group</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                        <li>
                            <a href="/groups/list" class="ttr-material-button"><span class="ttr-label">View Group</span></a>
                        </li>
                            <li>
                            <a href="/groups/add_group" class="ttr-material-button"><span class="ttr-label">Add Group</span></a>
                        </li>
                    </ul>

                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-view-grid"></i></span>
                        <span class="ttr-label">File/Videos</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/teachers/uploads/list" class="ttr-material-button"><span class="ttr-label">View File/Videos</span></a>
                        </li>
                        <li>
                            <a href="/teachers/uploads/add_uploads" class="ttr-material-button"><span class="ttr-label">Add File/Videos</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-camera"></i></span>
                        <span class="ttr-label">Conference</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/teachers/conference/list" class="ttr-material-button"><span class="ttr-label">View Conference</span></a>
                        </li>
                        <li>
                            <a href="/teachers/conference/add_conference" class="ttr-material-button"><span class="ttr-label">Add Conference</span></a>
                        </li>
                    </ul>
                </li>


                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Leads </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/admin/classes_list" class="ttr-material-button"><span class="ttr-label">Classes List</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="/teachers/students_list" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-home"></i></span>
                        <span class="ttr-label">Student Lists</span>
                    </a>
                </li>

                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Forum </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/forum/list" class="ttr-material-button"><span class="ttr-label">Questions List</span></a>
                        </li>

                        <li>
                            <a href="/forum/add_forum" class="ttr-material-button"><span class="ttr-label">Add Questions</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-receipt"></i></span>
                        <span class="ttr-label">Certificate</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                        <ul>
                            <li>
                            <a href="/certificates/certificates_list" class="ttr-material-button"><span class="ttr-label">View Certificate</span></a>
                        </li>
                        <li>
                            <a href="/certificates/add_certificates" class="ttr-material-button"><span class="ttr-label">Add Certificate</span></a>
                        </li>
                    </ul>

                </li>

                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">My Profile</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/teachers/edit_profile" class="ttr-material-button"><span class="ttr-label">Edit Profile</span></a>
                        </li>
                    </ul>
                </li>
                <li class="ttr-seperate"></li>
            </ul>
        </nav>
    </div>
</div>
@elseif(Auth::guard('student')->check())
<div class="ttr-sidebar">
    <div class="ttr-sidebar-wrapper content-scroll">
        <div class="ttr-sidebar-logo">
            <a href="#"><img alt="" src="../assets/images/logo.png" width="122" height="27"></a>
            <div class="ttr-sidebar-toggle-button">
                <i class="ti-arrow-left"></i>
            </div>
        </div>
        <nav class="ttr-sidebar-navi">
            <ul>
                <li>
                    <a href="/students/dashboard" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-home"></i></span>
                        <span class="ttr-label">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="/students/course_list" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-book"></i></span>
                        <span class="ttr-label">Courses / Events</span>
                    </a>
                </li>
                <li>
                    <a href="/students/groups_list" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-book"></i></span>
                        <span class="ttr-label">Groups</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-view-grid"></i></span>
                        <span class="ttr-label">View File/Videos</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/students/videos_list" class="ttr-material-button"><span class="ttr-label">Videos</span></a>
                        </li>
                        <li>
                            <a href="/students/files_list" class="ttr-material-button"><span class="ttr-label">Files</span></a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="/students/conference_list" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-camera"></i></span>
                        <span class="ttr-label">Conference</span>
                    </a>
                </li>
                <li>
                    <a href="#" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">My Profile</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/students/edit_profile" class="ttr-material-button"><span class="ttr-label">Edit Profile</span></a>
                        </li>
                    </ul>
                </li>


                <li class="ttr-seperate"></li>
            </ul>
        </nav>
    </div>
</div>
@elseif(Auth::guard('affiliate')->check())
<div class="ttr-sidebar">
    <div class="ttr-sidebar-wrapper content-scroll">
        <div class="ttr-sidebar-logo">
            <a href="#"><img alt="" src="../assets/images/logo.png" width="122" height="27"></a>
            <div class="ttr-sidebar-toggle-button">
                <i class="ti-arrow-left"></i>
            </div>
        </div>
        <nav class="ttr-sidebar-navi">
            <ul>
                <li>
                    <a href="/affiliate/dashboard" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-home"></i></span>
                        <span class="ttr-label">Dashboard</span>
                    </a>
                </li>

                <li class="show">
                    <a href="/affiliate/dashboard" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-view-grid"></i></span>
                        <span class="ttr-label">Payment Tracker</span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul style="display: block;">
                        <li>
                            <a href="/affiliate/list" class="ttr-material-button"><span class="ttr-label">Payment Collection</span></a>
                        </li>
                        <li>
                            <a href="/affiliate/payment_transferred_list" class="ttr-material-button"><span class="ttr-label">Payment Transferred</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">Forum </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/forum/list" class="ttr-material-button"><span class="ttr-label">Questions List</span></a>
                        </li>

                        <li>
                            <a href="/forum/add_forum" class="ttr-material-button"><span class="ttr-label">Add Questions</span></a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="" class="ttr-material-button">
                        <span class="ttr-icon"><i class="ti-user"></i></span>
                        <span class="ttr-label">My Profile </span>
                        <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul>
                        <li>
                            <a href="/affiliate/edit_profile" class="ttr-material-button"><span class="ttr-label">Edit Profile</span></a>
                        </li>

                    </ul>
                </li>

                <li class="ttr-seperate"></li>
            </ul>
        </nav>
    </div>
</div>
@endif

 --}}
