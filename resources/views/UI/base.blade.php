<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from Welcome to Skillsgroom.themetrades.com/demo/admin/# by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:08:15 GMT -->
<head>

	<!-- META ============================================= -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta name="robots" content="" />

	<!-- DESCRIPTION -->
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<meta name="description" content="Welcome to Skillsgroom" />

	<!-- OG -->
	<meta property="og:title" content="Welcome to Skillsgroom" />
	<meta property="og:description" content="Welcome to Skillsgroom" />
	<meta property="og:image" content="" />
	<meta name="format-detection" content="telephone=no">

	<!-- FAVICONS ICON ============================================= -->
	<link rel="icon" href="../error-404.html" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="../assets/images/favicon.ico" />

	<!-- PAGE TITLE HERE ============================================= -->
    <title>{{$title}}</title>

	<!-- MOBILE SPECIFIC ============================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!--[if lt IE 9]>
	<script src="../assets/js/html5shiv.min.js"></script>
	<script src="../assets/js/respond.min.js"></script>
	<![endif]-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<!-- All PLUGINS CSS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/assets.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/vendors/calendar/fullcalendar.css')}}">

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<!-- TYPOGRAPHY ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/typography.css')}}">

	<!-- SHORTCODES ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/shortcodes/shortcodes.css')}}">

	<!-- STYLESHEETS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/dashboard.css')}}">
	<link class="skin" rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/color/color-1.css')}}">
	{{-- <link class="skin" rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/pe-icon-7-stroke.css')}}"> --}}
	<link class="skin" rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/drop_uploader.css')}}">

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

	<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">

	<!-- header start -->
	@include('UI.common.header')
	<!-- header end -->
	<!-- Left sidebar menu start -->
	@include('UI.common.sidebar')
	<!-- Left sidebar menu end -->

	<!--Main container start -->
	@yield('Content')
	<div class="ttr-overlay"></div>

<!-- External JavaScripts -->
<script src="{{URL::asset('UI/js/jquery.min.js')}}"></script>
<script src="{{URL::asset('UI/vendors/bootstrap/js/popper.min.js')}}"></script>
<script src="{{URL::asset('UI/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('UI/vendors/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{URL::asset('UI/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js')}}"></script>
<script src="{{URL::asset('UI/vendors/magnific-popup/magnific-popup.js')}}"></script>
<script src="{{URL::asset('UI/vendors/counter/waypoints-min.js')}}"></script>
<script src="{{URL::asset('UI/vendors/counter/counterup.min.js')}}"></script>
<script src="{{URL::asset('UI/vendors/imagesloaded/imagesloaded.js')}}"></script>
<script src="{{URL::asset('UI/vendors/masonry/masonry.js')}}"></script>
<script src="{{URL::asset('UI/vendors/masonry/filter.js')}}"></script>
<script src="{{URL::asset('UI/vendors/owl-carousel/owl.carousel.js')}}"></script>
<script src='{{URL::asset('UI/vendors/scroll/scrollbar.min.js')}}'></script>
<script src="{{URL::asset('UI/js/functions.js')}}"></script>
<script src="{{URL::asset('UI/vendors/chart/chart.min.js')}}"></script>
<script src="{{URL::asset('UI/js/admin.js')}}"></script>
<script src='{{URL::asset('UI/vendors/calendar/moment.min.js')}}'></script>
<script src='{{URL::asset('UI/vendors/calendar/fullcalendar.js')}}'></script>
<script src='{{URL::asset('UI/js/custom/courses.js')}}'></script>
<script src='{{URL::asset('UI/js/drop_uploader.js')}}'></script>

<script src="{{URL::asset('UI/js/cropzee.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
	<script type="text/javascript">

	(function($){
$(document).ready(function(){

     // write code here
	 $('.ckeditor').ckeditor();

});

});
        $(document).ready(function() {
            $('select').select2({
                width: 'element',
  selectOnClose: true
});
        });
    </script>


@yield('JSScript')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>

<script>

var doc = new jsPDF();
var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};

$('#cmd').click(function () {
    doc.fromHTML($('#content').html(), 15, 15, {
        'width': 170,
            'elementHandlers': specialElementHandlers
    });
    doc.save('sample-file.pdf');
});



</script>
</body>

<!-- Mirrored from Welcome to Skillsgroom.themetrades.com/demo/admin/# by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:09:05 GMT -->
</html>
