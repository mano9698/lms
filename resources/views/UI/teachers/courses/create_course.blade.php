@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Courses</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Add Courses</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Add Courses</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/courses/store_course" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Course title</label>
                                    <div>
                                        <input class="form-control" name="title" type="text" value="">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Course Category</label>
                                    <div>
                                        {{-- <input class="form-control" name="category" type="text" value=""> --}}

                                        <select id="" class="form-control" name="category">
                                            <option selected disabled>Select Categories</option>
                                            <option value="1">Tuition & support classes </option>
                                            <option value="2">Hobbies & Activities</option>
                                            <option value="3">Technology Trainings </option>
                                            <option value="4">Certification Courses </option>
                                            <option value="5">Events & seminars </option>
                                            <option value="6">Exam and Test </option>
                                            <option value="7">JobSkills & Professional Skills</option>
                                            <option value="8">Mentorship & coaching </option>
                                            <option value="9">Job interview preparation </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Course Image</label>
                                    <div>
                                        <input class="form-control" id="AddChangeImg" name="course_image" type="file" value="">
                                    </div>
                                    <div id="" class="image-previewer" data-cropzee="AddChangeImg"></div>
                                    <br>
                                    {{-- <div>
                                        <h5>Preview</h5>
                                        <img src="" id="img_preview" alt="" style="
                                        width: 250px;
                                    ">
                                    </div> --}}
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Course description</label>
                                    <div>
                                        <textarea class="form-control ckeditor" name="course_description"> </textarea>
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Course Date</label>
                                    <div>
                                        <input class="form-control" name="course_date" type="date" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Duration</label>
                                    <div>
                                        <input class="form-control" name="duration" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Course Time</label>
                                    <div>
                                        <input class="form-control" name="course_time" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Location</label>
                                    <div>
                                        <input class="form-control" name="location" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Price</label>
                                    <div>
                                        <input class="form-control" name="price" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Who Should Join </label>
                                    <div>
                                        <input class="form-control" name="who_should_join" type="text" value="">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <button type="submit" class="btn">Save & Add Students</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
