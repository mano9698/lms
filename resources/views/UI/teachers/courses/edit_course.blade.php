@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Edit Courses / Events</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Edit Courses / Events</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>Edit Courses</h4> --}}
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/courses/update_course" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Course title</label>
                                    <div>
                                        <input class="form-control" name="id" type="hidden" value="{{$Courses->id}}">

                                    <input class="form-control" name="title" type="text" value="{{$Courses->title}}">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Course Category</label>
                                    <div>
                                        {{-- <input class="form-control" name="category" type="text" value="{{$Courses->category}}"> --}}
                                        <select id="" class="form-control" name="category">
                                            <option value="1" @if($Courses->category == 1) selected @endif>Tuition & support classes </option>
                                            <option value="2" @if($Courses->category == 2) selected @endif>Hobbies & Activities</option>
                                            <option value="3" @if($Courses->category == 3) selected @endif>Technology Trainings </option>
                                            <option value="4" @if($Courses->category == 4) selected @endif>Certification Courses </option>
                                            <option value="5" @if($Courses->category == 5) selected @endif>Events & seminars </option>
                                            <option value="6" @if($Courses->category == 6) selected @endif>Exam and Test </option>
                                            <option value="7" @if($Courses->category == 7) selected @endif>Job interview preparation </option>
                                            <option value="8" @if($Courses->category == 8) selected @endif>Mentorship & coaching </option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Course Image</label>
                                    <div>
                                        <input class="form-control" id="ChangeImg" name="course_image" type="file" value="{{$Courses->course_image}}">
                                    </div>
                                    <div id="" class="image-previewer" data-cropzee="ChangeImg"></div>
                                    <br>
                                    <div id="EditCourseImg">
                                        <h5>Preview</h5>
                                        <img src="/UI/courses/{{$Courses->course_image}}" id="img_preview" alt="" style="
                                        width: 250px;
                                    ">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Course description</label>
                                    <div>
                                        <textarea class="form-control ckeditor" name="course_description">{{$Courses->course_description}} </textarea>
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Course Date</label>
                                    <div>
                                        <input class="form-control" name="course_date" type="text" placeholder="Ex: 01/05/1994" value="{{$Courses->course_date}}">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Duration</label>
                                    <div>
                                        <input class="form-control" name="duration" type="text" value="{{$Courses->duration}}">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Course Time</label>
                                    <div>
                                        <input class="form-control" name="course_time" type="text" value="{{$Courses->course_time}}">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Location</label>
                                    <div>
                                        <input class="form-control" name="location" type="text" value="{{$Courses->location}}">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Price</label>
                                    <div>
                                        <input class="form-control" name="price" type="text" value="{{$Courses->price}}">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Who Should Join </label>
                                    <div>
                                        <input class="form-control" name="who_should_join" type="text" value="{{$Courses->who_should_join}}">
                                    </div>
                                </div>


                                <div class="col-12">
                                    <button type="submit" class="btn">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
