@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
   <div class="container-fluid">
      <div class="db-breadcrumb">
         <h4 class="breadcrumb-title">Courses / Events</h4>
         <ul class="db-breadcrumb-list">
            <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
            <li>Courses / Events List</li>
         </ul>
      </div>
      <!-- Card -->
      <div class="row">
         <!-- Your Profile Views Chart -->
         <div class="col-lg-12 m-b30">
            <div class="widget-box">
               <div class="wc-title">
                  {{-- 
                  <h4>My Courses</h4>
                  --}}
               </div>
               <div class="email-wrapper">
                  <div class="mail-list-container">
                     <div class="mail-toolbar">
                        {{-- 
                        <div class="check-all">
                           <div class="custom-control custom-checkbox checkbox-st1">
                              <input type="checkbox" class="custom-control-input" id="check1">
                              <label class="custom-control-label" for="check1"></label>
                           </div>
                        </div>
                        --}} 
                        <div class="mail-search-bar">
                           <input type="text" class="form-control" placeholder="Search"/> 										
                        </div>
                        <div class="dropdown all-msg-toolbar"><a href="/courses/add_courses" type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Course</a></div>
                        {{-- 
                        <div class="next-prev-btn">
                           <a href="#"><i class="fa fa-angle-left"></i></a>
                           <a href="#"><i class="fa fa-angle-right"></i></a>
                        </div>
                        --}}
                     </div>
                     <div class="table-responsive">
                     <table class="table public-user-block block">
                        <thead>
                           <tr>
                              <th>Title</th>
                              <th width="5%">Action</th>
                           </tr>
                        </thead>
                        <tbody>
                        @if($Courses)
                        @foreach($Courses as $Course)
                           <tr>
                              <td><strong>{{$Course->title}}</strong></td>
                              <td>
                                <ul class="mailbox-toolbar">
                                    <a href="/courses/students_list/{{$Course->id}}">
                                       <li data-toggle="tooltip" title="Students List"><i class="fa fa-list"></i></li>
                                    </a>
                                    <a href="/courses/add_students/{{$Course->id}}">
                                       <li data-toggle="tooltip" title="Add Students"><i class="fa fa-user-o"></i></li>
                                    </a>
                                    <a href="/courses/edit_courses/{{$Course->id}}">
                                       <li data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></li>
                                    </a>
                                    <a href="javascript:void(0);" class="delete_course" data-id="{{$Course->id}}">
                                       <li data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></li>
                                    </a>
                                 </ul>
                              </td>
                           </tr>
                        @endforeach
                        @else
                            <tr>
                                <td>No Data Found...</td>
                            </tr>
                        @endif
                        </tbody>
                     </table>
                    </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Your Profile Views Chart END-->
   </div>
</main>
@endsection