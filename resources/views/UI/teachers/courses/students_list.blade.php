@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Students List</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Students List</li>
            </ul>
        </div>
        <!-- Card -->

        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>Students Joined in the Course</h4> --}}
                    </div>
                    <div class="email-wrapper">

                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <!--{{-- <div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div> --}}-->
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/>
                                </div>
                                {{-- {{-- <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div> --}}
                            </div>
                            <div class="mail-box-list">
                                <div class="mail-list-info">

                                    <div class="mail-list-time col-lg-2">
                                        <span><p><strong>First Name</strong></p></span>
                                    </div>
                                    <div class="mail-list-time col-lg-2">
                                        <span><p><strong>Last Name</strong></p></span>
                                    </div>
                                    <div class="mail-list-time col-lg-2">
                                        <span><p><strong>Gender</strong></p></span>
                                    </div>
                                    {{-- <div class="mail-list-title-info col-lg-3">
                                        <p><strong>Email Address</strong></p>
                                    </div> --}}
                                    {{-- <div class="mail-list-title-info col-lg-1">
                                        <p><strong>Grade</strong></p>
                                    </div> --}}
                                    <div class="mail-list-title-info col-lg-3">
                                        <p><strong>Country/State/City</strong></p>
                                    </div>
                                    <div class="mail-list-time col-lg-2">
                                        <span><strong>Joined Date</strong></span>
                                    </div>

                                </div>
                                @if(isset($StudentsList))
                                @foreach($StudentsList as $Students)
                                <div class="mail-list-info">

                                    <div class="mail-list-time col-lg-2">
                                        <span>{{$Students->first_name}}</span>
                                    </div>
                                    <div class="mail-list-time col-lg-2">
                                        <span>{{$Students->last_name}}</span>
                                    </div>
                                    <div class="mail-list-time col-lg-2">
                                        <span>{{$Students->gender}}</span>
                                    </div>
                                    {{-- <div class="mail-list-title-info col-lg-3">
                                        <p>{{$Students->email}}</p>
                                    </div> --}}
                                    {{-- <div class="mail-list-title-info col-lg-1">
                                        <p>A</p>
                                    </div> --}}
                                    <div class="mail-list-title-info col-lg-3">
                                        <p>{{$Students->country}}/{{$Students->state}}/{{$Students->city}}</p>
                                    </div>
                                    <div class="mail-list-time col-lg-2">
                                        <span>{{date('d/m/Y', strtotime($Students->created_at))}}</span>
                                    </div>
                                </div>
                                @endforeach
                                @else
                                <br>
                                <h4 class="text-center">No data found...</h4>
                                @endif


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection



@section('JSScript')
<script>
    // Pricing add
        function newMenuItem() {
            var newElem = $('tr.list-item').first().clone();
            newElem.find('input').val('');
            newElem.appendTo('table#item-add');
        }

                // Get last id
        // var lastname_id = $('.list-item input[type=text]:nth-child(1)').last().attr('id');
        // var split_id = lastname_id.split('_');

        // // New index
        // var index = Number(split_id[1]) + 1;

        if ($("table#item-add").is('*')) {
            $('.add-item').on('click', function (e) {
                e.preventDefault();
                newMenuItem();
            });
            $(document).on("click", "#item-add .delete", function (e) {
                e.preventDefault();
                $(this).parent().parent().parent().parent().remove();
            });
        }
    </script>


<script>

    $(function() {

      $('input[type=text]').autocomplete({

           source: function(request, response) {
               $.ajax({
               url: "{{url('courses/autocomplete')}}",
               data: {
                       term : request.term
                },
               dataType: "json",
               success: function(data){
                  var resp = $.map(data,function(obj){
                       console.log(obj);
                       return obj.first_name;
                  });

                  response(resp);
               }
           });
       },

    });
   });


// $('.search').on("focus", function() {
//     var id = this.id;


//     $('#'+id).autocomplete({
//         source: function(request, response) {
//                $.ajax({
//                url: "{{url('courses/autocomplete')}}",
//                data: {
//                        term : request.term
//                 },
//                dataType: "json",
//                success: function(data){
//                   var resp = $.map(data,function(obj){
//                        console.log(obj);
//                        return obj.first_name;
//                   });

//                   response(resp);
//                }
//            });
//        },
//     });
// });

   </script>
@endsection
