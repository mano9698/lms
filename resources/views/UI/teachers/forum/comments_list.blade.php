@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Questions List</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Questions List</li>
            </ul>
        </div>	
        <!-- Card -->
        
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>Partners</h4> --}}
                    </div>
                    <div class="email-wrapper">
                        
                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <!--{{-- <div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div> --}}-->
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/> 										
                                </div>
<div class="dropdown all-msg-toolbar">
<span class="btn btn-info-icon" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></span>
                                    {{-- <ul class="dropdown-menu">											
                                        <li><a href="#"><i class="fa fa-arrow-up"></i> Activate</a></li>
                                        <li><a href="#"><i class="fa fa-arrow-down"></i> Deactivate</a></li>
<li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                    </ul> --}}
{{-- <a href="/teachers/conference/add_conference" type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Teachers</a> --}}
                                </div>
                                {{-- {{-- <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div> --}}
                            </div>
                            
                            <ol class="commentlist clearfix" style="
                            padding-left: 10%;
                        ">
                                @if($Comments)
                                @foreach($Comments as $Comment)
                                    <li class="comment even thread-even depth-1" id="li-comment-1">

                                        <div id="comment-1" class="comment-wrap clearfix">

                                            {{-- <div class="comment-meta">

                                                <div class="comment-author vcard">

                                                    <span class="comment-avatar clearfix">
                                                    <img alt="" src="images/forum/avatar.png" class="avatar avatar-60 photo avatar-default" height="60" width="60" /></span>

                                                </div>

                                            </div> --}}

                                            <div class="comment-content clearfix">

                                                <div class="comment-author">{{$Comment->name}}  | <span><a href="#" title="Permalink to this comment">{{date('M d Y', strtotime($Comment->created_at))}} at 19:35</a></span></div>

                                                <p>{{$Comment->comment}}</p>

                                                @if(Auth::guard('super_admin')->check())
                                                    <a class="comment-reply-link" href="/forum/delete_comments/{{$Comment->id}}" onclick="return confirm(' Are you sure. You want to delete?');" style="
                                                    color: red;
                                                ">Delete</a>
                                                @endif
                                            </div>

                                            <div class="clear"></div>

                                        </div>


                                    </li>
                                @endforeach
                            @else
                                <h3>No Comments Found...</h3>
                            @endif
                               
                             </ol>

                        </div>
                    </div>
                </div> 
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection

