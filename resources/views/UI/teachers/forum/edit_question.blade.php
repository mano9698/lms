@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Questions</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Edit Questions</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Edit Questions</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/forum/update_questions" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="row">

                                <div class="form-group col-6">
                                    <label class="col-form-label">Topic /Question</label>
                                    <div>
                                        <input class="form-control" name="id" type="hidden" value="{{$Questions->id}}">

                                        <input class="form-control" name="topic" type="text" value="{{$Questions->topic}}">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Subject /Category</label>
                                    <div>
                                        {{-- <input class="form-control" name="category" type="text" value=""> --}}

                                        <select id="" class="form-control" name="subject">
                                            <option selected disabled>Select Categories</option>
                                            <option value="1" @if($Questions->subject == 1) selected @endif>Forum </option>
                                            <option value="2" @if($Questions->subject == 2) selected @endif>Accountswale blogs</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="form-group col-6">
                                    <label class="col-form-label">Feature Image ( Optional )</label>
                                    <div>
                                        <input class="form-control" name="feature_img" type="file" value="{{$Questions->feature_img}}" id="ChangeImg">
                                    </div>

                                    <div id="" class="image-previewer" data-cropzee="ChangeImg"></div>
                                    <br>
                                    <div>
                                        <h5>Preview</h5>
                                        <img src="/UI/questions/{{$Questions->feature_img}}" id="img_preview" alt="" style="
                                        width: 250px;
                                    ">
                                    </div>

                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Tags</label>
                                    <div>
                                        <input class="form-control ckeditor" name="tags" type="text" value="{{$Questions->tags}}">
                                    </div>
                                </div>

                                {{-- <div class="form-group col-6">
                                    <label class="col-form-label">No Of Students</label>
                                    <div>
                                        <input class="form-control" type="text" value="">
                                    </div>
                                </div> --}}
                                <div class="form-group col-6">
                                    <label class="col-form-label">Details/Answer</label>
                                    <div>
                                        <textarea class="form-control" name="details_answer">{{$Questions->details_answer}}</textarea>
                                    </div>
                                </div>

                                <!--<div class="col-12">
                                    <table id="item-add" style="width:100%;">
                                        <tr class="list-item">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="col-form-label">Course Name</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="col-form-label">Course Category</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="col-form-label">Course Category</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Close</label>
                                                        <div class="form-group">
                                                            <a class="delete" href="#"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>-->
                                <div class="col-12">
                                    <!--<button type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Item</button>-->
                                    <button type="submit" class="btn">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
