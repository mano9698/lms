@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Conference</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Conference</li>
            </ul>
        </div>	
        <!-- Card -->
        
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>My Conferences</h4>
                    </div>
                    <div class="email-wrapper">
                        
                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <!--{{-- <div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div> --}}-->
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/> 										
                                </div>
<div class="dropdown all-msg-toolbar">
<span class="btn btn-info-icon" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></span>
                                    <ul class="dropdown-menu">											
                                        <li><a href="#"><i class="fa fa-arrow-up"></i> Activate</a></li>
                                        <li><a href="#"><i class="fa fa-arrow-down"></i> Deactivate</a></li>
<li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                    </ul>
<a href="/teachers/conference/add_conference" type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Conference</a>
                                </div>
                                {{-- <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div> --}}
                            </div>
                            <div class="mail-box-list">
                                <div class="mail-list-info">
                                    <div class="checkbox-list">
                                        <div class="custom-control custom-checkbox checkbox-st1">
                                            <input type="checkbox" class="custom-control-input" id="check">
                                            <label class="custom-control-label" for="check"></label>
                                        </div>
                                    </div>

                                    <div class="mail-list-title-info">
                                        <p><strong>Name</strong></p>
                                    </div>
                                    <div class="mail-list-title-info">
                                        <p><strong>Join</strong></p>
                                    </div>
                                    <div class="mail-list-title-info">
                                        <p><strong>Duration (Minutes)</strong></p>
                                    </div>
                                    <div class="mail-list-time">
                                        <span><strong>Date</strong></span>
                                    </div>
<!--										<ul class="mailbox-toolbar">
                                        <li data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></li>
                                        <li data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></li>
                                    </ul>-->
                                </div>

                                @if($Conference)
                                @foreach($Conference as $Conferences)
                                <div class="mail-list-info">
                                    {{-- <div class="checkbox-list">
                                        <div class="custom-control custom-checkbox checkbox-st1">
                                            <input type="checkbox" class="custom-control-input" id="check2">
                                            <label class="custom-control-label" for="check2"></label>
                                        </div>
                                    </div> --}}

                                    <div class="mail-list-title-info">
                                        <p>{{$Conferences->name}}</p>
                                    </div>
                                    <div class="mail-list-title-info">
                                        <a href="{{$Conferences->meeting_link}}" style="color:#000!important"><i class="fa fa-play" target="_blank"></i></a>
                                    </div>
                                    <div class="mail-list-title-info">
                                        <p>{{$Conferences->duration}}</p>
                                    </div>
                                    {{-- <div class="mail-list-time">
                                        <span>10/29/2020, 10:00</span>
                                    </div> --}}
                                    <ul class="mailbox-toolbar">
                                        <a href="/teachers/conference/add_groups/{{$Conferences->id}}">
                                            <li data-toggle="tooltip" title="Group Lists"><i class="fa fa-users"></i></li>
                                        </a>

                                        <a href="/teachers/conference/students_list/{{$Conferences->id}}">
                                            <li data-toggle="tooltip" title="Students List"><i class="fa fa-list"></i></li>
                                        </a>

                                        <a href="/teachers/conference/add_students/{{$Conferences->id}}">
                                            <li data-toggle="tooltip" title="Add Students"><i class="fa fa-user-o"></i></li>
                                        </a>

                                        <a href="/teachers/conference/edit_conference/{{$Conferences->id}}">
                                            <li data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></li>
                                        </a>

                                        <a href="javascript:void(0);" class="delete_conference" data-id="{{$Conferences->id}}">
                                            <li data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></li>
                                        </a>
                                    </ul>
                                </div>
                                @endforeach
                                @else

                                @endif

                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection