@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Certificate</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Add Certificate</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Add Certificate</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" method="post" action="/certificates/store_certificates" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="form-group col-12">
                                    <label class="col-form-label">Select the Certificate</label>
                                    <div class="radio-list d-flex">
                                        <div class="custom-control custom-radio radio-st1">
                                            <input type="radio" name="certificate_id" class="custom-control-input" id="check1" value="1">
                                            <label class="custom-control-label ml-2" for="check1"> <a href="{{ URL::asset('UI/images/certificate_1.jpg') }}" target="_blank"> <img src="{{ URL::asset('UI/images/certificate_1.jpg') }}" width="300"></a></label>
                                        </div>
                                        <div class="custom-control custom-radio radio-st1 ml-4">
                                            <input type="radio" name="certificate_id" class="custom-control-input" id="check2" value="2">
                                            <label class="custom-control-label ml-2" for="check2"><a href="{{ URL::asset('UI/images/certificate_1.jpg') }}" target="_blank">  <img src="{{ URL::asset('UI/images/certificate_2.jpg') }}" width="300"></a></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Select Title</label>
                                    <div>
                                        <select class="form-control" name="title">
                                            <option>Mr.</option>
                                            <option>Ms.</option>
                                            <option>Master.</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Select Award</label>
                                    <div>
                                        <select class="form-control" name="award" value="">
                                            <option value="Achievement">Achievement</option>
                                            <option value="Completion">Completion</option>
                                            <option value="Participation">Participation</option>
                                            <option value="Appreciation">Appreciation</option>
                                            <option value="Excellence">Excellence</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">First Name</label>
                                    <div>
                                        <input class="form-control" type="text" name="first_name" value="">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Last Name</label>
                                    <div>
                                        <input class="form-control" type="text" name="last_name" value="">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Coaching Partner Name</label>
                                    <div>
                                        <input class="form-control" name="coaching_partner_name" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Coaching Partner Logo</label>
                                    <div>
                                        <input class="form-control" type="file" name="partner_logo" value="">
                                    </div>
                                </div>


                                <div class="form-group col-6">
                                    <label class="col-form-label">Description</label>
                                    <div>
                                        <textarea class="form-control" name="description">For Successfully completing Accounting Certification Course Organized by ACCOUNTSWALE Bangalore, India from June 2020 through October 2020 as a special batch for TCO Members </textarea>
                                    </div>
                                </div>
                                <!--<div class="col-12">
                                    <table id="item-add" style="width:100%;">
                                        <tr class="list-item">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="col-form-label">Course Name</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="col-form-label">Course Category</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="col-form-label">Course Category</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Close</label>
                                                        <div class="form-group">
                                                            <a class="delete" href="#"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>-->
                                <div class="col-12">
                                    <!--<button type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Item</button>-->
                                    <button type="submit" name="action" value="save_preview" class="btn">Save & Preview</button>
                                    <button type="submit" name="action" value="confirm" class="btn">Confirm</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
