@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Certificates</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Certificates</li>
            </ul>
        </div>
        <!-- Card -->

        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>My Certificate</h4>
                    </div>
                    <div class="email-wrapper">

                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <!--<div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div>-->
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search By Id"/>
                                </div>
<div class="dropdown all-msg-toolbar">
<span class="btn btn-info-icon" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></span>
                                    <ul class="dropdown-menu">
                                        <li><a href="#"><i class="fa fa-arrow-up"></i> Activate</a></li>
                                        <li><a href="#"><i class="fa fa-arrow-down"></i> Deactivate</a></li>
<li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                    </ul>
<a href="/certificates/add_certificates" type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Certificate</a>
                                </div>
                            </div>
                            <table class="table public-user-block block">
                  <thead>
                    <tr>
                        <th>Id</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Teacher</th>
                      <th>Action</th>

                    </tr>
                  </thead>
                  <tbody>
                      @if($Certificates)
                      @foreach($Certificates as $Certificate)
                    <tr>
                        <td><strong><a href="/certificates/view_certificate/{{ $Certificate->id }}" target="_blank">{{ $Certificate->certificate_no }}</a></strong></td>
                      <td>{{ $Certificate->first_name }}</td>
                      <td>{{ $Certificate->last_name }}</td>
                      <td>{{ $Certificate->Fname }} {{ $Certificate->Lname }}</td>
                      <td><ul class="mailbox-toolbar" style="position: inherit;">
                            <a href="/certificates/view_certificate/{{ $Certificate->id }}" target="_blank"><li data-toggle="tooltip" title="Download"><i class="fa fa-download"></i></li>
                            </a>
                        </ul></td>

                    </tr>
                    @endforeach
                    @else
                        <tr>
                            <td>No Data Found</td>
                        </tr>
                    @endif
                  </tbody>
                </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
