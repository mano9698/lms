@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Students List </h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Students List</li>
            </ul>
        </div>	
        <!-- Card -->
        
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>Students Joined in the Course</h4> --}}
                    </div>
                    <div class="email-wrapper">
                        
                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <!--{{-- <div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div> --}}-->
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/> 										
                                </div>
                                {{-- {{-- <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div> --}}
                            </div>
                            <div class="mail-box-list">
                                <div class="table-responsive">
                                <table class="table public-user-block block">
                                    <thead>
                                       <tr>
                                          <th>First Name</th>
                                          <th>Last Name</th>
                                          <th>Gender</th>
                                          <th>Country/State/City</th>
                                          <th>Joined Date</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                    @if($GroupStudentsList)
                                    @foreach($GroupStudentsList as $Students)
                                       <tr>
                                          <td><strong>{{$Students->first_name}}</strong></td>
                                          <td><strong>{{$Students->last_name}}</strong></td>
                                          <td><strong>{{$Students->gender}}</strong></td>
                                          <td><strong>{{$Students->country}}/{{$Students->state}}/{{$Students->city}}</strong></td>
                                          <td><strong>{{date('d/m/Y', strtotime($Students->created_at))}}</strong></td>
                                       </tr>
                                    @endforeach
                                    @else
                                        <tr>
                                            <td>No Data Found...</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                 </table>
                                </div>
                                

                            </div>

                           
                        </div>
                    </div>
                </div> 
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection



@section('JSScript')
<script>
    // Pricing add
        function newMenuItem() {
            var newElem = $('tr.list-item').first().clone();
            newElem.find('input').val('');
            newElem.appendTo('table#item-add');
        }

                // Get last id 
        // var lastname_id = $('.list-item input[type=text]:nth-child(1)').last().attr('id');
        // var split_id = lastname_id.split('_');

        // // New index
        // var index = Number(split_id[1]) + 1;
        
        if ($("table#item-add").is('*')) {
            $('.add-item').on('click', function (e) {
                e.preventDefault();
                newMenuItem();
            });
            $(document).on("click", "#item-add .delete", function (e) {
                e.preventDefault();
                $(this).parent().parent().parent().parent().remove();
            });
        }
    </script>


<script>

    $(function() {
      
      $('input[type=text]').autocomplete({
   
           source: function(request, response) {
               $.ajax({
               url: "{{url('courses/autocomplete')}}",
               data: {
                       term : request.term
                },
               dataType: "json",
               success: function(data){
                  var resp = $.map(data,function(obj){
                       console.log(obj);
                       return obj.first_name;
                  }); 
   
                  response(resp);
               }
           });
       },
      
    }); 
   }); 


// $('.search').on("focus", function() {
//     var id = this.id;


//     $('#'+id).autocomplete({
//         source: function(request, response) {
//                $.ajax({
//                url: "{{url('courses/autocomplete')}}",
//                data: {
//                        term : request.term
//                 },
//                dataType: "json",
//                success: function(data){
//                   var resp = $.map(data,function(obj){
//                        console.log(obj);
//                        return obj.first_name;
//                   }); 
   
//                   response(resp);
//                }
//            });
//        },
//     }); 
// }); 
   
   </script>  
@endsection