@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Edit Group</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Edit Group</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>Edit Group</h4> --}}
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/groups/update_groups" method="post">
                            @csrf
                            <div class="row">
                                
                                <div class="form-group col-6">
                                    <label class="col-form-label">Group title</label>
                                    <div>
                                    <input class="form-control" name="id" type="hidden" value="{{$Groups->id}}">
                                    <input class="form-control" name="title" type="text" value="{{$Groups->title}}">
                                    </div>
                                </div>
                                {{-- <div class="form-group col-6">
                                    <label class="col-form-label">No Of Students</label>
                                    <div>
                                        <input class="form-control" type="text" value="">
                                    </div>
                                </div> --}}
                                <div class="form-group col-6">
                                    <label class="col-form-label">Group Description</label>
                                    <div>
                                        <textarea class="form-control" name="description"> {{$Groups->description}}</textarea>
                                    </div>
                                </div>
                                
                                <!--<div class="col-12">
                                    <table id="item-add" style="width:100%;">
                                        <tr class="list-item">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="col-form-label">Course Name</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="col-form-label">Course Category</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="col-form-label">Course Category</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Close</label>
                                                        <div class="form-group">
                                                            <a class="delete" href="#"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>-->
                                <div class="col-12">
                                    <!--<button type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Item</button>-->
                                    <button type="submit" class="btn">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection