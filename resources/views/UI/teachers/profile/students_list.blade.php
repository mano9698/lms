@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Student List</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Students List</li>
            </ul>
        </div>	
        <!-- Card -->
        
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>Add the students to the course</h4> --}}
                    </div>
                    <div class="email-wrapper">
                        
                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <!--{{-- <div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div> --}}-->
                                {{-- <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/> 										
                                </div> --}}
                                {{-- <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div> --}}
                            </div>
                            <div class="mail-box-list">
                                <div class="mail-list-info">

                                    <div class="mail-list-time col-lg-3">
                                        <span><p><strong>Name</strong></p></span>
                                    </div>
                                    {{-- <div class="mail-list-title-info col-lg-3">
                                        <p><strong>Date</strong></p>
                                    </div> --}}


                                </div>

                                @foreach($StudentsList as $Students)
                                <div class="mail-list-info">

                                    <div class="mail-list-time col-lg-3">
                                        <span>{{$Students->first_name}} {{$Students->last_name}}</span>
                                    </div>
                                    {{-- <div class="mail-list-title-info col-lg-3">
                                        <p>10/29/2020</p>
                                    </div> --}}

                                    
                                </div>								
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
