@extends('UI.base')
@section('Content')
<!--Main container start -->
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">My Profile</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>My Profile</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>User Profile update</h4> --}}
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/teachers/update_profile" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="">
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">First Name</label>
                                    <div class="col-sm-4">
                                        @if(isset($Users->first_name))
                                            <input class="form-control" type="text" name="first_name" value="{{$Users->first_name}}" disabled >
                                        @endif
                                    </div>
                                    <label class="col-sm-2 col-form-label">Last Name</label>
                                    <div class="col-sm-4">
                                        @if(isset($Users->last_name))
                                            <input class="form-control" type="text" name="last_name" value="{{$Users->last_name}}" disabled>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Email ID</label>
                                    <div class="col-sm-4">
                                        @if(isset($Users->email))
                                            <input class="form-control" type="text" name="email" value="{{$Users->email}}" disabled>
                                        @endif
                                    </div>
                                    <label class="col-sm-2 col-form-label">Contact Number</label>
                                    <div class="col-sm-4">
                                        @if(isset($Users->mobile))
                                            <input class="form-control" type="text" name="mobile" value="{{$Users->mobile}}">
                                        @else
                                            <input class="form-control" type="text" name="mobile" value="" required>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Country</label>
                                    <div class="col-sm-4">
                                        @if(isset($Users->country))
                                            <input class="form-control" type="text" name="country" value="{{$Users->country}}">
                                        @else
                                            <input class="form-control" type="text" name="country" value="" required>
                                        @endif
                                    </div>
                                    <label class="col-sm-2 col-form-label">State</label>
                                    <div class="col-sm-4">
                                        @if(isset($Users->state))
                                            <input class="form-control" type="text" name="state" value="{{$Users->state}}">
                                        @else
                                            <input class="form-control" type="text" name="state" value="" required>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">City</label>
                                    <div class="col-sm-4">
                                        @if(isset($Users->city))
                                            <input class="form-control" type="text" name="city" value="{{$Users->city}}">
                                        @else
                                            <input class="form-control" type="text" name="city" value="" required>
                                        @endif
                                    </div>
                                    <label class="col-sm-2 col-form-label">Pincode</label>
                                    <div class="col-sm-4">
                                        @if(isset($Users->pincode))
                                            <input class="form-control" type="text" name="pincode" value="{{$Users->pincode}}">
                                        @else
                                            <input class="form-control" type="text" name="pincode" value="" required>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Area</label>
                                    <div class="col-sm-4">
                                        @if(isset($Users->area))
                                            <input class="form-control" type="text" name="area" value="{{$Users->area}}">
                                        @else
                                            <input class="form-control" type="text" name="area" value="" required>
                                        @endif
                                    </div>
                                </div>
                                <!--<div class="form-group row">
                                    <div class="col-sm-10">
                                        <h3>Change Password</h3>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">New Password</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="Password" value="">
                                    </div>
                                    <label class="col-sm-2 col-form-label">Confirm Password</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="Password" value="">
                                    </div>
                                </div>-->
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <h3>1. Personal Details</h3>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Profile Picture</label>
                                    <div class="col-sm-7">
                                        @if(isset($TeachersPersonalDetails->profile_pic))
                                            <input class="form-control" type="file" name="profile_pic" id="profile_pic" value="{{$TeachersPersonalDetails->profile_pic}}">

                                            <div id="" class="image-previewer" data-cropzee="profile_pic"></div>
                                        @else
                                            <input class="form-control" id="profile_pic" type="file" name="profile_pic" value="" required>

                                            <div id="" class="image-previewer" data-cropzee="profile_pic"></div>
                                        @endif

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Gender</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->gender))
                                            <input class="form-control" type="text" name="gender" value="{{$TeachersPersonalDetails->gender}}">
                                        @else
                                        <input class="form-control" type="text" name="gender" value="" required>
                                        @endif
                                    </div>
                                    <label class="col-sm-2 col-form-label">Qualification</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->qualification))
                                        <input class="form-control" type="text" name="qualification" value="{{$TeachersPersonalDetails->qualification}}">
                                        @else
                                        <input class="form-control" type="text" name="qualification" value="" required>
                                        @endif


                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Partner type</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->tutor_type))
                                        <select class="form-control" name="tutor_type">
                                            <option value="Individual" @if($TeachersPersonalDetails->tutor_type == "Individual") selected @endif>Individual</option>
                                            <option value="Coaching Classes" @if($TeachersPersonalDetails->tutor_type == "Coaching Classes") selected @endif>Coaching Classes</option>
                                        </select>
                                        @else
                                        <select class="form-control" name="tutor_type">
                                            <option value="Individual">Individual</option>
                                            <option value="Coaching Classes" >Coaching Classes</option>
                                        </select>
                                        @endif
                                    </div>
                                    <label class="col-sm-2 col-form-label">Teaching Subjects</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->teaching_subjects))
                                        <input class="form-control" type="text" name="teaching_subjects" value="{{$TeachersPersonalDetails->teaching_subjects}}">
                                        @else
                                        <input class="form-control" type="text" name="teaching_subjects" value="" required>
                                        @endif

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Teaching Language</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->teaching_language))
                                        <input class="form-control" type="text" name="teaching_language" value="{{$TeachersPersonalDetails->teaching_language}}">
                                        @else
                                        <input class="form-control" type="text" name="teaching_language" value="" required>
                                        @endif

                                    </div>
                                    <label class="col-sm-2 col-form-label">No students at present</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->no_of_student_present))
                                        <input class="form-control" type="text" name="no_of_student_present" value="{{$TeachersPersonalDetails->no_of_student_present}}">
                                        @else
                                        <input class="form-control" type="text" name="no_of_student_present" value="" required>
                                        @endif

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Your Website link</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->website_link))
                                        <input class="form-control" type="text" name="website_link" value="{{$TeachersPersonalDetails->website_link}}">
                                        @else
                                        <input class="form-control" type="text" name="website_link" value="">
                                        @endif

                                    </div>
                                    <label class="col-sm-2 col-form-label">Your Instagram</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->instagram_link))
                                        <input class="form-control" type="text" name="instagram_link" value="{{$TeachersPersonalDetails->instagram_link}}">
                                        @else
                                        <input class="form-control" type="text" name="instagram_link" value="">
                                        @endif

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Your Facebook</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->facebook_link))
                                        <input class="form-control" type="text" name="facebook_link" value="{{$TeachersPersonalDetails->facebook_link}}">
                                        @else
                                        <input class="form-control" type="text" name="facebook_link" value="">
                                        @endif

                                    </div>
                                    <label class="col-sm-2 col-form-label">Your YouTube Channel</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->youtube_channel))
                                        <input class="form-control" type="text" name="youtube_channel" value="{{$TeachersPersonalDetails->youtube_channel}}">
                                        @else
                                        <input class="form-control" type="text" name="youtube_channel" value="">
                                        @endif

                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Write Your Profile Summary</label>
                                    <div class="col-sm-6">
                                        @if(isset($TeachersPersonalDetails->about_me))
                                            <textarea class="form-control ckeditor" name="profile_description">{{$TeachersPersonalDetails->about_me}}</textarea>
                                        @else
                                            <textarea class="form-control ckeditor" name="profile_description"> </textarea>
                                        @endif

                                    </div>
                                </div>

                                <!--<div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Company Name</label>
                                    <div class="col-sm-7">
                                        <input class="form-control" type="text" value="EduChamp">
                                        <span class="help">If you want your invoices addressed to a company. Leave blank to use your full name.</span>
                                    </div>
                                </div>-->

                                <div class="seperator"></div>

                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <h3>2. Who are your targetted students</h3>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">School /College Boards</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->school_clg_boards))
                                        <input class="form-control" type="text" name="school_clg_boards" value="{{$TeachersPersonalDetails->school_clg_boards}}">
                                        @else
                                        <input class="form-control" type="text" name="school_clg_boards" value="">
                                        @endif

                                    </div>
                                    <label class="col-sm-2 col-form-label">Students Grades / standard studying</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->student_grades_studying))
                                        <input class="form-control" type="text" name="student_grades_studying" value="{{$TeachersPersonalDetails->student_grades_studying}}">
                                        @else
                                        <input class="form-control" type="text" name="student_grades_studying" value="">
                                        @endif

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Students Age ( only if applicable )</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->student_age))
                                        <input class="form-control" type="text" name="student_age" value="{{$TeachersPersonalDetails->student_age}}">
                                        @else
                                        <input class="form-control" type="text" name="student_age" value="">
                                        @endif

                                    </div>
                                </div>
                                <div class="seperator"></div>

                                <div class="form-group row mb-1">
                                    <div class="col-sm-10">
                                        <h3>3. Your Fee / Rate</h3>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <table>
                                        <?php
                                        $UserId = Session::get('TeacherId');

                                            $teacher_fees = DB::table('teacher_fees')
                                                            ->where('teacher_id', $UserId)
                                                            ->get();
                                        ?>
                                        @if(isset($teacher_fees))
                                        @foreach($teacher_fees as $fees)
                                        <tr class="list-item1">
                                            <td>
                                                <div class="row">
                                                    <div class="col-mr-2">
                                                        <label class="col-form-label">Grade</label>
                                                        <div>
                                                            <input class="form-control" name="" type="text" value="{{$fees->grade}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Subject</label>
                                                        <div>
                                                            <input class="form-control" name="" type="text" value="{{$fees->subject}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Hourly Rate</label>
                                                        <div>
                                                            <input class="form-control" name="" type="text" value="{{$fees->hourly_rate}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Monthly Rate</label>
                                                        <div>
                                                            <input class="form-control" name="" type="text" value="{{$fees->monthly_rate}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Yearly Rate</label>
                                                        <div>
                                                            <input class="form-control" name="" type="text" value="{{$fees->yearly_rate}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Close</label>
                                                        <div class="form-group">
                                                            <a class="delete delete_fees" data-id="{{$fees->id}}" href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </table>
                                    <table id="item-add1" style="width:100%;">
                                        {{-- <tr class="list-item1">
                                            <td>
                                                <div class="row">
                                                    <div class="col-mr-2">
                                                        <label class="col-form-label">Grade</label>
                                                        <div>
                                                            <input class="form-control" name="fees[grade]" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Subject</label>
                                                        <div>
                                                            <input class="form-control" name="fees[subject]" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Hourly Rate</label>
                                                        <div>
                                                            <input class="form-control" name="fees[rate]" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Monthly Rate</label>
                                                        <div>
                                                            <input class="form-control" name="fees[monthly_rate]" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Yearly Rate</label>
                                                        <div>
                                                            <input class="form-control" name="fees[yearly_rate]" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Close</label>
                                                        <div class="form-group">
                                                            <a class="delete" href="#"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr> --}}
                                    </table>
                                </div>
                                <div class="col-12 pl-0">
                                    {{-- <button type="button" class="btn-secondry add-item1 m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add</button> --}}

                                    <button type="button" class="btn-secondry m-r5" id="add_fees"><i class="fa fa-fw fa-plus-circle"></i>Add</button>

                                </div>
                                <div class="col-12 m-t20">
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <h3 class="m-form__section">4. Your Batch Timings</h3>
                                    </div>
                                </div>
<div class="col-12 pl-0">
                                    <table>
                                        <?php
                                        $UserId = Session::get('TeacherId');

                                            $teacher_tuition = DB::table('teacher_tuition')
                                                            ->where('teacher_id', $UserId)
                                                            ->get();
                                        ?>
                                        @if(isset($teacher_tuition))
                                        @foreach($teacher_tuition as $tuition)
                                            <tr class="list-item2">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <label class="col-form-label">Day</label>
                                                        <div>
                                                            <select>
                                                                <option value="Monday" @if($tuition->days == "Monday") selected @endif>Monday</option>
                                                                <option value="Tuesday" @if($tuition->days == "Tuesday") selected @endif>Tuesday</option>
                                                                <option value="Wednesday" @if($tuition->days == "Wednesday") selected @endif>Wednesday</option>
                                                                <option value="Thursday" @if($tuition->days == "Thursday") selected @endif>Thursday</option>
                                                                <option value="Friday" @if($tuition->days == "Friday") selected @endif>Friday</option>
                                                                <option value="Saturday" @if($tuition->days == "Saturday") selected @endif>Saturday</option>
                                                                <option value="Sunday" @if($tuition->days == "Sunday") selected @endif>Sunday</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Time Option 1</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="{{$tuition->time_option1}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Time Option 2</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="{{$tuition->time_option2}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Time Option 3</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="{{$tuition->time_option3}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Time Option 4</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="{{$tuition->time_option4}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Time Option 5</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="{{$tuition->time_option5}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <label class="col-form-label">Close</label>
                                                        <div class="form-group">
                                                            <a class="delete delete_tuition" data-id="{{$tuition->id}}" href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </table>


                                    <table id="item-tuition" style="width:100%;">
                                        {{-- <tr class="list-item2">
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Day</label>
                                                        <div>
                                                            <select>
                                                                <option>Monday</option>
                                                                <option>Tuesday</option>
                                                                <option>Wednesday</option>
                                                                <option>Thursday</option>
                                                                <option>Friday</option>
                                                                <option>Saturday</option>
                                                                <option>Sunday</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Time Option 1</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Time Option 2</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Time Option 3</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Time Option 4</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <!--<div class="col-md-2">
                                                        <label class="col-form-label">Time Option 5</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>-->
                                                    <div class="col-md-1">
                                                        <label class="col-form-label">Close</label>
                                                        <div class="form-group">
                                                            <a class="delete" href="#"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr> --}}
                                    </table>
                                </div>
                                <div class="col-12 pl-0">
                                    <button type="button" id="add_tution" class="btn-secondry m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add</button>

                                </div>

                            </div>
                            <div class="pt-4">
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <h3>5. Additional Information ( Applicable if Logo Available )</h3>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Logo</label>
                                    <div class="col-sm-4">

                                        @if(isset($TeachersPersonalDetails->logo))
                                        <input class="form-control" type="file" id="logo" name="logo" value="{{$TeachersPersonalDetails->logo}}">

                                        <div id="" class="image-previewer" data-cropzee="logo"></div>

                                        @else
                                        <input class="form-control" type="file" id="logo" name="logo" value="">

                                        <div id="" class="image-previewer" data-cropzee="logo"></div>
                                        @endif


                                    </div>
                                    <label class="col-sm-2 col-form-label">Total No Of Students</label>
                                    <div class="col-sm-4">
                                        @if(isset($TeachersPersonalDetails->total_no_of_students))
                                        <input class="form-control" type="text" name="total_no_of_students" value="{{$TeachersPersonalDetails->total_no_of_students}}">
                                        @else
                                        <input class="form-control" type="text" name="total_no_of_students" value="">
                                        @endif


                                    </div>
                                </div>

                            <div class="col-12 m-t20">
                                </div>
                                <div class="col-12">
                                    <table>
                                        <?php
                                        $UserId = Session::get('TeacherId');

                                            $teachers_additional_info = DB::table('teachers_additional_info')
                                                            ->where('teacher_id', $UserId)
                                                            ->get();
                                        ?>
                                        @if(isset($teachers_additional_info))
                                        @foreach($teachers_additional_info as $info)

                                        <tr class="list-item">
                                            <td>
                                                <div class="row">
                                                    <div class="col-mr-2">
                                                        <label class="col-form-label">Teacher First Name</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="{{$info->first_name}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Teacher Last Name</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="{{$info->last_name}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Gender</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="{{$info->gender}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Experience in years</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="{{$info->experience}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Teaching Subjects</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="{{$info->subjects}}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Close</label>
                                                        <div class="form-group">
                                                            <a class="delete delete_info" data-id="{{$info->id}}" href="javascript:void(0);"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </table>


                                    <table id="item-info" style="width:100%;">
                                        {{-- <tr class="list-item">
                                            <td>
                                                <div class="row">
                                                    <div class="col-mr-2">
                                                        <label class="col-form-label">Teacher First Name</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Teacher Last Name</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Gender</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Experience in years</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Teaching Subjects</label>
                                                        <div>
                                                            <input class="form-control" type="text" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <label class="col-form-label">Close</label>
                                                        <div class="form-group">
                                                            <a class="delete" href="#"><i class="fa fa-close"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr> --}}
                                    </table>
                                </div>
                                <div class="col-12 pl-0">
                                    <button type="button" class="btn-secondry m-r5" id="add_info"><i class="fa fa-fw fa-plus-circle"></i>Add</button>

                                </div>

                            </div>
                            <div class="row mt-5">

                                <div class="col-sm-7">
                                    <button type="submit" class="btn">Save changes</button>
                                    <button type="reset" class="btn-secondry">Cancel</button>
                                </div><div class="col-sm-2">
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection

@section('JSScript')
<script>
    var i = 0;

       $("#add_fees").click(function(){

           ++i;

           $("#item-add1").append('<tr class="list-item1"><td><div class="row"><div class="col-mr-2"><label class="col-form-label">Grade</label><div><input class="form-control" name="fees['+i+'][grade]" type="text" value=""></div></div><div class="col-mr-2"><label class="col-form-label">Subject</label><div><input class="form-control" name="fees['+i+'][subject]" type="text" value=""></div></div><div class="col-mr-2"><label class="col-form-label">Hourly Rate</label><div><input class="form-control" name="fees['+i+'][hourly_rate]" type="text" value=""></div></div><div class="col-mr-2"><label class="col-form-label">Monthly Rate</label><div><input class="form-control" name="fees['+i+'][monthly_rate]" type="text" value=""></div></div><div class="col-mr-2"><label class="col-form-label">Yearly Rate</label><div><input class="form-control" name="fees['+i+'][yearly_rate]" type="text" value=""></div></div><div class="col-md-2"><label class="col-form-label">Close</label><div class="form-group"><a class="delete remove-tr" href="javascript:void(0);"><i class="fa fa-close"></i></a></div></div></div></td></tr>');
       });

       $(document).on('click', '.remove-tr', function(){
            $(this).parents('tr').remove();
       });



// Tution
       var j = 0;
$("#add_tution").click(function(){

      ++j;

      $("#item-tuition").append('<tr class="list-item2"><td><div class="row"><div class="col-md-1"><label class="col-form-label">Day</label><div><select class="form-control" name="tuition['+j+'][days]"><option value="Monday">Monday</option><option value="Tuesday">Tuesday</option><option value="Wednesday">Wednesday</option><option value="Thursday">Thursday</option><option value="Friday">Friday</option><option value="Saturday">Saturday</option><option value="Sunday">Sunday</option></select></div></div><div class="col-md-2"><label class="col-form-label">Time Option 1</label><div><input class="form-control" name="tuition['+j+'][option1]" type="text" value=""></div></div><div class="col-md-2"><label class="col-form-label">Time Option 2</label><div><input name="tuition['+j+'][option2]" class="form-control" type="text" value=""></div></div><div class="col-md-2"><label class="col-form-label">Time Option 3</label><div><input class="form-control" name="tuition['+j+'][option3]" type="text" value=""></div></div><div class="col-md-2"><label class="col-form-label">Time Option 4</label><div><input class="form-control" name="tuition['+j+'][option4]" type="text" value=""></div></div><div class="col-md-2"><label class="col-form-label">Time Option 5</label><div><input class="form-control" name="tuition['+j+'][option5]" type="text" value=""></div></div><div class="col-md-1"><label class="col-form-label">Close</label><div class="form-group"><a class="delete remove-tution" href="javascript:void(0);"><i class="fa fa-close"></i></a></div></div></div></td></tr>');
  });

  $(document).on('click', '.remove-tution', function(){
       $(this).parents('tr').remove();
  });




  var k = 0;
$("#add_info").click(function(){

      ++k;

      $("#item-info").append('<tr class="list-item"><td><div class="row"><div class="col-mr-2"><label class="col-form-label">Teacher First Name</label><div><input class="form-control" type="text" name="info['+k+'][fname]" value=""></div></div><div class="col-mr-2"><label class="col-form-label">Teacher Last Name</label><div><input class="form-control" type="text" name="info['+k+'][lname]" value=""></div></div><div class="col-mr-2"><label class="col-form-label">Gender</label><div><input class="form-control" type="text" value="" name="info['+k+'][gender]"></div></div><div class="col-mr-2"><label class="col-form-label">Experience in years</label><div><input name="info['+k+'][exp]" class="form-control" type="text" value=""></div></div><div class="col-mr-2"><label class="col-form-label">Teaching Subjects</label><div><input name="info['+k+'][subject]" class="form-control" type="text" value=""></div></div><div class="col-md-2"><label class="col-form-label">Close</label><div class="form-group"><a class="delete remove-info" href="javascript:void(0);"><i class="fa fa-close"></i></a></div></div></div></td></tr>');
  });

  $(document).on('click', '.remove-info', function(){
       $(this).parents('tr').remove();
  });


    // Pricing add
        // function newMenuItem() {
        //     var newElem = $('tr.list-item').first().clone();
        //     newElem.find('input').val('');
        //     newElem.appendTo('table#item-add');
        // }
        // if ($("table#item-add").is('*')) {
        //     $('.add-item').on('click', function (e) {
        //         e.preventDefault();
        //         newMenuItem();
        //     });
        //     $(document).on("click", "#item-add .delete", function (e) {
        //         e.preventDefault();
        //         $(this).parent().parent().parent().parent().remove();
        //     });
        // }
        // function newMenuItem1() {
        //     var newElem = $('tr.list-item1').first().clone();
        //     newElem.find('input').val('');
        //     newElem.appendTo('table#item-add1');
        // }
        // if ($("table#item-add1").is('*')) {
        //     $('.add-item1').on('click', function (e) {
        //         e.preventDefault();
        //         newMenuItem1();
        //     });
        //     $(document).on("click", "#item-add1 .delete", function (e) {
        //         e.preventDefault();
        //         $(this).parent().parent().parent().parent().remove();
        //     });
        // }
        //     function newTime() {
        //     var newElem = $('tr.list-item2').first().clone();
        //     newElem.find('input').val('');
        //     newElem.appendTo('table#item-add2');
        // }
        // if ($("table#item-add2").is('*')) {
        //     $('.add-item2').on('click', function (e) {
        //         e.preventDefault();
        //         newTime();
        //     });
        //     $(document).on("click", "#item-add2 .delete", function (e) {
        //         e.preventDefault();
        //         $(this).parent().parent().parent().parent().remove();
        //     });
        // }
    </script>
@endsection
