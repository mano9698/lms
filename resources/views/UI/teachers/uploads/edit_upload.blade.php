@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Edit File/Videos</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Edit File/Videos</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>Add Uploads</h4> --}}
                    </div>
                    <div class="widget-inner">
                        <form method="POST" action="/teachers/uploads/update_uploads" enctype="multipart/form-data">
                            <div class="row">
                                @if(session('message'))
                                    <div class="alert alert-success">
                                        <ul>
                                            <li>{!! session('message') !!}</li>
                                        </ul>
                                    </div>
                                @endif


                                    @csrf
                                    <div class="form-group col-12">
                                        <label class="col-form-label">Select the File Type</label>
                                        <div class="radio-list d-flex">
                                            <div class="custom-control custom-radio radio-st1">

                                                <input type="hidden" name="id" value="{{$Uploads->id}}">

                                                <input type="radio" class="custom-control-input" id="check1" value="1" name="file_type" @if($Uploads->file_type == 1) checked @endif>
                                                <label class="custom-control-label ml-2" for="check1"> <i class="fa fa-file-text-o"></i> File</label>
                                            </div>
                                            <div class="custom-control custom-radio radio-st1 ml-4">
                                                <input type="radio" class="custom-control-input" id="check2" value="2" name="file_type" @if($Uploads->file_type == 2) checked @endif>
                                                <label class="custom-control-label ml-2" for="check2"> <i class="fa fa-file-video-o"></i> Video</label>
                                            </div>
                                        </div>
                                    </div>

                            </div>

                            <div class="row" @if($Uploads->video_type == 2) @else style="display: none;" @endif  id="Video">
                                <div class="form-group col-6">
                                    <label class="col-form-label">File Name</label>
                                    <div>
                                        <input class="form-control" type="text" id="file_name" value="{{$Uploads->file_name}}" name="file_name" >
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Video Type</label>
                                    <div>
                                        <select class="form-control" name="video_type" id="video_type" onchange="choose_video_type()">
                                            <option value="1" @if($Uploads->video_type == 2) selected @endif>Upload Video</option>
                                            <option value="2" @if($Uploads->video_type == 1) selected @endif>Paste Video Link</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group col-6" @if($Uploads->video_type == 2) @else style="display: none;" @endif id="upload_video">
                                    <label class="col-form-label">Upload File</label>
                                    {{-- <div>
                                        <input class="form-control" type="file" value="">
                                    </div> --}}

                                        {{-- <input type="file" name="upload_file" data-method="ajax"> --}}
                                        <input type="file" name="upload_videos">
                                </div>

                                <div class="form-group col-6" @if($Uploads->video_type == 1) @else style="display: none;" @endif id="video_link">
                                    <label class="col-form-label">Paste Video Link</label>
                                    {{-- <div>
                                        <input class="form-control" type="file" value="">
                                    </div> --}}
                                    <div>
                                        <input type="text" class="form-control" name="video_link" value="{{$Uploads->video_link}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row" @if($Uploads->file_type == 1) @else style="display: none;" @endif id="File">

                                <div class="form-group col-6">
                                    <label class="col-form-label">File Name</label>
                                    <div>
                                        <input class="form-control" type="text" id="file_name" value="{{$Uploads->file_name}}" name="file_names">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Upload File</label>
                                    {{-- <div>
                                        <input class="form-control" type="file" value="">
                                    </div> --}}

                                        {{-- <input type="file" name="upload_file" data-method="ajax"> --}}
                                        <input type="file" name="upload_file">
                                </div>

                            </div>

                            <div class="col-12">
                                <!--<button type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Item</button>-->
                                <img src="/UI/loader.gif" alt="" id="loader" style="
                                width: 80px;
                                display:none;
                            ">
<br>
                                <button type="submit" class="btn">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection

@section('JSScript')
<script>
    $(document).ready(function(){
        $('input[type=file]').drop_uploader({
            uploader_text: 'Drop files to upload, or',
            browse_text: 'Browse',
            only_one_error_text: 'Only one file allowed',
            not_allowed_error_text: 'File type is not allowed',
            big_file_before_error_text: 'Files, bigger than',
            big_file_after_error_text: 'is not allowed',
            allowed_before_error_text: 'Only',
            allowed_after_error_text: 'files allowed',
            browse_css_class: 'button button-primary',
            browse_css_selector: 'file_browse',
            uploader_icon: '<i class="pe-7s-cloud-upload"></i>',
            file_icon: '<i class="pe-7s-file"></i>',
            progress_color: '#4a90e2',
            time_show_errors: 5,
            layout: 'list',
            method: 'normal',
            // url: 'ajax_upload.php',
            // delete_url: 'ajax_delete.php',
        });
    });


    $(document).ready(function() {
    $("#Submit_Video").click(function() {
      // disable button
      $(this).prop("disabled", true);
      $('#loader').show();

    });
});

$(document).ready(function() {
    $('input:radio[name=file_type]').change(function() {
        if (this.value == '1') {
             $("#File").show();
            $("#Video").hide();
        }
        else if (this.value == '2') {
            $("#Video").show();
            $("#File").hide();
        }
    });
});


function choose_video_type(){
    var video_type = $("#video_type").val();


    if(video_type == 1){
        $("#upload_video").show();
        $("#video_link").hide();
    }else{
        $("#upload_video").hide();
        $("#video_link").show();
    }
}
</script>

@endsection
