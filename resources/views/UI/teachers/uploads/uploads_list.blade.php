@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">File/Videos</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>File/Videos</li>
            </ul>
        </div>
        <!-- Card -->

        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>My Uploads</h4> --}}
                    </div>
                    <div class="email-wrapper">

                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                {{-- <div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div> --}}
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/>
                                </div>
<div class="dropdown all-msg-toolbar">
{{-- <span class="btn btn-info-icon" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></span> --}}
                                    {{-- <ul class="dropdown-menu">
                                        <li><a href="#"><i class="fa fa-arrow-up"></i> Activate</a></li>
                                        <li><a href="#"><i class="fa fa-arrow-down"></i> Deactivate</a></li>
<li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                    </ul> --}}
<a href="/teachers/uploads/add_uploads" type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Files</a>
                                </div>
                                {{-- <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div> --}}
                            </div>
                            <div class="mail-box-list">
                                @if($Uploads)
                                    @foreach($Uploads as $Upload)
                                    <div class="mail-list-info">
                                        <div class="checkbox-list">
                                            <div class="custom-control custom-checkbox checkbox-st1">
                                                <input type="checkbox" class="custom-control-input" id="check2">
                                                <label class="custom-control-label" for="check2"></label>
                                            </div>
                                        </div>
                                        @if($Upload->file_type == 1)
                                            <div class="mail-rateing">
                                                <span><i class="fa fa-file-text-o"></i></span>
                                            </div>

                                            <div class="mail-list-title-info">
                                                <p><a href="/UI/uploads/{{$Upload->files}}" target="_blank">{{$Upload->file_name}}</a></p>
                                            </div>
                                            <?php
                                                $CheckFileDownloads= DB::table('total_file_downloads')->where('file_id', $Upload->id)->count();
                                            ?>
                                            <div class="mail-list-title-info">
                                                <p>Downloads - {{$CheckFileDownloads}}</p>
                                            </div>

                                        @elseif($Upload->file_type == 2)
                                            <div class="mail-rateing">
                                                <span><i class="fa fa-video-camera"></i></span>
                                            </div>

                                            <div class="mail-list-title-info">
                                                <p><a href="{{$Upload->files}}" target="_blank">{{$Upload->file_name}}</a></p>
                                            </div>

<div class="mail-list-title-info">
    <a href="{{$Upload->files}}" target="_blank" style="color:#000!important"><i class="fa fa-play" target="_blank"></i> Play</a>
</div>

                                            <?php
                                                $CheckTotalVideoViews = DB::table('total_video_views')->where('video_id', $Upload->id)->count();
                                            ?>


                                            <div class="mail-list-title-info">
                                                <p>Views - {{$CheckTotalVideoViews}}</p>
                                            </div>
                                        @endif

                                        {{-- <div class="mail-list-time">
                                            <span>10:59 AM</span>
                                        </div> --}}
                                        <ul class="mailbox-toolbar">
                                            <a href="/teachers/uploads/add_groups/{{$Upload->id}}">
                                                <li data-toggle="tooltip" title="Group Lists"><i class="fa fa-users"></i></li>
                                            </a>

                                            <a href="/teachers/uploads/add_students/{{$Upload->id}}">
                                                <li data-toggle="tooltip" title="Add Students"><i class="fa fa-user-o"></i></li>
                                            </a>

                                            <a href="/teachers/uploads/edit_uploads/{{$Upload->id}}">
                                                <li data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></li>
                                            </a>

                                            <a href="javascript:void(0);" class="delete_uploads" data-id="{{$Upload->id}}">
                                                <li data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></li>
                                            </a>
                                        </ul>
                                    </div>
                                    @endforeach
                                @else

                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>

@endsection
