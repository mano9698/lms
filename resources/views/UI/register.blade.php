<!DOCTYPE html>
<html lang="en">

<head>

	<!-- META ============================================= -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta name="robots" content="" />

	<!-- DESCRIPTION -->
	<meta name="description" content="Welcome to Skillsgroom" />

	<!-- OG -->
	<meta property="og:title" content="Welcome to Skillsgroom" />
	<meta property="og:description" content="Welcome to Skillsgroom" />
	<meta property="og:image" content="" />
	<meta name="format-detection" content="telephone=no">

	<!-- FAVICONS ICON ============================================= -->
	<link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
	<link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico" />

	<!-- PAGE TITLE HERE ============================================= -->
	<title>Welcome to Skillsgroom </title>

	<!-- MOBILE SPECIFIC ============================================= -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!--[if lt IE 9]>
	<script src="assets/js/html5shiv.min.js"></script>
	<script src="assets/js/respond.min.js"></script>
	<![endif]-->

	<!-- All PLUGINS CSS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/assets.css')}}">

	<!-- TYPOGRAPHY ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/typography.css')}}">

	<!-- SHORTCODES ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/shortcodes/shortcodes.css')}}">

	<!-- STYLESHEETS ============================================= -->
	<link rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/style.css')}}">
	<link class="skin" rel="stylesheet" type="text/css" href="{{URL::asset('UI/css/color/color-1.css')}}">

</head>
<body id="bg">
<div class="page-wraper">
	<div id="loading-icon-bx"></div>
	<div class="account-form">
		<div class="account-head" style="background-image:url(assets/images/bg2.jpg);">
			<a href="#"><img src="assets/images/logo-white.png" alt=""></a>
		</div>
		<div class="account-form-inner">
			<div class="account-container">
				<div class="heading-bx left">
					<h2 class="title-head">Sign Up <span>Now</span></h2>
					<p>Login Your Account <a href="/">Click here</a></p>
                </div>
                @if(session('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! session('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <form class="contact-bx" action="/add_users" method="post">
                    @csrf
					<div class="row placeani">
						{{-- <div class="col-lg-12">
							<div class="form-group">
								<div class="input-group">
									<select name="user_type" type="text" required="" class="form-control">
										<option selected disabled>Select Your User Type</option>
										<option value="2">Teacher</option>
                                        <option value="3">Students</option>
                                        <option value="4">Affiliate</option>
                                    </select>
								</div>
							</div>
						</div> --}}
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<label>First Name</label>
									<input name="first_name" type="text" required="" class="form-control">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<label>Last Name</label>
									<input name="last_name" type="text" required="" class="form-control">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<label>Email Address</label>
									<input name="email" type="email" required="" class="form-control">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<label>Contact Number</label>
									<input name="mobile" type="text" required="" class="form-control">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<label>Country</label>
									<input name="country" type="text" required="" class="form-control">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<label>State</label>
									<input name="state" type="text" required="" class="form-control">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<label>City</label>
									<input name="city" type="text" required="" class="form-control">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<label>Pincode</label>
									<input name="pincode" type="text" required="" class="form-control">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<label>Area</label>
									<input name="area" type="text" required="" class="form-control">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<div class="input-group">
									<label>Password</label>
									<input name="password" type="password" class="form-control" required="">
								</div>
							</div>
						</div>
						<div class="col-lg-12 m-b30">
							<button name="submit" type="submit" value="Submit" class="btn button-md">Sign Up</button>
						</div>
						<!--<div class="col-lg-12">
							<h6>Sign Up with Social media</h6>
							<div class="d-flex">
								<a class="btn flex-fill m-r5 facebook" href="#"><i class="fa fa-facebook"></i>Facebook</a>
								<a class="btn flex-fill m-l5 google-plus" href="#"><i class="fa fa-google-plus"></i>Google Plus</a>
							</div>
						</div>-->
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- External JavaScripts -->
<script src="{{URL::asset('UI/js/jquery.min.js')}}"></script>
<script src="{{URL::asset('UI/vendors/bootstrap/js/popper.min.js')}}"></script>
<script src="{{URL::asset('UI/vendors/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('UI/vendors/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{URL::asset('UI/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js')}}"></script>
<script src="{{URL::asset('UI/vendors/magnific-popup/magnific-popup.js')}}"></script>
<script src="{{URL::asset('UI/vendors/counter/waypoints-min.js')}}"></script>
<script src="{{URL::asset('UI/vendors/counter/counterup.min.js')}}"></script>
<script src="{{URL::asset('UI/vendors/imagesloaded/imagesloaded.js')}}"></script>
<script src={{URL::asset('UI/vendors/masonry/masonry.js')}}"></script>
<script src="{{URL::asset('UI/vendors/masonry/filter.js')}}"></script>
<script src="{{URL::asset('UI/vendors/owl-carousel/owl.carousel.js')}}"></script>
<script src="{{URL::asset('UI/js/functions.js')}}"></script>
<script src="{{URL::asset('UI/js/contact.js')}}"></script>

</body>

</html>
