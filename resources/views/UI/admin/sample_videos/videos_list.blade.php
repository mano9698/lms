@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Teachers Sample Videos</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Teachers Sample Videos</li>
            </ul>
        </div>
        <!-- Card -->

        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Sample Videos</h4>
                    </div>
                    <div class="email-wrapper">

                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/>
                                </div>
<div class="dropdown all-msg-toolbar">
<span class="btn btn-info-icon" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></span>
                                    <ul class="dropdown-menu">
                                        <li><a href="#"><i class="fa fa-arrow-up"></i> Activate</a></li>
                                        <li><a href="#"><i class="fa fa-arrow-down"></i> Deactivate</a></li>
<li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                    </ul>
<a href="/sample_videos/add_sample_videos" type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Files</a>
                                </div>
                                <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                            @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                            <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>Video Name</th>
                      <th>Teacher Name</th>

                        <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if($Videos)
                      @foreach($Videos as $Video)
                    <tr>
                    <td>{{ $Video->file }}</td>
                      <td><strong>{{ $Video->first_name }} {{ $Video->last_name }}</strong></td>

<td><ul class="mailbox-toolbar" style="position: inherit;">
     <a href="/sample_videos/edit_sample_videos/{{$Video->id}}">
        <li data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></li>
     </a>
     <a href="/sample_videos/delete_sample_videos/{{$Video->id}}" onclick="return confirm(' Are you sure. You want to delete?');">
        <li data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></li>
    </a>


                        </ul></td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td>No Data Found...</td>
                    </tr>
                    @endif
                  </tbody>
                </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
