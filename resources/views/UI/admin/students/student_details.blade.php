@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Teacher Details</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Teacher Details</li>
            </ul>
        </div>	
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="widget-inner">
                        <div class="card-courses-list admin-courses">
                            <div class="card-courses-media">
                                @if($Users->profile_pic)
                                    <img src="/UI/students_profile_pic/{{$Users->profile_pic}}" alt=""/>
                                @else
                                    <img src="{{URL::asset('UI/user.jpg')}}" alt=""/>
                                @endif
                                <!--<div class="card-courses-title">
                                    <p><strong>Name:</strong> Anna Mathew<br>
                                    <strong>Email:</strong> anna@gmail.com<br>
                                    <strong>Contact No:</strong> +91-9876543210<br>
                                    <strong>Country:</strong> India<br>
                                    <strong>State:</strong> Karnataka<br>
                                    <strong>City:</strong> Bangalore<br>
                                    <strong>Pincode:</strong> 560078<br>
                                    <strong>Area:</strong> HSR Layout</p>
                                </div>-->
                            </div>
                            <div class="card-courses-full-dec">
                                <div class="card-courses-title">
                                    <p><strong>Name:</strong> {{$Users->first_name}} {{$Users->last_name}}<br>
                                    <strong>Gender:</strong> {{$Users->gender}}<br>
                                    <strong>Date Of Birth:</strong> {{$Users->dob}}<br>
                                        
<strong>Age in Years:</strong> {{$Users->age}}<br>
<strong>Grade:</strong> {{$Users->grade}}<br>
<strong>School/College Name:</strong> {{$Users->school_clg_name}}<br>
<strong>School Board:</strong> {{$Users->school_board}}<br>
<strong>Language known:</strong> {{$Users->language_known}}<br>
<strong>Favorite Subject:</strong> {{$Users->fav_subject}}<br>
<strong>Favorite Sport:</strong> {{$Users->fav_sport}}<br>
<strong>Favorite Musical Instrument:</strong> {{$Users->fav_music}}</p>
                                </div>
                                
                            
                                
                            </div>
                        </div>
                        
                        
                        
                        
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection