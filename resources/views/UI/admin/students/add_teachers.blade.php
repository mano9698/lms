@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Students Insertion</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Add Students</li>
            </ul>
        </div>	
        <!-- Card -->
        
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Add the students to the course</h4>
                    </div>
                    <div class="email-wrapper">
                        
                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <!--{{-- <div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div> --}}-->
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/> 										
                                </div>
    <div class="dropdown all-msg-toolbar">
                <span class="btn btn-info-icon" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></span>
                                    <ul class="dropdown-menu">											
                                        <li><a href="#"><i class="fa fa-arrow-up"></i> Activate</a></li>
                                        <li><a href="#"><i class="fa fa-arrow-down"></i> Deactivate</a></li>
<li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                    </ul>

                                </div>
                                {{-- <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div> --}}
                            </div>
                            <div class="mail-box-list">
                                <div class="mail-list-info">

                                    <div class="mail-list-time col-lg-3">
                                        <span><p><strong>Name</strong></p></span>
                                    </div>
                                    <div class="mail-list-title-info col-lg-3">
                                        <p><strong>Role</strong></p>
                                    </div>
                                    {{-- <div class="mail-list-title-info col-lg-3">
                                        <p><strong>Date</strong></p>
                                    </div> --}}


                                </div>
                                <input type="hidden" id="student_id" value="{{$Users->id}}">
                                @foreach($TeachersList as $Teachers)
                                <div class="mail-list-info">

                                    <div class="mail-list-time col-lg-3">
                                    <span>{{$Teachers->first_name}} {{$Teachers->last_name}}</span>
                                    </div>
                                    <div class="mail-list-title-info col-lg-3">
                                        @if($Teachers->user_type == 1)
                                            <p>Instructor</p>
                                        @elseif($Teachers->user_type == 2)
                                            <p>Teacher</p>
                                        @elseif($Teachers->user_type == 3)
                                            <p>Student</p>
                                        @endif
                                    </div>
                                    {{-- <div class="mail-list-title-info col-lg-3">
                                        <p>10/29/2020</p>
                                    </div> --}}

                                    <ul class="mailbox-toolbar">
                                        <?php 

                                            $CheckTeachers = DB::table('add_teachers_to_students')    
                                                            ->where('student_id', $Users->id)
                                                            ->where('teacher_id', $Teachers->id)
                                                            ->first();

                                                            // echo json_encode($CheckStudents);
                                                            
                                        ?>
                                        @if($CheckTeachers)
                                            <a href="javascript:void(0);" data-id="{{$Teachers->id}}" class="Remove_Teachers" id="Remove_Teachers">
                                            <li data-toggle="tooltip" title="View"><i class="fa fa-minus-circle fa-2x"></i></li>
                                        </a>
                                        @else
                                            <a href="javascript:void(0);" data-id="{{$Teachers->id}}" class="Add_Teachers" id="Add_Teachers">
                                            <li data-toggle="tooltip" title="View"><i class="fa fa-plus-circle fa-2x"></i></li>
                                        </a>
                                            
                                        @endif
                                    </ul>
                                    
                                </div>								
                                @endforeach
                                
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection



@section('JSScript')
<script>
    // Pricing add
        function newMenuItem() {
            var newElem = $('tr.list-item').first().clone();
            newElem.find('input').val('');
            newElem.appendTo('table#item-add');
        }

                // Get last id 
        // var lastname_id = $('.list-item input[type=text]:nth-child(1)').last().attr('id');
        // var split_id = lastname_id.split('_');

        // // New index
        // var index = Number(split_id[1]) + 1;
        
        if ($("table#item-add").is('*')) {
            $('.add-item').on('click', function (e) {
                e.preventDefault();
                newMenuItem();
            });
            $(document).on("click", "#item-add .delete", function (e) {
                e.preventDefault();
                $(this).parent().parent().parent().parent().remove();
            });
        }
    </script>


<script>

    $(function() {
      
      $('input[type=text]').autocomplete({
   
           source: function(request, response) {
               $.ajax({
               url: "{{url('courses/autocomplete')}}",
               data: {
                       term : request.term
                },
               dataType: "json",
               success: function(data){
                  var resp = $.map(data,function(obj){
                       console.log(obj);
                       return obj.first_name;
                  }); 
   
                  response(resp);
               }
           });
       },
      
    }); 
   }); 


// $('.search').on("focus", function() {
//     var id = this.id;


//     $('#'+id).autocomplete({
//         source: function(request, response) {
//                $.ajax({
//                url: "{{url('courses/autocomplete')}}",
//                data: {
//                        term : request.term
//                 },
//                dataType: "json",
//                success: function(data){
//                   var resp = $.map(data,function(obj){
//                        console.log(obj);
//                        return obj.first_name;
//                   }); 
   
//                   response(resp);
//                }
//            });
//        },
//     }); 
// }); 
   
   </script>  
@endsection