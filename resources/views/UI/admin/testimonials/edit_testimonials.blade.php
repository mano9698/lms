@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Testimonials</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Edit Testimonials</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Edit Testimonials</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/testimonials/update_testimonials" method="POST">
                            @csrf
                            <div class="row">

                                <div class="form-group col-6">
                                    <label class="col-form-label">Select Teacher</label>
                                    <div>
                                        <input type="hidden" name="id" value="{{ $Testimonials->id }}">
                                        <select name="teacher_id">
                                            @foreach($Teachers as $Teacher)
                                                <option @if($Testimonials->teacher_id == $Teacher->id) selected @endif value="{{ $Teacher->id }}">{{ $Teacher->first_name }} {{ $Teacher->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Select Student</label>
                                    <div>
                                        <select name="student_id">
                                            @foreach($Students as $Student)
                                                <option @if($Testimonials->student_id == $Student->id) selected @endif value="{{ $Student->id }}">{{ $Student->first_name }} {{ $Student->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Testimonial</label>
                                    <div>
                                        <textarea class="form-control" name="testimonial">{{ $Testimonials->testimonial }}</textarea>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <!--<button type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Item</button>-->
                                    <button type="submit" class="btn">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
