
@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Payment Tracker</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Edit Payment Transferred</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Edit Payment Transferred</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/payments/transferred/update_payment_transferred" method="POST">
                            @csrf
                            <div class="row">
                            <div class="form-group col-6">
                                    <label class="col-form-label">Select Affiliate</label>
                                    <input class="form-control" name="id" type="hidden" value="{{ $PaymentTransferred->id }}">
                                    <div>
                                        <select class="form-control" name="affiliate" type="text" value="">
                                            @foreach($Affiliates as $Affiliatess)
                                            <option value="{{ $Affiliatess->id }}" @if($PaymentTransferred->affiliate == $Affiliatess->id) selected @endif>{{ $Affiliatess->first_name }} {{ $Affiliatess->last_name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Commission Month</label>
                                    <div>
                                        <select class="form-control" name="commision_month" type="text">

                                            <option value="Janaury" @if($PaymentTransferred->commision_month == "Janaury") selected @endif>Janaury</option>
                                            <option value="February" @if($PaymentTransferred->commision_month == "February") selected @endif>February</option>
                                            <option value="March" @if($PaymentTransferred->commision_month == "March")selected @endif>March</option>
                                            <option value="April" @if($PaymentTransferred->commision_month == "April")selected @endif>April</option>
                                            <option value="May" @if($PaymentTransferred->commision_month == "May")selected @endif>May</option>
                                            <option value="June" @if($PaymentTransferred->commision_month == "June")selected @endif>June</option>
                                            <option value="July" @if($PaymentTransferred->commision_month == "July")selected @endif>July</option>
                                            <option value="August" @if($PaymentTransferred->commision_month == "August")selected @endif>August</option>
                                            <option value="September" @if($PaymentTransferred->commision_month == "September")selected @endif>September</option>
                                            <option value="October" @if($PaymentTransferred->commision_month == "October")selected @endif>October</option>
                                            <option value="November" @if($PaymentTransferred->commision_month == "November")selected @endif>November</option>
                                            <option value="December" @if($PaymentTransferred->commision_month == "December")selected @endif>December</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Total Collection</label>
                                    <div>
                                        <input class="form-control" name="total_collection" type="text" value="{{ $PaymentTransferred->total_collection }}">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Commission Due Amount</label>
                                    <div>
                                        <input class="form-control" name="commission_due_date" type="text" value="{{ $PaymentTransferred->commission_due_date }}">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Commission Paid Amount</label>
                                    <div>
                                        <input class="form-control" name="commission_paid_amount" type="text" value="{{ $PaymentTransferred->commission_paid_amount }}">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Paid Date</label>
                                    <div>
                                        <input class="form-control" name="paid_date" type="text" value="{{ $PaymentTransferred->paid_date }}">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Payment Mode</label>
                                    <div>
                                        <input class="form-control" name="payment_mode" type="text" value="{{ $PaymentTransferred->payment_mode }}">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Payment Ref Number</label>
                                    <div>
                                        <input class="form-control" name="payment_ref_no" type="text" value="{{ $PaymentTransferred->payment_ref_no }}">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <button type="submit" class="btn">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
