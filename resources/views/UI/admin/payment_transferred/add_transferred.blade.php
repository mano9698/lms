@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Payment Tracker</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Add Payment Transferred</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Add Payment Transferred</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/payments/transferred/store_payment_transferred" method="POST">
                            @csrf
                            <div class="row">
                            <div class="form-group col-6">
                                    <label class="col-form-label">Select Affiliate</label>
                                    <div>
                                        <select class="form-control" name="affiliate" type="text" value="">
                                            <option selected disabled>Select Affiliate</option>
                                            @foreach($Affiliates as $Affiliatess)
                                                <option value="{{ $Affiliatess->id }}">{{ $Affiliatess->first_name }} {{ $Affiliatess->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Commission Month</label>
                                    <div>
                                        <select class="form-control" name="commision_month" type="text">
                                            <option selected disabled>Select Commission Month</option>
                                            <option value="Janaury">Janaury</option>
                                            <option value="February">February</option>
                                            <option value="March">March</option>
                                            <option value="April">April</option>
                                            <option value="May">May</option>
                                            <option value="June">June</option>
                                            <option value="July">July</option>
                                            <option value="August">August</option>
                                            <option value="September">September</option>
                                            <option value="October">October</option>
                                            <option value="November">November</option>
                                            <option value="December">December</option>
                                        </select>

                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Total Collection</label>
                                    <div>
                                        <input class="form-control" name="total_collection" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Commission Due Amount</label>
                                    <div>
                                        <input class="form-control" name="commission_due_date" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Commission Paid Amount</label>
                                    <div>
                                        <input class="form-control" name="commission_paid_amount" type="text" value="">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Paid Date</label>
                                    <div>
                                        <input class="form-control" name="paid_date" type="text" value="">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Payment Mode</label>
                                    <div>
                                        <input class="form-control" name="payment_mode" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Payment Ref Number</label>
                                    <div>
                                        <input class="form-control" name="payment_ref_no" type="text" value="">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <button type="submit" class="btn">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
