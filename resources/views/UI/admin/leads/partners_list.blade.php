@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Partners</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Partners</li>
            </ul>
        </div>	
        <!-- Card -->
        
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        {{-- <h4>Partners</h4> --}}
                    </div>
                    <div class="email-wrapper">
                        
                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <!--{{-- <div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div> --}}-->
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/> 										
                                </div>
<div class="dropdown all-msg-toolbar">
<span class="btn btn-info-icon" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></span>
                                    <ul class="dropdown-menu">											
                                        <li><a href="#"><i class="fa fa-arrow-up"></i> Activate</a></li>
                                        <li><a href="#"><i class="fa fa-arrow-down"></i> Deactivate</a></li>
<li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                    </ul>
{{-- <a href="/teachers/conference/add_conference" type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Teachers</a> --}}
                                </div>
                                {{-- {{-- <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div> --}}
                            </div>
                            <div class="table-responsive">
                            <table class="table public-user-block block">
                      <thead>
                        <tr>
                          {{-- <th><div class="custom-control custom-checkbox checkbox-st1">
                                                <input type="checkbox" class="custom-control-input" id="check">
                                                <label class="custom-control-label" for="check"></label>
                                            </div>
                                        </th>                           --}}
                          <th>Name</th>
                          <th>Email</th>
                          <th>Mobile</th>
                          <th>State / City </th>
                          <th>Tuition</th>
                          <th>Subjects</th>
                          <th>Standard</th>
                          <th>Education</th>
                          <th>No Of Students</th>
                          <th>Website</th>
                          {{-- <th>Area / Pincode</th> --}}
                          {{-- <th></th> --}}
                        </tr>
                      </thead>
                      <tbody>
                          @if($Partners)
                                @foreach($Partners as $Partner)
                        <tr>
                          {{-- <th scope="row"><div class="custom-control custom-checkbox checkbox-st1">
                                                <input type="checkbox" class="custom-control-input" id="check2">
                                                <label class="custom-control-label" for="check2"></label>
                                            </div></th> --}}
                          <td><strong>{{$Partner->name}}</strong></td>
                          <td>{{$Partner->email}}</td>
                          <td>{{$Partner->mobile}}</td>
                          <td>{{$Partner->state}} / {{$Partner->city}}</td>
                          <td>{{$Partner->tution}}</td>
                          <td>{{$Partner->subjects}}</td>
                          <td>{{$Partner->standard}}</td>
                          <td>{{$Partner->education}}</td>
                          <td>{{$Partner->no_of_students}}</td>
                          <td>{{$Partner->website}}</td>

                        </tr>
                        @endforeach
                                @else
                                <h4>No Data Found</h4>
                                @endif
                      </tbody>
                    </table>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection

