@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Teachers</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="/teachers/dashboard"><i class="fa fa-home"></i>Home</a></li>
                <li>Teachers</li>
            </ul>
        </div>	
        <!-- Card -->
        
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Teachers List</h4>
                    </div>
                    <div class="email-wrapper">
                        
                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <!--{{-- <div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div> --}}-->
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/> 										
                                </div>
<div class="dropdown all-msg-toolbar">
<span class="btn btn-info-icon" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></span>
                                    <ul class="dropdown-menu">											
                                        <li><a href="#"><i class="fa fa-arrow-up"></i> Activate</a></li>
                                        <li><a href="#"><i class="fa fa-arrow-down"></i> Deactivate</a></li>
<li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                    </ul>
{{-- <a href="/teachers/conference/add_conference" type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Teachers</a> --}}
                                </div>
                                {{-- <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div> --}}
                            </div>
                            <div class="table-responsive">
                            <table class="table public-user-block block">
                      <thead>
                        <tr>
                          <th><div class="custom-control custom-checkbox checkbox-st1">
                                                <input type="checkbox" class="custom-control-input" id="check">
                                                <label class="custom-control-label" for="check"></label>
                                            </div>
                                        </th>                          
                          <th>Name</th>
                          <th>Email</th>
                          <th>Mobile</th>
                          <th>Country / State / City</th>
                          {{-- <th>Area / Pincode</th> --}}
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          @if($Users)
                                @foreach($Users as $User)
                        <tr>
                          <th scope="row"><div class="custom-control custom-checkbox checkbox-st1">
                                                <input type="checkbox" class="custom-control-input" id="check2">
                                                <label class="custom-control-label" for="check2"></label>
                                            </div></th>
                          <td><strong>{{$User->first_name}} {{$User->last_name}}</strong></td>
                          <td>{{$User->email}}</td>
                          <td>{{$User->mobile}}</td>
                          <td>{{$User->country}} / {{$User->state}} / {{$User->city}}</td>
                          {{-- <td>{{$User->area}} / {{$User->pincode}}</td> --}}
                          <td>
                            <input data-id="{{$User->id}}" class="toggle-class" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $User->status ? 'checked' : '' }}>
                         </td>
                         <td>
                            <?php 
                            $CheckTeacherProfile  = DB::table('users')->where('users.id', $User->id)
                                                    ->join('teacher_personal_details', 'teacher_personal_details.teacher_id', '=', 'users.id')
                                                            ->first();
                        ?>
                        @if($CheckTeacherProfile)
                            <a href="/admin/teachers_details/{{$User->id}}" class="btn" target="_blank">Click Here For Teacher Details</a>
                        @else

                        @endif
                             
                        </td>
                          {{-- <td class="mail-list-info" style="
                          width: 100px;
                      ">
<div class="mail-list-time">
                                            <span></span>
                                        </div>
                                        <ul class="mailbox-toolbar">

                                         <a href="/teachers/conference/edit_conference/{{$User->id}}">
                                            <li data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></li>
                                        </a> 

                                        <a href="javascript:void(0);" class="delete_conference" data-id="{{$User->id}}">
                                            <li data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></li>
                                        </a>
                                        </ul>
                          </td> --}}

                        </tr>
                        @endforeach
                                @else

                                @endif
                      </tbody>
                    </table>
                </div>
                        </div>
                    </div>
                </div> 
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection



@section('JSScript')
<script>
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

  


    $(function() {
      $('.toggle-class').change(function() {
          var status = $(this).prop('checked') == true ? 1 : 0; 
          var id = $(this).data('id'); 
           console.log(status);
          $.ajax({
              type: "POST",
              dataType: "json",
              url: '/admin/changeStatus',
              data: {'status': status, 'id': id},
              success: function(data){
                console.log(data.success)
                alert(data.success);
              }
          });
      })
    })
  </script>
@endsection