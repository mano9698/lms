@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Teacher Details</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Teacher Details</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="widget-inner">
                        <div class="card-courses-list admin-courses">
                            <div class="card-courses-media" style="height: auto;">
                                @if($Users->logo)
                                    <img src="/UI/teachers_profile_pic/{{$Users->profile_pic}}" alt=""/>
                                @else
                                    <img src="{{URL::asset('UI/user.jpg')}}" alt=""/>
                                @endif
                                <div class="card-courses-title">
                                    <p><strong>Name:</strong> {{$Users->first_name}} {{$Users->last_name}}<br>
                                    <strong>Email:</strong> <a href="mailto:{{$Users->email}}">{{$Users->email}}</a><br>
                                    <strong>Contact No:</strong> {{$Users->mobile}}<br>
                                    <strong>Country:</strong> {{$Users->country}}<br>
                                    <strong>State:</strong> {{$Users->state}}<br>
                                    <strong>City:</strong> {{$Users->city}}<br>
                                    <strong>Pincode:</strong> {{$Users->pincode}}<br>
                                    <strong>Area:</strong> {{$Users->area}}</p>
                                </div>
                            </div>
                            <div class="card-courses-full-dec">
                                <!--<div class="card-courses-title">
                                    <p><strong>Name:</strong> Anna Mathew<br>
                                    <strong>Email:</strong> anna@gmail.com<br>
                                    <strong>Contact No:</strong> +91-9876543210<br>
                                    <strong>Country:</strong> India<br>
                                    <strong>State:</strong> Karnataka<br>
                                    <strong>City:</strong> Bangalore<br>
                                    <strong>Pincode:</strong> 560078<br>
                                    <strong>Area:</strong> HSR Layout</p>
                                </div>-->

                                <div class="row card-courses-dec">
                                  <div class="col-md-12">
                                    <h6 class="m-b10"><strong><u>Personal details</u></strong></h6>
                                    <p><strong>Gender:</strong> {{$Users->gender}}<br>
<strong>Qualification:</strong> {{$Users->qualification}}<br>
<strong>Tutor type:</strong> {{$Users->tutor_type}}<br>
<strong>Teaching Subjects:</strong> {{$Users->teaching_subjects}}<br>
<strong>Teaching Language:</strong> {{$Users->teaching_language}}<br>
<strong>No students at present:</strong> {{$Users->no_of_student_present}}</p>
                                        <h6 class="m-b10"><u><b>Who are their students</b></u></h6>
                                    <p><strong>School /College Boards:</strong> {{$Users->school_clg_boards}}<br>
<strong>Students Grades / standard studying:</strong> {{$Users->student_grades_studying}}<br>
<strong>Students Age ( only if applicable ):</strong> {{$Users->student_age}}</p>
                                        <h6 class="m-b10"><u><b>Fee/Rate</b></u></h6>
                                        <div class="table-responsive">
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Grade</th>
                      <th>Subject</th>
                      <th> Hourly Rate</th>
                      <th>Monthly Rate</th>
                         <th>Yearly Rate</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                            $TeacherFees = DB::table('teacher_fees')
                                            ->where('teacher_id', $Users->teacher_id)
                                            ->get();
                        ?>
                        @if($TeacherFees)
                        @foreach($TeacherFees as $Fees)
                    <tr>
                        <th scope="row">{{$Fees->id}}</th>
                      <td>{{$Fees->grade}}</td>
                      <td>{{$Fees->subject}}</td>
                      <td>Rs. {{$Fees->hourly_rate}}</td>
                         <td>Rs. {{$Fees->monthly_rate}}</td>
                        <td>Rs. {{$Fees->yearly_rate}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td>No Found...</td>
                    </tr>

                    @endif
                  </tbody>
                </table>
                    </div>
                                            <h6 class="m-b10"><u><b>Tuition Timings</b></u></h6>
                                        <div class="table-responsive">
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Day</th>
                      <th>Time Option 1</th>
                      <th>Time Option 2</th>
                      <th>Time Option 3</th>
                         <th>Time Option 4</th>
                         <th>Time Option 5</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $TeacherTuition = DB::table('teacher_tuition')
                                    ->where('teacher_id', $Users->teacher_id)
                                    ->get();
                ?>
                 @if($TeacherTuition)
                 @foreach($TeacherTuition as $Tuition)
                    <tr>
                        <th scope="row">{{$Tuition->id}}</th>
                      <th>{{$Tuition->days}}</th>
                      <td>{{$Tuition->time_option1}}</td>
                      <td align="center">{{$Tuition->time_option2}}</td>
                      <td align="center">{{$Tuition->time_option3}}</td>
                        <td align="center">{{$Tuition->time_option4}}</td>
                        <td align="center">{{$Tuition->time_option5}}</td>
                    </tr>
                @endforeach
                @else
                    <tr>
                        <td>No Found...</td>
                    </tr>

                @endif
                  </tbody>
                </table>
                    </div>
                                    <h6 class="m-b10"><strong><u>Additional Information (Applicable if Coaching Classes)</u></strong></h6>
                                    <p><strong>Logo:</strong>
                                        @if($Users->logo)
                                            <img src="/UI/teachers_logo/{{$Users->logo}}" alt="" style="
                                            width: 250px;
                                        "/>
                                        @else
                                            <img src="{{URL::asset('UI/logo.jpg')}}" alt=""/>
                                        @endif
                                    </p>
                                    <p><strong>Total No Of Students:</strong> {{$Users->total_no_of_students}}</p>
                                      <div class="table-responsive">
                <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Teacher First Name</th>
                      <th>Teacher Last Name</th>
                      <th>Gender</th>
                      <th>Experience in years</th>
                       <th>Teaching Subjects</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $TeacherAdditional = DB::table('teachers_additional_info')
                                    ->where('teacher_id', $Users->teacher_id)
                                    ->get();
                ?>
                 @if($TeacherAdditional)
                 @foreach($TeacherAdditional as $Info)
                    <tr>
                        <th scope="row">{{$Info->id}}</th>
                      <td>{{$Info->first_name}}</td>
                      <td>{{$Info->last_name}}</td>
                      <td>{{$Info->gender}}</td>
                        <td>{{$Info->experience}}</td>
                        <td>{{$Info->subjects}}</td>
                    </tr>

                @endforeach
                @else
                    <tr>
                        <td>No Found...</td>
                    </tr>

                @endif
                  </tbody>
                </table>
                                      </div>

                                      {{-- <h3>Change User Type</h3>
                                      <input type="hidden" value="{{ Request::segment(3) }}" name="" id="UserId">
                                      <select name="" id="UserTypeId" onchange="ChangeUsertype()">
                                        <option value="">Select Data</option>
                                          @foreach($UserTypes as $Types)
                                            <option  value="{{ $Types->id }}">{{ $Types->name }}</option>
                                          @endforeach
                                      </select> --}}
                                    </div>

                                </div>

                            </div>
                        </div>




                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection

{{--
@section('JSScript')
    <script>
        function ChangeUsertype(){
            var UserId = $("#UserId").val();
          var UserTypeId = $("#UserTypeId").val();

          $.ajax({
              type: "POST",
              dataType: "json",
              url: '/admin/ChangeUserType',
              data: {'UserTypeId': UserTypeId, 'UserId': UserId},
              success: function(data){
                console.log(data.success)
                alert(data.success);
              }
          });
    }
    </script>
@endsection --}}
