@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">User type</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>User type List</li>
            </ul>
        </div>
        <!-- Card -->

        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>User type</h4>
                    </div>
                    <div class="email-wrapper">

                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search"/>
                                </div>
<div class="dropdown all-msg-toolbar">
<span class="btn btn-info-icon" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></span>
                                    <ul class="dropdown-menu">
                                        <li><a href="#"><i class="fa fa-arrow-up"></i> Activate</a></li>
                                        <li><a href="#"><i class="fa fa-arrow-down"></i> Deactivate</a></li>
<li><a href="#"><i class="fa fa-trash-o"></i> Delete</a></li>
                                    </ul>

                                </div>
                                <div class="next-prev-btn">
                                    <a href="#"><i class="fa fa-angle-left"></i></a>
                                    <a href="#"><i class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                            @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif

                            <table class="table public-user-block block">
                  <thead>
                    <tr>

                      <th>Name</th>
                        <th width="15%">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      @if($UserTypes)
                      @foreach($UserTypes as $User)
                      <tr>

                      <td><strong>{{ $User->name }}</strong></td>
<td><ul class="mailbox-toolbar" style="position: inherit;">

                                       {{-- <li data-toggle="tooltip" title="Edit">
                                             <a href="/user_type/view_user_types/{{ $User->id }}"><i class="fa fa-info"></i></a></li> --}}

                                             <li data-toggle="tooltip" title="Edit">
                                                <a href="/user_type/add_permission/{{ $User->id }}"><i class="fa fa-edit"></i></a></li>

                                            <li data-toggle="tooltip" title="Delete"><a href="/user_type/delete_user_type/{{$User->id}}" onclick="return confirm(' Are you sure. You want to delete?');"><i class="fa fa-trash-o"></i></a></li>
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td>No Data Found...</td>
                    </tr>
                    @endif
                  </tbody>
                </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
