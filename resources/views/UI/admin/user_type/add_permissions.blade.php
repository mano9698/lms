@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Add Permission</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Add Permission</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Add Permission</h4>
                    </div>
                    <div class="widget-inner">

                        <form class="edit-profile m-b30" action="/user_type/store_permissions" method="POST">
                            @csrf
                            <div class="row">

                                {{-- <div class="form-group col-6">
                                    <label class="col-form-label">User type default role</label>
                                    <div>
                                        <select name="roles">
                                        @foreach($UserTypes as $Users)
                                            <option>{{ $Users->name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="form-group col-6">
                                    <label class="col-form-label">Permission</label>
                                    <div>

                                        <!--<input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"> Administrator</label>-->


<div style="padding: 0 0 0 20px;">
    <input type="hidden" value="{{ Request::segment(3) }}" name="Usertypeid" id="">
    <select name="modules" id="modules" onchange="checkmodules()">
        <option value="1">Courses</option>
        <option value="2">Groups</option>
        <option value="3">Files/Videos</option>
        <option value="4">Online Class Link</option>
        <option value="5">Leads</option>
        <option value="6">Forum</option>
        <option value="7">Payment Tracker</option>
        <option value="8">Testimonials</option>
        <option value="9">Sample Video</option>
        <option value="10">Certification</option>
        <option value="11">Fee Collection</option>
        <option value="12">Teacher Profile</option>
        <option value="13">Student Profile</option>
        <option value="14">Affiliate Profile</option>
        <option value="15">Home Work</option>
    </select>

    <div id="CommonPermission">
        <input type="hidden" name="id" id="PermissionId">
    <input type="checkbox" class="PermissionClick" id="ViewOnly" name="view_only" value="1"> View<br>
            <input type="checkbox" class="PermissionClick" id="AddOnly" name="add_only" value="2"> Add<br>
        <input type="checkbox" class="PermissionClick" id="EditOnly" name="edit_only" value="3"> Update<br>
        <input type="checkbox" class="PermissionClick" id="DeleteOnly" name="delete_only" value="4"> Delete
    </div>

    <div id="LeadsPermission" style="display: none;">
        <input type="hidden" name="id" id="PermissionId">
    <input type="checkbox" class="PermissionClick" id="ViewOnly" name="view_only" value="1"> My Leads
    </div>

    <div id="HomeworkPermission" style="display: none;">
        <input type="hidden" name="id" id="PermissionId">
    <input type="checkbox" class="PermissionClick" id="ViewOnly" name="view_only" value="1"> View<br>
            <input type="checkbox" class="PermissionClick" id="AddOnly" name="add_only" value="2"> Add<br>
        <input type="checkbox" class="PermissionClick" id="EditOnly" name="edit_only" value="3"> View Homework
    </div>


    @if(session('message'))
    <div class="alert alert-success">
        <ul>
            <li>{!! session('message') !!}</li>
        </ul>
    </div>
@endif

    <table class="table public-user-block block">
        <thead>
          <tr>

            <th>Permission</th>
            <th>View</th>
            <th>Add</th>
            <th>Update</th>
            <th>Delete</th>
              <th width="15%">Action</th>
          </tr>
        </thead>
        <tbody>
            @if($UserPermissions)
            @foreach($UserPermissions as $Permissions)
            <tr>

            <td><strong>@if($Permissions->permissions == 1)
                Courses
                @elseif($Permissions->permissions == 2)
                Groups
                @elseif($Permissions->permissions == 3)
                Files/Videos
                @elseif($Permissions->permissions == 4)
                Online Class Link
                @elseif($Permissions->permissions == 5)
Leads
                @elseif($Permissions->permissions == 6)
                Forum
                @elseif($Permissions->permissions == 7)
Payment Tracker
                @elseif($Permissions->permissions == 8)
Testimonials
                @elseif($Permissions->permissions == 9)
Sample Video
                @elseif($Permissions->permissions == 10)
Certificate
                @elseif($Permissions->permissions == 11)
Fee Collection
                @elseif($Permissions->permissions == 12)
Teacher Profile
                @elseif($Permissions->permissions == 13)
                Student Profile
                @elseif($Permissions->permissions == 14)
                Affiliate Profile
                @elseif($Permissions->permissions == 15)
Home Work
                @endif
            </strong></td>
            <td>{{ $Permissions->view_only }}</td>
            <td>{{ $Permissions->add_only }}</td>
            <td>{{ $Permissions->edit_only }}</td>
            <td>{{ $Permissions->delete_only }}</td>
<td><ul class="mailbox-toolbar" style="position: inherit;">

                             <li data-toggle="tooltip" title="Edit">
                                   <a onclick="EditPermission({{$Permissions->id}})"><i class="fa fa-edit"></i></a></li>

                                  <li data-toggle="tooltip" title="Delete"><a href="/user_type/delete_permission/{{$Permissions->id}}" onclick="return confirm(' Are you sure. You want to delete?');"><i class="fa fa-trash-o"></i></a></li>
                  </ul>
              </td>
          </tr>
          @endforeach
          @else
          <tr>
              <td>No Data Found...</td>
          </tr>
          @endif
        </tbody>
      </table>


</div>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <!--<button type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Item</button>-->
                                    <button type="submit" name="action" value="add_permission" class="btn">Add Permission</button>

                                    <button type="submit" name="action" value="update_permission" class="btn">Update Permission</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection


@section('JSScript')
<script>
	function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
	</script>
	<script>
	//Accordian
	$('.accordion').each(function () {
	    var $accordian = $(this);
	    $accordian.find('.accordion-head').on('click', function () {
            $(this).removeClass('open').addClass('closes');
	        $accordian.find('.accordion-body').slideUp();
	        if (!$(this).next().is(':visible')) {
                $(this).removeClass('closes').addClass('open');
	            $(this).next().slideDown();
	        }
	    });
	});

</script>

<script>
    function EditPermission(id){
        var modules = $("#modules").val();
        $.ajax({
            type: "POST",
            url: "/user_type/getpermission/"+id,
            data: {'id': id},
            dataType: "JSON",
            success: function (data) {
                $("#PermissionId").val(data.data.id);

                if(data.data.view_only == 1){
                    $('#ViewOnly').attr('checked', true);
                }

                if(data.data.add_only == 2){
                    $('#AddOnly').attr('checked', true);
                }

                if(data.data.edit_only == 3){
                    $('#EditOnly').attr('checked', true);
                }

                if(data.data.delete_only == 4){
                    $('#DeleteOnly').attr('checked', true);
                }
            }
        });
    }

    function checkmodules(){
        var modules = $("#modules").val();

        if(modules == 5){
            $("#LeadsPermission").show();
            $("#CommonPermission").hide();
            $("#HomeworkPermission").hide();
        }else if(modules == 15){
            $("#LeadsPermission").hide();
            $("#CommonPermission").hide();
            $("#HomeworkPermission").show();
        }else if(modules == 12){
            $("#LeadsPermission").hide();
            $("#CommonPermission").hide();
            $("#HomeworkPermission").hide();
        }else if(modules == 13){
            $("#LeadsPermission").hide();
            $("#CommonPermission").hide();
            $("#HomeworkPermission").hide();
        }else if(modules == 14){
            $("#LeadsPermission").hide();
            $("#CommonPermission").hide();
            $("#HomeworkPermission").hide();
        }

    }
</script>
{{--
<script>
 $(document).ready(function(){
        $('.PermissionClick').click(function(){
            if($(this).prop("checked") == true){
                $(this).attr('checked', true).val(1);
                console.log("Checkbox is checked. 1");
            }
            else if($(this).prop("checked") == false){
                $(this).attr('checked', true).val(0);
                console.log("Checkbox is unchecked. 0");
            }
        });
    });
</script> --}}
@endsection
