@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">User type</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Add User type</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Add User type</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/user_type/store_user_type" method="POST">
                            @csrf
                            <div class="row">

                                <div class="form-group col-12">
                                    <label class="col-form-label">Name</label>
                                    <div>
                                        <input type="text" name="name" class="form-control">
                                    </div>
                                </div>
                                {{-- <div class="form-group col-6">
                                    <label class="col-form-label">User type default role</label>
                                    <div>
                                        <select name="roles">
                                        @foreach($UserTypes as $Users)
                                            <option>{{ $Users->name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="form-group col-6">
                                    <label class="col-form-label">Category</label>
                                <div>

                                    <div style="padding: 0 0 0 20px;">
                                        <div class="accordion">
                                            <div class="accordion-head closes">
                                                &nbsp;<input type="radio" name="user_type" value="1"> Teacher

                                            </div>
                                        </div>

                                        <div class="accordion">
                                            <div class="accordion-head closes">
                                                &nbsp;<input type="radio" name="user_type" value="2"> Students

                                            </div>
                                        </div>

                                    </div>

                                <div class="form-group col-6">
                                    {{-- <label class="col-form-label">Permission</label> --}}
                                    <div>

                                        <!--<input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"> Administrator</label>-->


{{-- <div style="padding: 0 0 0 20px;"> --}}

    {{-- <select name="" id="">
        <option value="1">Courses</option>
        <option value="2">Groups</option>
        <option value="3">Files/Videos</option>
        <option value="4">Online Class Link</option>
        <option value="5">Leads</option>
        <option value="6">Forum</option>
        <option value="7">Payment Tracker</option>
        <option value="8">Testimonials</option>
        <option value="9">Sample Video</option>
        <option value="10">Certification</option>
        <option value="11">Fee Collection</option>
        <option value="12">Teacher Profile</option>
        <option value="13">Student Profile</option>
        <option value="14">Affiliate Profile</option>
        <option value="15">Home Work</option>
    </select>

    <div>
    <input type="checkbox" class="PermissionClick" name="view_permission[]" value="0"> View<br>
            <input type="checkbox" class="PermissionClick" name="add_permission[]" value="0"> Add<br>
        <input type="checkbox" class="PermissionClick" name="edit_permission[]" value="0"> Update<br>
        <input type="checkbox" class="PermissionClick" name="delete_permission[]" value="0"> Delete
    </div> --}}

    {{-- <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="15"> Home Work<div class="arrow down"></div>

        </div>
        <div class="accordion-body" >
            <input type="checkbox" class="PermissionClick" name="view_permission[]" value="0"> View<br>
            <input type="checkbox" class="PermissionClick" name="add_permission[]" value="0"> Add<br>
        <input type="checkbox" class="PermissionClick" name="edit_permission[]" value="0"> View Home Work
        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="2"> Courses<div class="arrow down"></div>

        </div>
        <div class="accordion-body" >
            <input type="checkbox" class="PermissionClick" name="view_permission[]" value="0"> View<br>
            <input type="checkbox" class="PermissionClick" name="add_permission[]" value="0"> Add<br>
        <input type="checkbox" class="PermissionClick" name="edit_permission[]" value="0"> Update<br>
        <input type="checkbox" class="PermissionClick" name="delete_permission[]" value="0"> Delete
        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="3"> Groups<div class="arrow down"></div>

        </div>
        <div class="accordion-body" >
            <input type="checkbox" class="PermissionClick" name="view_permission[]" value="0"> View<br>
            <input type="checkbox" class="PermissionClick" name="add_permission[]" value="0"> Add<br>
        <input type="checkbox" class="PermissionClick" name="edit_permission[]" value="0"> Update<br>
        <input type="checkbox" class="PermissionClick" name="delete_permission[]" value="0"> Delete
        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="4"> Files/Videos<div class="arrow down"></div>

        </div>
        <div class="accordion-body" >
            <input type="checkbox" class="PermissionClick" name="view_permission[]" value="0"> View<br>
            <input type="checkbox" class="PermissionClick" name="add_permission[]" value="0"> Add<br>
        <input type="checkbox" class="PermissionClick" name="edit_permission[]" value="0"> Update<br>
        <input type="checkbox" class="PermissionClick" name="delete_permission[]" value="0"> Delete
        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="5"> Online Class Link <div class="arrow down"></div>

        </div>
        <div class="accordion-body" >
            <input type="checkbox" class="PermissionClick" name="view_permission[]" value="0"> View<br>
            <input type="checkbox" class="PermissionClick" name="add_permission[]" value="0"> Add<br>
        <input type="checkbox" class="PermissionClick" name="edit_permission[]" value="0"> Update<br>
        <input type="checkbox" class="PermissionClick" name="delete_permission[]" value="0"> Delete
        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="6"> Leads<div class="arrow down"></div>

        </div>
        <div class="accordion-body" >
        <input type="checkbox" name="edit_permission[]" value="0"> Classess List
        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="7"> Forum<div class="arrow down"></div>

        </div>
        <div class="accordion-body" >
            <input type="checkbox" class="PermissionClick" name="view_permission[]" value="0"> View<br>
            <input type="checkbox" class="PermissionClick" name="add_permission[]" value="0"> Add<br>
        <input type="checkbox" class="PermissionClick" name="edit_permission[]" value="0"> Update<br>
        <input type="checkbox" class="PermissionClick" name="delete_permission[]" value="0"> Delete
        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="8"> Payment Tracker<div class="arrow down"></div>

        </div>
        <div class="accordion-body" >
            <input type="checkbox" class="PermissionClick" name="view_permission[]" value="0"> View<br>
            <input type="checkbox" class="PermissionClick" name="add_permission[]" value="0"> Add<br>
        <input type="checkbox" class="PermissionClick" name="edit_permission[]" value="0"> Update<br>
        <input type="checkbox" class="PermissionClick" name="delete_permission[]" value="0"> Delete
        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="9"> Testimonials<div class="arrow down"></div>

        </div>
        <div class="accordion-body" >
            <input type="checkbox" class="PermissionClick" name="view_permission[]" value="0"> View<br>
            <input type="checkbox" class="PermissionClick" name="add_permission[]" value="0"> Add<br>
        <input type="checkbox" class="PermissionClick" name="edit_permission[]" value="0"> Update<br>
        <input type="checkbox" class="PermissionClick" name="delete_permission[]" value="0"> Delete
        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="10"> Sample Video<div class="arrow down"></div>

        </div>
        <div class="accordion-body" >
            <input type="checkbox" class="PermissionClick" name="view_permission[]" value="0"> View<br>
            <input type="checkbox" class="PermissionClick" name="add_permission[]" value="0"> Add<br>
        <input type="checkbox" class="PermissionClick" name="edit_permission[]" value="0"> Update<br>
        <input type="checkbox" class="PermissionClick" name="delete_permission[]" value="0"> Delete
        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="14"> Certificates<div class="arrow down"></div>

        </div>
        <div class="accordion-body" >
            <input type="checkbox" class="PermissionClick" name="view_permission[]" value="0"> View<br>
            <input type="checkbox" class="PermissionClick" name="add_permission[]" value="0"> Add<br>
        <input type="checkbox" class="PermissionClick" name="edit_permission[]" value="0"> Update<br>
        <input type="checkbox" class="PermissionClick" name="delete_permission[]" value="0"> Delete
        </div>
    </div>



    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="16"> Fee Collection<div class="arrow down"></div>

        </div>
        <div class="accordion-body" >
            <input type="checkbox" class="PermissionClick" name="view_permission[]" value="0"> View<br>
            <input type="checkbox" class="PermissionClick" name="add_permission[]" value="0"> Add<br>
            <input type="checkbox" class="PermissionClick" name="edit_permission[]" value="0"> Update<br>
            <input type="checkbox" class="PermissionClick" name="delete_permission[]" value="0"> Delete
        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="11"> Teacher Profile

        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="12"> Student Profile

        </div>
    </div>

    <div class="accordion">
        <div class="accordion-head closes">
            &nbsp;<input type="checkbox" name="permission[]" value="13"> Affiliate Profile

        </div>
    </div> --}}


{{-- </div> --}}
                                    </div>
                                </div>

                                <div class="col-12">
                                    <!--<button type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Item</button>-->
                                    <button type="submit" class="btn">Save & Add Permission</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection


@section('JSScript')
<script>
	function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
	</script>
	<script>
	//Accordian
	$('.accordion').each(function () {
	    var $accordian = $(this);
	    $accordian.find('.accordion-head').on('click', function () {
            $(this).removeClass('open').addClass('closes');
	        $accordian.find('.accordion-body').slideUp();
	        if (!$(this).next().is(':visible')) {
                $(this).removeClass('closes').addClass('open');
	            $(this).next().slideDown();
	        }
	    });
	});

</script>
{{--
<script>
 $(document).ready(function(){
        $('.PermissionClick').click(function(){
            if($(this).prop("checked") == true){
                $(this).attr('checked', true).val(1);
                console.log("Checkbox is checked. 1");
            }
            else if($(this).prop("checked") == false){
                $(this).attr('checked', true).val(0);
                console.log("Checkbox is unchecked. 0");
            }
        });
    });
</script> --}}
@endsection
