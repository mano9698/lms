@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">User type</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Edit User type</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Edit User type</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/user_type/update_permissions" method="POST">
                            @csrf
                            <div class="row">

                                <div class="form-group col-12">
                                    <label class="col-form-label">Name</label>
                                    <div>
                                        <input type="hidden" name="id" class="form-control" value="{{ $UserTypes->id }}">

                                        <input type="text" name="name" class="form-control" value="{{ $UserTypes->name }}">
                                    </div>
                                </div>
                                {{-- <div class="form-group col-6">
                                    <label class="col-form-label">User type default role</label>
                                    <div>
                                        <select name="roles">
                                        @foreach($UserTypes as $Users)
                                            <option>{{ $Users->name }}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="form-group col-6">
                                    <label class="col-form-label">Permission</label>
                                    <div>

                                        <!--<input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"> Administrator</label>-->

                                        <div style="padding: 0 0 0 20px;"> <!--<i class="accordion">User</i>
                                            <div class="panel">
                                              <input type="checkbox" /> Edit<br />
                                                <input type="checkbox" /> Update<br />
                                                <input type="checkbox" /> Delete
                                            </div>-->
                                            <?php
$UserPermissions = DB::table('user_permissions')->where('permissions', 2)->first();

                                $UserSubPermissions = DB::table('user_sub_permissions')->where('user_permission_id', $UserPermissions->id)->first();
                            ?>
                                            <div class="accordion">
                                                <div class="accordion-head closes">
                                                    &nbsp;<input type="checkbox" name="permission[]" value="2" @if($UserPermissions->permissions == 2) checked @endif> Courses<div class="arrow down"></div>

                                                </div>
                                                <div class="accordion-body" >
                                                    <input type="checkbox" name="view_permission[]" value="1" @if($UserSubPermissions->view == 1) checked @endif> View<br>
                                                    <input type="checkbox" name="add_permission[]" value="1" @if($UserSubPermissions->add == 1) checked @endif> Add<br>
                                                <input type="checkbox" name="edit_permission[]" value="1" @if($UserSubPermissions->edit == 1) checked @endif> Update<br>
                                                <input type="checkbox" name="delete_permission[]" value="1" @if($UserSubPermissions->delete == 1) checked @endif> Delete
                                                </div>
                                            </div>
<?php
$UserPermissions = DB::table('user_permissions')->where('permissions', 3)->first();

                                $UserSubPermissions = DB::table('user_sub_permissions')->where('user_permission_id', $UserPermissions->id)->first();
                            ?>
                                            <div class="accordion">
                                                <div class="accordion-head closes">
                                                    &nbsp;<input type="checkbox" name="permission[]" value="3" @if($UserPermissions->permissions == 3) checked @endif> Groups<div class="arrow down"></div>

                                                </div>
                                                <div class="accordion-body" >
                                                    <input type="checkbox" name="view_permission[]" value="1" @if($UserSubPermissions->view == 1) checked @endif> View<br>
                                                    <input type="checkbox" name="add_permission[]" value="1" @if($UserSubPermissions->add == 1) checked @endif> Add<br>
                                                <input type="checkbox" name="edit_permission[]" value="1" @if($UserSubPermissions->edit == 1) checked @endif> Update<br>
                                                <input type="checkbox" name="delete_permission[]" value="1" @if($UserSubPermissions->delete == 1) checked @endif> Delete
                                                </div>
                                            </div>
<?php
$UserPermissions = DB::table('user_permissions')->where('permissions', 4)->first();

                                $UserSubPermissions = DB::table('user_sub_permissions')->where('user_permission_id', $UserPermissions->id)->first();
                            ?>
                                            <div class="accordion">
                                                <div class="accordion-head closes">
                                                    &nbsp;<input type="checkbox" name="permission[]" value="4" @if($UserPermissions->permissions == 4) checked @endif> Files/Videos<div class="arrow down"></div>

                                                </div>
                                                <div class="accordion-body" >
                                                    <input type="checkbox" name="view_permission[]" value="1" @if($UserSubPermissions->view == 1) checked @endif> View<br>
                                                    <input type="checkbox" name="add_permission[]" value="1" @if($UserSubPermissions->add == 1) checked @endif> Add<br>
                                                <input type="checkbox" name="edit_permission[]" value="1" @if($UserSubPermissions->edit == 1) checked @endif> Update<br>
                                                <input type="checkbox" name="delete_permission[]" value="1" @if($UserSubPermissions->delete == 1) checked @endif> Delete
                                                </div>
                                            </div>
<?php
$UserPermissions = DB::table('user_permissions')->where('permissions', 5)->first();

                                $UserSubPermissions = DB::table('user_sub_permissions')->where('user_permission_id', $UserPermissions->id)->first();
                            ?>
                                            <div class="accordion">
                                                <div class="accordion-head closes">
                                                    &nbsp;<input type="checkbox" name="permission[]" value="5" @if($UserPermissions->permissions == 5) checked @endif> Conference<div class="arrow down"></div>

                                                </div>
                                                <div class="accordion-body" >
                                                    <input type="checkbox" name="view_permission[]" value="1" @if($UserSubPermissions->view == 1) checked @endif> View<br>
                                                    <input type="checkbox" name="add_permission[]" value="1" @if($UserSubPermissions->add == 1) checked @endif> Add<br>
                                                <input type="checkbox" name="edit_permission[]" value="1" @if($UserSubPermissions->edit == 1) checked @endif> Update<br>
                                                <input type="checkbox" name="delete_permission[]" value="1" @if($UserSubPermissions->delete == 1) checked @endif> Delete
                                                </div>
                                            </div>
<?php
$UserPermissions = DB::table('user_permissions')->where('permissions', 6)->first();


                                $UserSubPermissions = DB::table('user_sub_permissions')->where('user_permission_id', $UserPermissions->id)->first();

                            ?>
                                            <div class="accordion">
                                                <div class="accordion-head closes">
                                                    &nbsp;<input type="checkbox" name="permission[]" value="6" @if($UserPermissions->permissions == 6) checked @endif> Leads<div class="arrow down"></div>

                                                </div>
                                                <div class="accordion-body" >
                                                    <input type="checkbox" name="view_permission[]" value="1" @if($UserSubPermissions->view == 1) checked @endif> View<br>
                                                    <input type="checkbox" name="add_permission[]" value="1" @if($UserSubPermissions->add == 1) checked @endif> Add<br>
                                                <input type="checkbox" name="edit_permission[]" value="1" @if($UserSubPermissions->edit == 1) checked @endif> Update<br>
                                                <input type="checkbox" name="delete_permission[]" value="1" @if($UserSubPermissions->delete == 1) checked @endif> Delete
                                                </div>
                                            </div>
<?php
$UserPermissions = DB::table('user_permissions')->where('permissions', 7)->first();

                                $UserSubPermissions = DB::table('user_sub_permissions')->where('user_permission_id', $UserPermissions->id)->first();
                            ?>
                                            <div class="accordion">
                                                <div class="accordion-head closes">
                                                    &nbsp;<input type="checkbox" name="permission[]" value="7" @if($UserPermissions->permissions == 7) checked @else @endif> Forum<div class="arrow down"></div>

                                                </div>
                                                <div class="accordion-body" >
                                                    <input type="checkbox" name="view_permission[]" value="1" @if($UserSubPermissions->view == 1) checked @else @endif> View<br>
                                                    <input type="checkbox" name="add_permission[]" value="1" @if($UserSubPermissions->add == 1) checked @else @endif> Add<br>
                                                <input type="checkbox" name="edit_permission[]" value="1" @if($UserSubPermissions->edit == 1) checked @else @endif> Update<br>
                                                <input type="checkbox" name="delete_permission[]" value="1" @if($UserSubPermissions->delete == 1) checked @else @endif> Delete
                                                </div>
                                            </div>
<?php
$UserPermissions = DB::table('user_permissions')->where('permissions', 8)->first();

                                $UserSubPermissions = DB::table('user_sub_permissions')->where('user_permission_id', $UserPermissions->id)->first();
                            ?>
                                            <div class="accordion">
                                                <div class="accordion-head closes">
                                                    &nbsp;<input type="checkbox" name="permission[]" value="8" @if($UserPermissions->permissions == 8) checked @endif> Payment Tracker<div class="arrow down"></div>

                                                </div>
                                                <div class="accordion-body" >
                                                    <input type="checkbox" name="view_permission[]" value="1" @if($UserSubPermissions->view == 1) checked @endif> View<br>
                                                    <input type="checkbox" name="add_permission[]" value="1" @if($UserSubPermissions->add == 1) checked @endif> Add<br>
                                                <input type="checkbox" name="edit_permission[]" value="1" @if($UserSubPermissions->edit == 1) checked @endif> Update<br>
                                                <input type="checkbox" name="delete_permission[]" value="1" @if($UserSubPermissions->delete == 1) checked @endif> Delete
                                                </div>
                                            </div>
<?php
$UserPermissions = DB::table('user_permissions')->where('permissions', 9)->first();

                                $UserSubPermissions = DB::table('user_sub_permissions')->where('user_permission_id', $UserPermissions->id)->first();
                            ?>
                                            <div class="accordion">
                                                <div class="accordion-head closes">
                                                    &nbsp;<input type="checkbox" name="permission[]" value="9" @if($UserPermissions->permissions == 9) checked @endif> Testimonials<div class="arrow down"></div>

                                                </div>
                                                <div class="accordion-body" >
                                                    <input type="checkbox" name="view_permission[]" value="1" @if($UserSubPermissions->view == 1) checked @endif> View<br>
                                                    <input type="checkbox" name="add_permission[]" value="1" @if($UserSubPermissions->add == 1) checked @endif> Add<br>
                                                <input type="checkbox" name="edit_permission[]" value="1" @if($UserSubPermissions->edit == 1) checked @endif> Update<br>
                                                <input type="checkbox" name="delete_permission[]" value="1" @if($UserSubPermissions->delete == 1) checked @endif> Delete
                                                </div>
                                            </div>
<?php
$UserPermissions = DB::table('user_permissions')->where('permissions', 10)->first();

                                $UserSubPermissions = DB::table('user_sub_permissions')->where('user_permission_id', $UserPermissions->id)->first();
                            ?>
                                            <div class="accordion">
                                                <div class="accordion-head closes">
                                                    &nbsp;<input type="checkbox" name="permission[]" value="10" @if($UserPermissions->permissions == 10) checked @endif> Sample Video<div class="arrow down"></div>

                                                </div>
                                                <div class="accordion-body" >
                                                    <input type="checkbox" name="view_permission[]" value="1" @if($UserSubPermissions->view == 1) checked @endif> View<br>
                                                    <input type="checkbox" name="add_permission[]" value="1" @if($UserSubPermissions->add == 1) checked @endif> Add<br>
                                                <input type="checkbox" name="edit_permission[]" value="1" @if($UserSubPermissions->edit == 1) checked @endif> Update<br>
                                                <input type="checkbox" name="delete_permission[]" value="1" @if($UserSubPermissions->delete == 1) checked @endif> Delete
                                                </div>
                                            </div>

                                            {{-- <input type="checkbox"> Courses<br>
                                            <input type="checkbox"> Groups<br>
                                            <input type="checkbox"> Categories<br>
                                            <input type="checkbox"> Branches<br>
                                            <input type="checkbox"> Events engine<br>
                                            <input type="checkbox"> Import - Export<br>
                                            <input type="checkbox"> User types<br>
                                            <input type="checkbox"> Account &amp; Settings<br>
                                            <input type="checkbox"> Reports<br>
                                            <input type="checkbox"> Files<br></div>
<div style="padding: 0 0 0 20px;"><input type="checkbox" name="permission[]" value="1"/> Users<br />
<input type="checkbox" name="permission[]" value="2"/> Courses<br />
<input type="checkbox" name="permission[]" value="3"/> Groups<br />
<input type="checkbox" name="permission[]" value="4"/> Categories<br />
<input type="checkbox" name="permission[]" value="5"/> Branches<br />
<input type="checkbox" name="permission[]" value="6"/> Events engine<br />
<input type="checkbox" name="permission[]" name="permission[]" value="7"/> Import - Export<br />
<input type="checkbox" name="permission[]" value="8"/> User types<br />
<input type="checkbox" name="permission[]" value="9"/> Account &amp; Settings<br />
<input type="checkbox" name="permission[]" value="10"/> Reports<br />
<input type="checkbox" name="permission[]" value="11"/> Files<br /></div> --}}
                                    </div>
                                </div>

                                <div class="col-12">
                                    <!--<button type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Item</button>-->
                                    <button type="submit" class="btn">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection

@section('JSScript')
<script>
	function toggle(source) {
    var checkboxes = document.querySelectorAll('input[type="checkbox"]');
    for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i] != source)
            checkboxes[i].checked = source.checked;
    }
}
	</script>
	<script>
	//Accordian
	$('.accordion').each(function () {
	    var $accordian = $(this);
	    $accordian.find('.accordion-head').on('click', function () {
            $(this).removeClass('open').addClass('closes');
	        $accordian.find('.accordion-body').slideUp();
	        if (!$(this).next().is(':visible')) {
                $(this).removeClass('closes').addClass('open');
	            $(this).next().slideDown();
	        }
	    });
	});
</script>
@endsection
