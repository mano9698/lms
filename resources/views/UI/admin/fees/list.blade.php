@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Fee Collection</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Fee Collection</li>
            </ul>
        </div>
        <!-- Card -->

        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Fee Collection</h4>
                    </div>
                    <div class="email-wrapper">

                        <div class="mail-list-container">
                            <div class="mail-toolbar">
                                <!--<div class="check-all">
                                    <div class="custom-control custom-checkbox checkbox-st1">
                                        <input type="checkbox" class="custom-control-input" id="check1">
                                        <label class="custom-control-label" for="check1"></label>
                                    </div>
                                </div>-->
                                <div class="mail-search-bar">
                                    <input type="text" class="form-control" placeholder="Search By Id">
                                </div>
<div class="dropdown all-msg-toolbar">
<a href="/fee_collection/add_fees" type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Fees</a>
                                </div>
                            </div>
                            @if(session('message'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! session('message') !!}</li>
                </ul>
            </div>
        @endif
                            <table class="table public-user-block block">
                  <thead>
                    <tr>
                        <th>Id</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Amount received</th>
                      <th>Date of Receipt</th>
                      <th>Mode of receipt</th>
                      <th>Received Towards</th>
                      <th>Action</th>

                    </tr>
                  </thead>
                  <tbody>
                      @foreach($FeeCollection as $Fees)
                    <tr>
                        <td><strong>{{ $Fees->id }}</strong></td>
                      <td>{{ $Fees->first_name }}</td>
                      <td>{{ $Fees->last_name }}</td>
                      <td>{{ $Fees->amount_received }}</td>
                        <td>{{ $Fees->date_of_receipt }}</td>
                        <td>{{ $Fees->mode_of_receipt }}</td>
                        <td>{{ $Fees->received_towards }}</td>
                      <td><ul class="mailbox-toolbar" style="position: inherit;">
                        <a href="/fee_collection/edit_fees/{{$Fees->id}}">
                            <li data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></li>
                        </a>

                        <a href="/fee_collection/delete_fees/{{$Fees->id}}" onclick="return confirm(' Are you sure. You want to delete?');">
                            <li data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></li>
                        </a>

                                    </ul></td>

                    </tr>
                    @endforeach
                  </tbody>
                </table>

                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
