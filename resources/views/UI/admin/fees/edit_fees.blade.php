@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Fee Collection</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Edit Fee Collection</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Edit Fee Collection</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif

                        <form class="edit-profile m-b30" action="/fee_collection/update_fees" method="POST">
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Select Student</label>
                                    <input type="hidden" name="id" id="" value="{{ $FeeCollection->id }}">
                                    <div>
                                        <select class="form-control" type="text" value="" name="student_id">
                                            @foreach($Users as $User)
                                                <option value="{{ $User->id }}" @if($User->id == $FeeCollection->student_id) selected @endif>{{ $User->first_name }} {{ $User->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Amount Received</label>
                                    <div>
                                        <input class="form-control" type="text" value="{{ $FeeCollection->amount_received }}" name="amount_received">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Date of receipt</label>
                                    <div>
                                        <input class="form-control" type="date" value="{{ $FeeCollection->date_of_receipt }}" name="date_of_receipt">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Mode of receipt</label>
                                    <div>
                                        <select class="form-control" type="text" value="" name="mode_of_receipt">
                                            <option @if($FeeCollection->mode_of_receipt == "Bank Transfer") selected @endif>Bank Transfer</option>
                                            <option @if($FeeCollection->mode_of_receipt == "Google Pay") selected @endif>Google Pay</option>
                                            <option @if($FeeCollection->mode_of_receipt == "Paytm") selected @endif>Paytm</option>
                                            <option @if($FeeCollection->mode_of_receipt == "Cash") selected @endif>Cash</option>
                                            <option @if($FeeCollection->mode_of_receipt == "Skillsgroom") selected @endif>Skillsgroom</option>
                                            <option @if($FeeCollection->mode_of_receipt == "Payment Gateway") selected @endif>Payment Gateway</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Received towards</label>
                                    <div>
                                        <textarea name="received_towards" class="form-control"> {{ $FeeCollection->received_towards }}</textarea>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <!--<button type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Item</button>-->
                                    <button type="submit" class="btn">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
