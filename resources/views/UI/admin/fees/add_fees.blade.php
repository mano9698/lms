@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Fee Collection</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Add Fee Collection</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Add Fee Collection</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif

                        <form class="edit-profile m-b30" action="/fee_collection/store_fees" method="POST">
                            @csrf
                            <div class="row">
                                <div class="form-group col-6">
                                    <label class="col-form-label">Select Student</label>
                                    <div>
                                        <select class="form-control" type="text" value="" name="student_id">
                                            @foreach($Users as $User)
                                                <option value="{{ $User->id }}">{{ $User->first_name }} {{ $User->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Amount Received</label>
                                    <div>
                                        <input class="form-control" type="text" value="" name="amount_received">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Date of receipt</label>
                                    <div>
                                        <input class="form-control" type="date" value="" name="date_of_receipt">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Mode of receipt</label>
                                    <div>
                                        <select class="form-control" type="text" value="" name="mode_of_receipt">
                                            <option>Bank Transfer</option>
                                            <option>Google Pay</option>
                                            <option>Paytm</option>
                                            <option>Cash</option>
                                            <option>Skillsgroom</option>
                                            <option>Payment Gateway</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Received towards</label>
                                    <div>
                                        <textarea name="received_towards" class="form-control"> </textarea>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <!--<button type="button" class="btn-secondry add-item m-r5"><i class="fa fa-fw fa-plus-circle"></i>Add Item</button>-->
                                    <button type="submit" class="btn">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
