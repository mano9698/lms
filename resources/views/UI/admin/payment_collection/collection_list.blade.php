@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Dashboard</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Affiliate Partner Payment Collection</li>
            </ul>
        </div>
        <!-- Card -->
        <div class="row">
            @if(session('message'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! session('message') !!}</li>
                </ul>
            </div>
        @endif
            <table class="table public-user-block block">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Email ID</th>
                      <th>Contact Number</th>
                      <th>Course Name</th>
                      <th>Amount collected</th>
                      <th>Collection Mode</th>
                      <th>Collection Ref Number</th>
                      <th>Collection Date</th>
                        <th>Affiliate </th>
                        <th>Affiliate Id</th>
                        <th>Action </th>
                    </tr>
                  </thead>
                  <tbody>
                    @if($PaymentCollection)
                    @foreach($PaymentCollection as $Payment)
                    <tr>
                      <th>{{$Payment->id}}</th>
                      <td><strong>{{ $Payment->first_name }}</strong></td>
                      <td>{{ $Payment->last_name }}</td>
                      <td>{{ $Payment->email }}</td>
                      <td>{{ $Payment->contact }}</td>
<td>{{ $Payment->course_name }}</td>
                        <td>{{ $Payment->amount_collected }}</td>
                      <td>{{ $Payment->collection_mode }}</td>
                      <td>{{ $Payment->collection_ref_no }}</td>
                      <td>{{ $Payment->collection_date }}</td>
                        <td>{{ $Payment->AFirstName }} {{ $Payment->ALastName }}</td>
                        <td>{{ $Payment->UserId }}</td>
                        <td>
                            <ul class="mailbox-toolbar">
                                <a href="/payments/collection/edit_payment_collection/{{$Payment->id}}">
                                    <li data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></li>
                                </a>

                                <a href="/payments/collection/delete_payment_collection/{{$Payment->id}}" onclick="return confirm(' Are you sure. You want to delete?');">
                                    <li data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></li>
                                </a>
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                    @else

                    @endif
                  </tbody>
                </table>

        </div>
        <!-- Card END -->

    </div>
</main>
@endsection
