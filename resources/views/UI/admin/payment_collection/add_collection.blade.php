@extends('UI.base')
@section('Content')
<main class="ttr-wrapper">
    <div class="container-fluid">
        <div class="db-breadcrumb">
            <h4 class="breadcrumb-title">Payment Tracker</h4>
            <ul class="db-breadcrumb-list">
                <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                <li>Add Payment Collection</li>
            </ul>
        </div>
        <div class="row">
            <!-- Your Profile Views Chart -->
            <div class="col-lg-12 m-b30">
                <div class="widget-box">
                    <div class="wc-title">
                        <h4>Add Payment Collection</h4>
                    </div>
                    <div class="widget-inner">
                        @if(session('message'))
                            <div class="alert alert-success">
                                <ul>
                                    <li>{!! session('message') !!}</li>
                                </ul>
                            </div>
                        @endif
                        <form class="edit-profile m-b30" action="/payments/collection/store_payment_collection" method="POST">
                            @csrf
                            <div class="row">

                                <div class="form-group col-6">
                                    <label class="col-form-label">First Name</label>
                                    <div>
                                        <input class="form-control" name="first_name" type="text" value="">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Last Name</label>
                                    <div>
                                        <input class="form-control" name="last_name" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Email ID</label>
                                    <div>
                                        <input class="form-control" name="email" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Contact Number</label>
                                    <div>
                                        <input class="form-control" name="contact" type="text" value="">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Course Name</label>
                                    <div>
                                        <input class="form-control" name="course_name" type="text" value="">
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label class="col-form-label">Amount collected</label>
                                    <div>
                                        <input class="form-control" name="amount_collected" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Collection Mode</label>
                                    <div>
                                        <input class="form-control" name="collection_mode" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Collection Ref Number</label>
                                    <div>
                                        <input class="form-control" name="collection_ref_no" type="text" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Collection Date</label>
                                    <div>
                                        <input class="form-control" name="collection_date" type="date" value="">
                                    </div>
                                </div>

                                <div class="form-group col-6">
                                    <label class="col-form-label">Select Affiliate</label>
                                    <div>
                                        <select class="form-control" name="affiliate" type="text" value="">
                                            <option selected disabled>Select Affiliate</option>
                                            @foreach($Affiliates as $Affiliatess)
                                                <option value="{{ $Affiliatess->id }}">{{ $Affiliatess->first_name }} {{ $Affiliatess->last_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>



                                <div class="col-12">
                                    <button type="submit" class="btn">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>
@endsection
