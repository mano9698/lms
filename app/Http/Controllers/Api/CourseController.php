<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Courses;

use App\Models\UI\Partners;

use App\Models\UI\Inquiry;
use App\Models\UI\Classes;
use App\Models\UI\RequestCallBack;

class CourseController extends Controller
{
    public function course_list($id)
    {
        $Courses = Courses::where('category', $id)->get();

        return response()->json(array(
            "error"=>False,
            "data"=> $Courses
        ));
    }

    public function course_details($id)
    {
        $Courses = Courses::where('courses.id', $id)->select('courses.*', 'users.id as UserId', 'users.first_name','users.last_name', 'teacher_personal_details.instagram_link', 'teacher_personal_details.facebook_link', 'teacher_personal_details.youtube_channel', 'teacher_personal_details.profile_pic', 'teacher_personal_details.about_me')->join('users', 'users.id', '=', 'courses.teacher_id')->join('teacher_personal_details', 'users.id', '=', 'teacher_personal_details.teacher_id')->first();

        return response()->json(array(
            "error"=>False,
            "data"=> $Courses
        ));
    }

    public function store_courses(Request $request)
    {
        $Courses = new Courses();

        $Courses->title = $request->title;
        $Courses->course_image = $request->course_image;
        // $Classes->class_name = $request->classes;
        $Courses->course_description = $request->course_description;
        $Courses->course_date = $request->course_date;
        $Courses->price = $request->price;

        $SaveCourses = $Courses->save();

        // if($SaveCourses){
        //     return response()->json(array(
        //         "error"=>False,
        //         "message"=>"Thank you for submitting your details. Our team will connect you in next 48 hours."
        //     ));
        // }else{
        //     return response()->json(array(
        //         "error"=>TRUE,
        //         "message"=>"Failed"
        //     ));
        // }
    }

    public function update_courses(Request $request, $id)
    {
        $Courses = Courses::Where('id', $id)->first();

        $Courses->title = $request->title;
        $Courses->course_image = $request->course_image;
        // $Classes->class_name = $request->classes;
        $Courses->course_description = $request->course_description;
        $Courses->course_date = $request->course_date;
        $Courses->price = $request->price;

        $SaveCourses = $Courses->save();

        // if($SaveCourses){
        //     return response()->json(array(
        //         "error"=>False,
        //         "message"=>"Thank you for submitting your details. Our team will connect you in next 48 hours."
        //     ));
        // }else{
        //     return response()->json(array(
        //         "error"=>TRUE,
        //         "message"=>"Failed"
        //     ));
        // }
    }

    public function store_classes(Request $request)
    {
        $Classes = new Classes();

        $Classes->teacher_id = $request->teacher_id;
        $Classes->course_id = $request->course_id;
        // $Classes->class_name = $request->classes;
        $Classes->first_name = $request->first_name;
        $Classes->last_name = $request->last_name;
        $Classes->email = $request->email;
        $Classes->contact = $request->contact;
        $Classes->country = $request->country;
        $Classes->state = $request->state;
        $Classes->city = $request->city;

        $SaveClasses = $Classes->save();

        if($SaveClasses){
            return response()->json(array(
                "error"=>False,
                "message"=>"Thank you for submitting your details. Our team will connect you in next 48 hours."
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }


    public function store_request_call_back(Request $request)
    {
        $RequestCallBack = new RequestCallBack();

        $RequestCallBack->course_name = $request->course_name;
        // $Classes->class_name = $request->classes;
        $RequestCallBack->requirement = $request->requirement;
        $RequestCallBack->first_name = $request->first_name;
        $RequestCallBack->last_name = $request->last_name;
        $RequestCallBack->email = $request->email;
        $RequestCallBack->contact = $request->contact;
        $RequestCallBack->country = $request->country;
        $RequestCallBack->state = $request->state;
        $RequestCallBack->city = $request->city;

        $SaveRequestCallBack = $RequestCallBack->save();

        if($SaveRequestCallBack){
            return response()->json(array(
                "error"=>False,
                "message"=>"Thank you for submitting your details. Our team will connect you in next 48 hours."
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }


    public function store_partners(Request $request)
    {
        $Partners = new Partners();

        $Partners->name = $request->name;
        $Partners->email = $request->email;
        $Partners->mobile = $request->mobile;
        $Partners->state = $request->state;
        $Partners->city = $request->city;
        $Partners->tution = $request->tution;
        $Partners->subjects = $request->subjects;
        $Partners->standard = $request->standard;
        $Partners->education = $request->education;
        $Partners->no_of_students = $request->no_of_students;
        $Partners->website = $request->website;

        $SavePartners = $Partners->save();

        if($SavePartners){
            return response()->json(array(
                "error"=>False,
                "message"=>"Thank you for submitting your details. Our team will connect you in next 48 hours."
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    public function store_inquiry(Request $request)
    {
        $Inquiry = new Inquiry();

        $Inquiry->name = $request->name;
        $Inquiry->email = $request->email;
        $Inquiry->mobile = $request->mobile;
        $Inquiry->location = $request->location;
        $Inquiry->category = $request->category;
        $Inquiry->inquiry = $request->inquiry;

        $SaveInquiry = $Inquiry->save();

        if($SaveInquiry){
            return response()->json(array(
                "error"=>False,
                "message"=>"Thank you for submitting your details. Our team will connect you in next 48 hours."
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }


    public function search_course($Name)
    {

        $Courses = Courses::where('title', 'LIKE', "%{$Name}%")
        ->orWhere('course_description', 'LIKE', "%{$Name}%")
        ->get();

        return response()->json(array(
            "error"=>False,
            "data"=> $Courses
        ));
    }
}
