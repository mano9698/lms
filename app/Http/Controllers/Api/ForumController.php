<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Questions;
use App\Models\UI\Comments;
use App\Models\UI\ForumLikes;

class ForumController extends Controller
{
    public function forum_list($id)
    {
        $Questions = Questions::select('questions.*', 'users.first_name', 'users.last_name')
                        ->join('users', 'users.id', '=', 'questions.teacher_id')
                        ->where('questions.subject', $id)
                        ->where('questions.status', 1)
                        ->get();

        return response()->json(array(
            "error"=>False,
            "data"=> $Questions
        ));
    }


    public function forum_details($id)
    {
        $Questions = Questions::select('questions.*', 'users.first_name', 'users.last_name', 'teacher_personal_details.profile_pic', 'teacher_personal_details.about_me')
                        ->join('users', 'users.id', '=', 'questions.teacher_id')
                        ->join('teacher_personal_details', 'users.id', '=', 'teacher_personal_details.teacher_id')
                        ->where('questions.id', $id)
                        ->first();

        return response()->json(array(
            "error"=>False,
            "data"=> $Questions
        ));
    }

    public function comments_list($id)
    {
        $Questions = Comments::where('question_id', $id)
                        ->get();

        return response()->json(array(
            "error"=>False,
            "data"=> $Questions
        ));
    }


    public function likes_list($id)
    {
        $ForumLikes = ForumLikes::where('question_id', $id)
                        ->get();

        return response()->json(array(
            "error"=>False,
            "data"=> $ForumLikes
        ));
    }


    public function add_comments(Request $request)
    {
        $Comments = new Comments();

        $Comments->question_id = $request->question_id;
        $Comments->name = $request->name;
        // $Classes->class_name = $request->classes;
        $Comments->email = $request->email;
        $Comments->location = $request->location;
        $Comments->comment = $request->comment;
        $Comments->status = 0;

        $SaveComments = $Comments->save();

        if($SaveComments){
            return response()->json(array(
                "error"=>False,
                "message"=>"Comments Updated Successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }

    public function add_likes(Request $request)
    {
        $ForumLikes = new ForumLikes();

        $ForumLikes->question_id = $request->question_id;
        $SaveForumLikes= $ForumLikes->save();

        if($SaveForumLikes){
            return response()->json(array(
                "error"=>False,
                "message"=>"Updated Successfully"
            ));
        }else{
            return response()->json(array(
                "error"=>TRUE,
                "message"=>"Failed"
            ));
        }
    }
}
