<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Users;
use App\Models\UI\Testimonials;
use App\Models\UI\Courses;
use App\Models\UI\Videos;

class TeachersController extends Controller
{
    public function teachers_list(Request $request)
    {
        $Users = Users::select('users.*', 'teacher_personal_details.teaching_subjects','teacher_personal_details.profile_pic')
                    ->join('teacher_personal_details', 'teacher_personal_details.teacher_id', '=', 'users.id')
                    ->where('user_main_type', 1)->orderBy('users.created_at', 'DESC')->get();

        return response()->json(array(
            "error"=>False,
            "data"=> $Users
        ));
    }

    public function teachers_details($id)
    {
        $Users = Users::join('teacher_personal_details', 'teacher_personal_details.teacher_id', '=', 'users.id')
                    ->where('users.id', $id)
                    ->first();

        return response()->json(array(
            "error"=>False,
            "data"=> $Users
        ));
    }

    public function testimonials_details($id)
    {
        $Testimonials = Testimonials::join('students_profile', 'students_profile.student_id', '=', 'testimonials.student_id')
                    ->join('users', 'testimonials.student_id', '=', 'users.id')
                    ->where('testimonials.teacher_id', $id)
                    ->get();

        return response()->json(array(
            "error"=>False,
            "data"=> $Testimonials
        ));
    }

    public function course_list_basedon_teachers($id)
    {
        $Courses = Courses::where('teacher_id', $id)->get();

        return response()->json(array(
            "error"=>False,
            "data"=> $Courses
        ));
    }

    public function videos_list($id)
    {
        $Videos = Videos::where('teacher_id', $id)->get();

        return response()->json(array(
            "error"=>False,
            "data"=> $Videos
        ));
    }
}
