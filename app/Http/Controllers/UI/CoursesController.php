<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Courses;

use App\Models\UI\Users;
use App\Models\UI\StudentsList;

use Illuminate\Support\Facades\Auth;
use Session;

class CoursesController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('guest:super_admin');
        // $this->middleware('guest:teacher')->except('courses_list');
        if(Auth::guard('super_admin')->check()){
            $this->middleware('guest:super_admin');
        }elseif(Auth::guard('teacher')->check()){
            $this->middleware('guest:teacher');
        }
    }

    public function courses_list(){

        $title = "Courses List";
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
            $Courses = Courses::orderBy('created_at', 'DESC')->get();
        }else{
            $UserId = Session::get('TeacherId');

            $Courses = Courses::where('teacher_id', $UserId)->orderBy('created_at', 'DESC')->get();
        }

        return view('UI.teachers.courses.courses_list', compact('title', 'Courses'));
    }


    public function add_courses(){
        $title = "Add Courses";
        return view('UI.teachers.courses.create_course', compact('title'));
    }

    public function edit_courses($id){
        $title = "Edit Courses";
        $Courses = Courses::where('id', $id)->first();
        return view('UI.teachers.courses.edit_course', compact('title', 'Courses'));
    }

    public function store_course(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }
        $Courses = new Courses();

        $Courses->teacher_id = $UserId;
        $Courses->title = $request->title;
        $Courses->category = $request->category;

        if($request->hasfile('course_image')){
            $extension = $request->file('course_image')->getClientOriginalExtension();
            $dir = 'UI/courses/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('course_image')->move($dir, $filename);


            // $extension1 = $request->file('course_image')->getClientOriginalExtension();
            // $dir1 = 'https://staging.skillsgroom.com/UI/courses/';
            // $filename1 = uniqid() . '_' . time() . '.' . $extension1;
            // $request->file('course_image')->move($dir1, $filename1);



            $Courses->course_image = $filename;
        }

        $Courses->course_description = $request->course_description;
        $Courses->course_date = $request->course_date;
        $Courses->duration = $request->duration;
        $Courses->course_time = $request->course_time;
        $Courses->location = $request->location;
        $Courses->price = $request->price;
        $Courses->who_should_join = $request->who_should_join;
        $Courses->status = 1;

        $AddCourses = $Courses->save();

        $CourseId = $request->session()->put('CourseId', $Courses->id);

        // return redirect()->back()->with('message','Courses Added Successfully');
        return redirect('courses/add_students/'.$Courses->id);
    }



    public function update_course(Request $request){
        // if(Auth::guard('super_admin')->check()){
        //     $UserId = Session::get('AdminId');
        // }elseif(Auth::guard('teacher')->check()){
        //     $UserId = Session::get('TeacherId');
        // }elseif(Auth::guard('student')->check()){
        //     $UserId = Session::get('StudentId');
        // }

        $Id = $request->id;

        $Courses = Courses::where('id', $Id)->first();

        // $Courses->teacher_id = $UserId;
        $Courses->title = $request->title;
        $Courses->category = $request->category;

        if($request->hasfile('course_image')){
            $extension = $request->file('course_image')->getClientOriginalExtension();
            $dir = 'UI/courses/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('course_image')->move($dir, $filename);

            // $extension1 = $request->file('course_image')->getClientOriginalExtension();
            // $dir1 = 'https://staging.skillsgroom.com/UI/courses/';
            // $filename1 = uniqid() . '_' . time() . '.' . $extension1;
            // $request->file('course_image')->move($dir1, $filename1);


            $Courses->course_image = $filename;
        }else{
            $Courses->course_image = $Courses->course_image;
        }

        $Courses->course_description = $request->course_description;
        $Courses->course_date = $request->course_date;
        $Courses->duration = $request->duration;
        $Courses->course_time = $request->course_time;
        $Courses->location = $request->location;
        $Courses->price = $request->price;
        $Courses->who_should_join = $request->who_should_join;
        // $Courses->status = 1;

        $AddCourses = $Courses->save();

        return redirect()->back()->with('message','Courses Updated Successfully');
    }

    public function delete_course(Request $request)
    {
        $CourseId = $request->id;

        // echo $request->student_id;
        // exit;
        $Courses = Courses::where('id', $CourseId)->delete();

        $StudentsList = StudentsList::where('course_id', $CourseId)->delete();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> " Course Removed Successfully"
                        )
        );
    }


    public function add_students($id){
        $title = "Add Students";
        $Courses = Courses::where('id', $id)->first();

        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');

            $StudentsList = Users::where('user_type', 3)
            ->select('users.*')
            ->join('add_teachers_to_students', 'add_teachers_to_students.student_id', '=', 'users.id')
            // ->where('add_teachers_to_students.teacher_id', $UserId)
            ->orderBy('users.created_at', 'DESC')->get();
        }else{
            $UserId = Session::get('TeacherId');

            $StudentsList = Users::where('user_type', 3)
            ->select('users.*')
            ->join('add_teachers_to_students', 'add_teachers_to_students.student_id', '=', 'users.id')
            ->where('add_teachers_to_students.teacher_id', $UserId)
            ->orderBy('users.created_at', 'DESC')->get();
        }

        // echo json_encode($StudentsList);
        // exit;

        return view('UI.teachers.courses.add_students', compact('title', 'StudentsList', 'Courses'));
    }


    public function store_students_list(Request $request){

        $CourseId = $request->course_id;

        $StudentsList = new StudentsList();

        $StudentsList->course_id = $CourseId;
        $StudentsList->student_id = $request->student_id;

        $StudentsList->save();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> " Student Added Successfully"
                        )
        );
    }

    public function delete_students_list(Request $request)
    {
    	// \Log::info($request->all());

        $CourseId = $request->course_id;

        // echo $request->student_id;
        // exit;

        $StudentsList = StudentsList::where('course_id', $CourseId)
                        ->where('student_id', $request->student_id)->delete();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> " Student Removed Successfully"
                        )
        );
    }


    public function students_list($id){

        $title = "Students List";
        // $Courses = Courses::where('id', $id)->first();
        $StudentsList = StudentsList::join('users', 'users.id', 'students_list.student_id')
                                    ->join('students_profile', 'users.id', 'students_profile.student_id')
                                    ->select('users.*', 'students_profile.gender')
                                    ->where('students_list.course_id', $id)
                                    ->get();


        return view('UI.teachers.courses.students_list', compact('title', 'StudentsList'));
    }

}
