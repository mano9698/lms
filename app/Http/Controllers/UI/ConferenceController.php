<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Conference;
use App\Models\UI\Users;

use App\Models\UI\ConferenceStudentsList;
use App\Models\UI\ConferenceGroup;
use App\Models\UI\Groups;

use Illuminate\Support\Facades\Auth;
use Session;

class ConferenceController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('guest:super_admin')->except('dashboard');
        if(Auth::guard('super_admin')->check()){
            $this->middleware('guest:super_admin');
        }elseif(Auth::guard('teacher')->check()){
            $this->middleware('guest:teacher');
        }
    }

    public function conference_list(){
        $title = "Online Class Link";
        $Conference = Conference::orderBy('created_at', 'DESC')->get();
        return view('UI.teachers.conference.conference_list', compact('title', 'Conference'));
    }


    public function add_conference(){
        $title = "Add Class Link";
        return view('UI.teachers.conference.add_conference', compact('title'));
    }

    public function edit_conference($id){
        $title = "Edit Class Link";
        $Conference = Conference::where('id', $id)->first();
        return view('UI.teachers.conference.edit_conference', compact('title', 'Conference'));
    }


    public function store_conference(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }
        $Conference = new Conference();

        $Conference->teacher_id = $UserId;
        $Conference->name = $request->name;
        $Conference->date = $request->date;

        $Conference->duration = $request->duration;
        $Conference->start_time = $request->start_time;
        $Conference->description = $request->description;
        $Conference->meeting_link = $request->meeting_link;
        $Conference->status = 1;

        $AddConference = $Conference->save();


        // return redirect()->back()->with('message','Courses Added Successfully');
        return redirect('teachers/conference/add_students/'.$Conference->id);
    }

    public function update_conference(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $id = $request->id;

        $Conference = Conference::where('id', $id)->first();

        $Conference->teacher_id = $UserId;
        $Conference->name = $request->name;
        $Conference->date = $request->date;

        $Conference->duration = $request->duration;
        $Conference->start_time = $request->start_time;
        $Conference->description = $request->description;
        $Conference->meeting_link = $request->meeting_link;
        $Conference->status = 1;

        $AddConference = $Conference->save();


        return redirect()->back()->with('message','Conference Updated Successfully');
        // return redirect('teachers/conference/add_students/'.$Conference->id);
    }

    public function delete_conference(Request $request)
    {
        $conference_id = $request->id;

        // echo $request->student_id;
        // exit;
        $Conference = Conference::where('id', $conference_id)->delete();

        $ConferenceStudentsList = ConferenceStudentsList::where('conference_id', $conference_id)->delete();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> "Conference Removed Successfully"
                        )
        );
    }


    public function add_students($id){

        $title = "Add Students";
        $Conference = Conference::where('id', $id)->first();
        $StudentsList = Users::where('user_type', 3)->orderBy('created_at', 'DESC')->get();

        return view('UI.teachers.conference.add_students', compact('title', 'StudentsList', 'Conference'));
    }


    public function store_students_list(Request $request){

        $ConferenceId = $request->conference_id;

        $StudentsList = new ConferenceStudentsList();

        $StudentsList->conference_id = $ConferenceId;
        $StudentsList->student_id = $request->student_id;

        $StudentsList->save();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> " Student Added Successfully"
                        )
        );
    }

    public function delete_students_list(Request $request)
    {
    	// \Log::info($request->all());

        $ConferenceId = $request->conference_id;

        // echo $request->student_id;
        // exit;

        $ConferenceStudentsList = ConferenceStudentsList::where('conference_id', $ConferenceId)
                        ->where('student_id', $request->student_id)->delete();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> " Student Removed Successfully"
                        )
        );
    }


    public function students_list($id){

        $title = "Students List";
        // $Courses = Courses::where('id', $id)->first();
        $ConferenceStudentsList = ConferenceStudentsList::join('users', 'users.id', 'conference_students_list.student_id')
                                    ->join('students_profile', 'users.id', 'students_profile.student_id')
                                    ->select('users.*', 'students_profile.gender')
                                    ->where('conference_students_list.conference_id', $id)
                                    ->get();


        return view('UI.teachers.conference.students_list', compact('title', 'ConferenceStudentsList'));
    }

    public function add_groups($id){

        $title = "Add Groups";
        $Groups = Groups::get();
        $Conference = Conference::where('id', $id)->first();
        // $StudentsList = Users::where('user_type', 3)->orderBy('created_at', 'DESC')->get();

        return view('UI.teachers.conference.add_groups', compact('title', 'Groups', 'Conference'));
    }

    public function update_group_of_students(Request $request){
        $group_id = $request->group_id;
        $conference_id = $request->conference_id;

        ConferenceGroup::where('conference_id', $conference_id)
                                ->delete();

        $CheckConferenceGroup = ConferenceGroup::where('group_id', $group_id)
                                                            ->where('conference_id', $conference_id)
                                                            ->first();

        if($CheckConferenceGroup != null){
            $CheckConferenceGroup->group_id = $group_id;
            $CheckConferenceGroup->conference_id = $conference_id;

            $CheckConferenceGroup->save();
        }else{
            $ConferenceGroup = new ConferenceGroup();
            $ConferenceGroup->group_id = $group_id;
            $ConferenceGroup->conference_id = $conference_id;

            $ConferenceGroup->save();
        }


        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> " Student Group Added Successfully"
                        )
        );
    }
}
