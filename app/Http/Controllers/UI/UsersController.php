<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Users;

use App\Models\UI\TeachersPersonalDetails;
use App\Models\UI\TeacherFees;
use App\Models\UI\TeacherTuition;
use App\Models\UI\TeachersAdditionalInfo;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class UsersController extends Controller
{
    public function error(){
        return view('UI.error');
    }

    public function login(){
        return view('UI.login');
    }

    public function register(){
        return view('UI.register');
    }

    public function edit_profile(){
        $title = "Edit Profile";
        $UserId = Session::get('TeacherId');

        $Users = Users::where('id', $UserId)->first();
        $TeachersPersonalDetails = TeachersPersonalDetails::where('teacher_id', $UserId)->first();
        return view('UI.teachers.profile.edit_profile', compact('title', 'TeachersPersonalDetails', 'Users'));
    }


    public function update_profile(Request $request){

        $UserId = Session::get('TeacherId');

        // echo json_encode($request->info);
        // exit;

        $CheckTeachersPersonalDetails = TeachersPersonalDetails::where('teacher_id' ,$UserId)->first();

        if($CheckTeachersPersonalDetails != null){

            $Users = Users::where('id', $UserId)->first();

            $Users->mobile = $request->mobile;
            $Users->country = $request->country;
            $Users->state = $request->state;
            $Users->city = $request->city;
            $Users->area = $request->area;
            $Users->pincode = $request->pincode;
            $Users->save();

            $CheckTeachersPersonalDetails->teacher_id = $UserId;

            if($request->hasfile('profile_pic')){
                $extension = $request->file('profile_pic')->getClientOriginalExtension();
                $dir = 'UI/teachers_profile_pic/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('profile_pic')->move($dir, $filename);

                $CheckTeachersPersonalDetails->profile_pic = $filename;
            }else{
                $CheckTeachersPersonalDetails->profile_pic = $CheckTeachersPersonalDetails->profile_pic;
            }

            $CheckTeachersPersonalDetails->gender = $request->gender;
            $CheckTeachersPersonalDetails->qualification = $request->qualification;

            $CheckTeachersPersonalDetails->tutor_type = $request->tutor_type;
            $CheckTeachersPersonalDetails->teaching_subjects = $request->teaching_subjects;
            $CheckTeachersPersonalDetails->teaching_language = $request->teaching_language;
            $CheckTeachersPersonalDetails->no_of_student_present = $request->no_of_student_present;
            $CheckTeachersPersonalDetails->website_link = $request->website_link;
            $CheckTeachersPersonalDetails->instagram_link = $request->instagram_link;
            $CheckTeachersPersonalDetails->facebook_link = $request->facebook_link;
            $CheckTeachersPersonalDetails->youtube_channel = $request->youtube_channel;
            $CheckTeachersPersonalDetails->school_clg_boards = $request->school_clg_boards;
            $CheckTeachersPersonalDetails->student_grades_studying = $request->student_grades_studying;
            $CheckTeachersPersonalDetails->student_age = $request->student_age;
            $CheckTeachersPersonalDetails->status = 1;


            if($request->hasfile('logo')){
                $extension = $request->file('logo')->getClientOriginalExtension();
                $dir = 'UI/teachers_logo/';
                $filename1 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('logo')->move($dir, $filename1);

                $CheckTeachersPersonalDetails->logo = $filename1;
            }else{
                $CheckTeachersPersonalDetails->logo = $CheckTeachersPersonalDetails->logo;
            }

            $CheckTeachersPersonalDetails->total_no_of_students = $request->total_no_of_students;
            $CheckTeachersPersonalDetails->about_me = $request->profile_description;

            $AddCheckTeachersPersonalDetails = $CheckTeachersPersonalDetails->save();



            if(isset($request->fees)){
                TeacherFees::where('teacher_id', $UserId)->delete();
                foreach ($request->fees as $key => $value) {

                    // echo json_encode($value);
                    // exit;

                    $TeacherFees = new TeacherFees();

                    $TeacherFees->teacher_id = $UserId;
                    $TeacherFees->grade = $value['grade'];
                    $TeacherFees->subject = $value['subject'];
                    $TeacherFees->hourly_rate = $value['hourly_rate'];
                    $TeacherFees->monthly_rate = $value['monthly_rate'];
                    $TeacherFees->yearly_rate = $value['yearly_rate'];

                    $TeacherFees->save();
                }
            }





            if(isset($request->tuition)){
                TeacherTuition::where('teacher_id', $UserId)->delete();
            foreach ($request->tuition as $key => $value) {

                $TeacherTuition = new TeacherTuition();

                $TeacherTuition->teacher_id = $UserId;
                $TeacherTuition->days = $value['days'];
                $TeacherTuition->time_option1 = $value['option1'];
                $TeacherTuition->time_option2 = $value['option2'];
                $TeacherTuition->time_option3 = $value['option3'];
                $TeacherTuition->time_option4 = $value['option4'];
                $TeacherTuition->time_option5 = $value['option5'];

                $TeacherTuition->save();
            }
        }



        if(isset($request->info)){
                TeachersAdditionalInfo::where('teacher_id', $UserId)->delete();
            foreach ($request->info as $key => $value) {

                $TeachersAdditionalInfo = new TeachersAdditionalInfo();

                $TeachersAdditionalInfo->teacher_id = $UserId;
                $TeachersAdditionalInfo->first_name = $value['fname'];
                $TeachersAdditionalInfo->last_name = $value['lname'];
                $TeachersAdditionalInfo->gender = $value['gender'];
                $TeachersAdditionalInfo->experience = $value['exp'];
                $TeachersAdditionalInfo->subjects = $value['subject'];

                $TeachersAdditionalInfo->save();
            }
        }


            return redirect()->back()->with('message','Teacher Profile Updated Successfully');
        }else{

            $TeachersPersonalDetails = new TeachersPersonalDetails();

            $TeachersPersonalDetails->teacher_id = $UserId;

            if($request->hasfile('profile_pic')){
                $extension = $request->file('profile_pic')->getClientOriginalExtension();
                $dir = 'UI/teachers_profile_pic/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('profile_pic')->move($dir, $filename);

                $TeachersPersonalDetails->profile_pic = $filename;
            }

            $TeachersPersonalDetails->gender = $request->gender;
            $TeachersPersonalDetails->qualification = $request->qualification;

            $TeachersPersonalDetails->tutor_type = $request->tutor_type;
            $TeachersPersonalDetails->teaching_subjects = $request->teaching_subjects;
            $TeachersPersonalDetails->teaching_language = $request->teaching_language;
            $TeachersPersonalDetails->no_of_student_present = $request->no_of_student_present;
            $TeachersPersonalDetails->website_link = $request->website_link;
            $TeachersPersonalDetails->instagram_link = $request->instagram_link;
            $TeachersPersonalDetails->facebook_link = $request->facebook_link;
            $TeachersPersonalDetails->youtube_channel = $request->youtube_channel;
            $TeachersPersonalDetails->school_clg_boards = $request->school_clg_boards;
            $TeachersPersonalDetails->student_grades_studying = $request->student_grades_studying;
            $TeachersPersonalDetails->student_age = $request->student_age;
            $TeachersPersonalDetails->status = 1;

            if($request->hasfile('logo')){
                $extension = $request->file('logo')->getClientOriginalExtension();
                $dir = 'UI/teachers_logo/';
                $filename1 = uniqid() . '_' . time() . '.' . $extension;
                $request->file('logo')->move($dir, $filename1);

                $TeachersPersonalDetails->logo = $filename1;
            }

            $TeachersPersonalDetails->total_no_of_students = $request->total_no_of_students;
            $TeachersPersonalDetails->about_me = $request->profile_description;

            $AddTeachersPersonalDetails = $TeachersPersonalDetails->save();

        if(isset($request->fees)){
            TeacherFees::where('teacher_id', $UserId)->delete();
            foreach ($request->fees as $key => $value) {

                $TeacherFees = new TeacherFees();

                $TeacherFees->teacher_id = $UserId;
                $TeacherFees->grade = $value['grade'];
                $TeacherFees->subject = $value['subject'];
                $TeacherFees->hourly_rate = $value['hourly_rate'];
                $TeacherFees->monthly_rate = $value['monthly_rate'];
                $TeacherFees->yearly_rate = $value['yearly_rate'];

                $TeacherFees->save();
            }
        }



            if(isset($request->tuition)){
                TeacherTuition::where('teacher_id', $UserId)->delete();
                foreach ($request->tuition as $key => $value) {

                    $TeacherTuition = new TeacherTuition();

                    $TeacherTuition->teacher_id = $UserId;
                    $TeacherTuition->days = $value['days'];
                    $TeacherTuition->time_option1 = $value['option1'];
                    $TeacherTuition->time_option2 = $value['option2'];
                    $TeacherTuition->time_option3 = $value['option3'];
                    $TeacherTuition->time_option4 = $value['option4'];
                    $TeacherTuition->time_option5 = $value['option5'];

                    $TeacherTuition->save();
                }
            }



        if(isset($request->info)){
                TeachersAdditionalInfo::where('teacher_id', $UserId)->delete();
            foreach ($request->info as $key => $value) {

                $TeachersAdditionalInfo = new TeachersAdditionalInfo();

                $TeachersAdditionalInfo->teacher_id = $UserId;
                $TeachersAdditionalInfo->first_name = $value['fname'];
                $TeachersAdditionalInfo->last_name = $value['lname'];
                $TeachersAdditionalInfo->gender = $value['gender'];
                $TeachersAdditionalInfo->experience = $value['exp'];
                $TeachersAdditionalInfo->subjects = $value['subject'];

                $TeachersAdditionalInfo->save();
            }
        }

            return redirect()->back()->with('message','Teacher Profile Updated Successfully');
        }

    }



    public function admin_login(Request $request){
        $Email = $request->email;
        $Password = $request->password;

        $CheckEmail = Users::where('email', $Email)->first();

        $request->session()->put('last_login_timestamp', time());

        if($CheckEmail == null){

            return redirect()->back()->with('message','Please check your credentials');

        }else{

            if($CheckEmail->status == 0){

                return redirect()->back()->with('message','Your account is not activated. Please contact your administrator...');

            }elseif($CheckEmail->user_type == 1){
                if (Auth::guard('super_admin')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('AdminName', Auth::guard('super_admin')->user()->name);
                    $request->session()->put('AdminEmail', Auth::guard('super_admin')->user()->email);
                    $request->session()->put('AdminId', Auth::guard('super_admin')->user()->id);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('admin/dashboard');
                }else{
                    return redirect()->back()->with('message','Please check your credentials');

                }

            }else{
                if (Auth::guard('teacher')->attempt(['email' => $Email, 'password' => $Password])) {
                    $request->session()->put('Teachername',$CheckEmail->name);
                    $request->session()->put('TeacherEmail', $CheckEmail->email);
                    $request->session()->put('TeacherId', $CheckEmail->id);
                    // return response()->json(array(
                    //     "error"=>FALSE,
                    //     "message"=> "Login successfully",
                    //     "type" => 1
                    // ));
                    return redirect('teachers/dashboard');
                }else{
                    return redirect()->back()->with('message','Please check your credentials');

                }


            }
            // }elseif($CheckEmail->user_type == 2){
            //     if (Auth::guard('teacher')->attempt(['email' => $Email, 'password' => $Password])) {
            //         $request->session()->put('Teachername', Auth::guard('teacher')->user()->name);
            //         $request->session()->put('TeacherEmail', Auth::guard('teacher')->user()->email);
            //         $request->session()->put('TeacherId', Auth::guard('teacher')->user()->id);
            //         // return response()->json(array(
            //         //     "error"=>FALSE,
            //         //     "message"=> "Login successfully",
            //         //     "type" => 1
            //         // ));
            //         return redirect('teachers/dashboard');
            //     }else{
            //         return redirect()->back()->with('message','Please check your credentials');

            //     }
            // }
            // elseif($CheckEmail->user_type == 3){
            //     if (Auth::guard('student')->attempt(['email' => $Email, 'password' => $Password])) {
            //         $request->session()->put('Studentname', Auth::guard('student')->user()->name);
            //         $request->session()->put('StudentEmail', Auth::guard('student')->user()->email);
            //         $request->session()->put('StudentId', Auth::guard('student')->user()->id);
            //         // return response()->json(array(
            //         //     "error"=>FALSE,
            //         //     "message"=> "Login successfully",
            //         //     "type" => 1
            //         // ));
            //         return redirect('students/dashboard');
            //     }else{
            //         return redirect()->back()->with('message','Please check your credentials');

            //     }
            // }elseif($CheckEmail->user_type == 4){
            //     if (Auth::guard('affiliate')->attempt(['email' => $Email, 'password' => $Password])) {
            //         $request->session()->put('Affiliatename', Auth::guard('affiliate')->user()->name);
            //         $request->session()->put('AffiliateEmail', Auth::guard('affiliate')->user()->email);
            //         $request->session()->put('AffiliateId', Auth::guard('affiliate')->user()->id);
            //         // return response()->json(array(
            //         //     "error"=>FALSE,
            //         //     "message"=> "Login successfully",
            //         //     "type" => 1
            //         // ));
            //         return redirect('affiliate/dashboard');
            //     }else{
            //         return redirect()->back()->with('message','Please check your credentials');

            //     }
            // }
        }

    }


    public function add_users(Request $request){
        $Users = new Users();

        $Users->first_name = $request->first_name;
        $Users->last_name = $request->last_name;
        $Users->email = $request->email;
        $Users->mobile = $request->mobile;
        $Users->password = Hash::make($request->password);
        $Users->status = 0;
        // $Users->user_type = $request->user_type;
        $Users->user_type = 3;
        $Users->country = $request->country;
        $Users->state = $request->state;
        $Users->city = $request->city;
        $Users->area = $request->area;
        $Users->pincode = $request->pincode;

        $AddUsers = $Users->save();

        return redirect()->back()->with('message','User Registered Successfully');
    }



    public function delete_fees(Request $request)
    {
        $fees_id = $request->id;

        // echo $request->student_id;
        // exit;
        $TeacherFees = TeacherFees::where('id', $fees_id)->delete();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> "Fees Removed Successfully"
                        )
        );
    }


    public function delete_tuition(Request $request)
    {
        $tuition_id = $request->id;

        // echo $request->student_id;
        // exit;
        $TeacherTuition = TeacherTuition::where('id', $tuition_id)->delete();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> "Tuition Removed Successfully"
                        )
        );
    }


    public function delete_info(Request $request)
    {
        $info_id = $request->id;

        // echo $request->student_id;
        // exit;
        $TeachersAdditionalInfo = TeachersAdditionalInfo::where('id', $info_id)->delete();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> "Additional Info Removed Successfully"
                        )
        );
    }


    public function teacher_logout()
    {
        Auth::guard('teacher')->logout();
        Session::flush();
        return redirect('/');
    }


    public function student_logout()
    {
        Auth::guard('student')->logout();
        return redirect('/');
    }

    public function admin_logout()
    {
        Auth::guard('super_admin')->logout();
        Session::flush();
        return redirect('/');
    }

    public function affiliate_logout()
    {
        Auth::guard('affiliate')->logout();
        return redirect('/');
    }

    public function user_logout()
    {
        Auth::guard('teacher')->logout();
        Session::flush();
        return redirect('/');
    }

    public function auth_login()
    {
        echo "dsa";
        exit;
        if (Auth::check()) {
            return redirect()->intended('/');
        }

        return App::make('auth0')->login(
            null,
            null,
            ['scope' => 'openid name email email_verified'],
            'code'
        );
    }
}
