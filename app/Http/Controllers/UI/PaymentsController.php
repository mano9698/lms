<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\PaymentCollection;
use App\Models\UI\PaymentTransferred;
use App\Models\UI\Users;

use Illuminate\Support\Facades\Auth;
use Session;

class PaymentsController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('guest:super_admin')->except('dashboard');
        if(Auth::guard('super_admin')->check()){
            $this->middleware('guest:super_admin');
        }elseif(Auth::guard('teacher')->check()){
            $this->middleware('guest:teacher');
        }
    }

    public function payment_collection_list(){
        $title = "Payment Collection List";
        $PaymentCollection = PaymentCollection::join('users', 'users.id', '=', 'payment_collection.affiliate')
                        ->select('users.id as UserId','users.first_name as AFirstName', 'users.last_name as ALastName', 'payment_collection.*')
                        ->orderBy('created_at', 'DESC')->get();

        return view('UI.admin.payment_collection.collection_list', compact('title', 'PaymentCollection'));
    }


    public function add_payment_collection(){
        $title = "Add Payment Collection";
        $Affiliates = Users::where('user_type', 4)->get();
        return view('UI.admin.payment_collection.add_collection', compact('title', 'Affiliates'));
    }

    public function edit_payment_collection($id){
        $title = "Edit Payment Collection";
        $PaymentCollection = PaymentCollection::where('id', $id)->first();
        $Affiliates = Users::where('user_type', 4)->get();
        return view('UI.admin.payment_collection.edit_collection', compact('title', 'PaymentCollection', 'Affiliates'));
    }

    public function store_payment_collection(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $PaymentCollection = new PaymentCollection();

        $PaymentCollection->first_name = $request->first_name;
        $PaymentCollection->last_name = $request->last_name;
        $PaymentCollection->email = $request->email;
        $PaymentCollection->contact = $request->contact;
        $PaymentCollection->course_name = $request->course_name;
        $PaymentCollection->amount_collected = $request->amount_collected;
        $PaymentCollection->collection_mode = $request->collection_mode;
        $PaymentCollection->collection_ref_no = $request->collection_ref_no;
        $PaymentCollection->collection_date = $request->collection_date;
        $PaymentCollection->affiliate = $request->affiliate;
        $PaymentCollection->status = 1;

        $AddPaymentCollection = $PaymentCollection->save();

        return redirect()->back()->with('message','Payment Collection Added Successfully');
    }


    public function update_payment_collection(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $id = $request->id;

        $PaymentCollection = PaymentCollection::where('id', $id)->first();

        $PaymentCollection->first_name = $request->first_name;
        $PaymentCollection->last_name = $request->last_name;
        $PaymentCollection->email = $request->email;
        $PaymentCollection->contact = $request->contact;
        $PaymentCollection->course_name = $request->course_name;
        $PaymentCollection->amount_collected = $request->amount_collected;
        $PaymentCollection->collection_mode = $request->collection_mode;
        $PaymentCollection->collection_ref_no = $request->collection_ref_no;
        $PaymentCollection->collection_date = $request->collection_date;
        $PaymentCollection->affiliate = $request->affiliate;
        $PaymentCollection->status = 1;

        $AddPaymentCollection = $PaymentCollection->save();

        return redirect()->back()->with('message','Payment Collection Updated Successfully');
    }


    public function delete_payment_collection($id)
    {

        $PaymentCollection = PaymentCollection::where('id', $id)->delete();

        return redirect()->back()->with('message','Payment Collection Deleted Successfully');
    }

// Payment Transeffred

public function payment_transferred_list(){
    $title = "Payment Transferred List";
    $PaymentTransferred = PaymentTransferred::join('users', 'users.id', '=', 'payment_transferred.affiliate')
    ->select('users.id as UserId','users.first_name as AFirstName', 'users.last_name as ALastName', 'payment_transferred.*')
    ->orderBy('created_at', 'DESC')->get();
    return view('UI.admin.payment_transferred.transferred_list', compact('title', 'PaymentTransferred'));
}


public function add_payment_transferred(){
    $title = "Add Payment Transferred";
    $Affiliates = Users::where('user_type', 4)->get();
    return view('UI.admin.payment_transferred.add_transferred', compact('title', 'Affiliates'));
}

public function edit_payment_transferred($id){
    $title = "Edit Payment Transferred";
    $Affiliates = Users::where('user_type', 4)->get();
    $PaymentTransferred = PaymentTransferred::where('id', $id)->first();
    return view('UI.admin.payment_transferred.edit_transferred', compact('title', 'PaymentTransferred', 'Affiliates'));
}

public function store_payment_transferred(Request $request){
    // if(Auth::guard('super_admin')->check()){
    //     $UserId = Session::get('AdminId');
    // }else{
    //     $UserId = Session::get('TeacherId');
    // }

    $PaymentTransferred = new PaymentTransferred();

    $PaymentTransferred->affiliate = $request->affiliate;
    $PaymentTransferred->commision_month = $request->commision_month;
    $PaymentTransferred->total_collection = $request->total_collection;
    $PaymentTransferred->commission_due_date = $request->commission_due_date;
    $PaymentTransferred->commission_paid_amount = $request->commission_paid_amount;
    $PaymentTransferred->paid_date = $request->paid_date;
    $PaymentTransferred->payment_mode = $request->payment_mode;
    $PaymentTransferred->payment_ref_no = $request->payment_ref_no;
    $PaymentTransferred->status = 1;

    $AddPaymentTransferred = $PaymentTransferred->save();

    return redirect()->back()->with('message','Payment Transferred Added Successfully');
}


public function update_payment_transferred(Request $request){
    // if(Auth::guard('super_admin')->check()){
    //     $UserId = Session::get('AdminId');
    // }else{
    //     $UserId = Session::get('TeacherId');
    // }

    $id = $request->id;

    $PaymentTransferred = PaymentTransferred::where('id', $id)->first();

    $PaymentTransferred->affiliate = $request->affiliate;
    $PaymentTransferred->commision_month = $request->commision_month;
    $PaymentTransferred->total_collection = $request->total_collection;
    $PaymentTransferred->commission_due_date = $request->commission_due_date;
    $PaymentTransferred->commission_paid_amount = $request->commission_paid_amount;
    $PaymentTransferred->paid_date = $request->paid_date;
    $PaymentTransferred->payment_mode = $request->payment_mode;
    $PaymentTransferred->payment_ref_no = $request->payment_ref_no;
    $PaymentTransferred->status = 1;

    $AddPaymentTransferred = $PaymentTransferred->save();

    return redirect()->back()->with('message','Payment Transferred Updated Successfully');
}


public function delete_payment_transferred($id)
{

    $PaymentTransferred = PaymentTransferred::where('id', $id)->delete();

    return redirect()->back()->with('message','Payment Transferred Deleted Successfully');
}

}
