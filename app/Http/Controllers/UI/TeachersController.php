<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Courses;
use App\Models\UI\Groups;
use App\Models\UI\Users;
use App\Models\UI\Uploads;
use App\Models\UI\Conference;
use Session;

class TeachersController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('guest:super_admin')->except('dashboard');
        // $this->middleware('guest:teacher');

    }

    public function dashboard(){
        $title = "Teachers Dashboard";
        $UserId = Session::get('TeacherId');
        $Courses = Courses::where('teacher_id', $UserId)
                            ->get();

        $Groups = Groups::where('teacher_id', $UserId)
                    ->count();

        $Uploads = Uploads::where('user_id', $UserId)
        ->count();

        $Conference = Conference::where('teacher_id', $UserId)
                    ->count();

        $Groups = Groups::where('teacher_id', $UserId)
                    ->count();
        return view('UI.teachers.dashboard', compact('title', 'Courses', 'Groups', 'Uploads', 'Conference'));
    }

    public function students_list(){
        $title = "Students List";

        $UserId = Session::get('TeacherId');

        $StudentsList = Users::where('user_type', 3)
                        ->select('users.*')
                        ->join('add_teachers_to_students', 'add_teachers_to_students.student_id', '=', 'users.id')
                        ->where('add_teachers_to_students.teacher_id', $UserId)
                        ->orderBy('users.created_at', 'DESC')->get();

        // echo json_encode($StudentsList);
        // exit;

        return view('UI.teachers.profile.students_list', compact('title', 'StudentsList'));
    }
}
