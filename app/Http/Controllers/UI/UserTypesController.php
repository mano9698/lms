<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\UserTypes;
use App\Models\UI\UserPermissions;
use App\Models\UI\UserSubPermissions;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class UserTypesController extends Controller
{
    public function user_types_list(){
        $title = "User Types";

        $UserTypes = UserTypes::get();

        return view('UI.admin.user_type.list', compact('title', 'UserTypes'));
    }

    public function add_user_types(){
        $title = "Add User Types";

        $UserTypes = UserTypes::get();


        return view('UI.admin.user_type.add_user_type', compact('title', 'UserTypes'));
    }

    public function edit_user_types($id){
        $title = "Edit User Types";

        $UserTypes = UserTypes::where('id', $id)->first();
        $UserPermissions = UserPermissions::where('id', $id)->get();
        // echo json_encode($UserTypes);
        // exit;

        return view('UI.admin.user_type.edit_user_type', compact('title', 'UserTypes', 'UserPermissions'));
    }

    public function view_user_types($id){
        $title = "View User Types";

        $UserTypes = UserTypes::where('id', $id)->first();
        $UserPermissions = UserPermissions::where('id', $id)->get();
        // echo json_encode($UserTypes);
        // exit;

        return view('UI.admin.user_type.view_permissions', compact('title', 'UserTypes', 'UserPermissions'));
    }

    public function add_permission($id){
        $title = "Add Permission";

        $UserTypes = UserTypes::where('id', $id)->first();
        $UserPermissions = UserPermissions::where('user_type_id', $id)->get();

        return view('UI.admin.user_type.add_permissions', compact('title', 'UserTypes', 'UserPermissions'));
    }

    public function store_user_type(Request $request){

        $UserTypes = new UserTypes();

        $UserTypes->name = $request->name;
        $UserTypes->roles = $request->user_type;
        $UserTypes->save();

        return redirect('user_type/add_permission/'.$UserTypes->id);
        // $Permissions = $request->permission;
        // foreach($Permissions as $Permission){


        //     $UserPermissions = new UserPermissions();
        //     // echo json_encode($UserPermissions);
        //     $UserPermissions->user_type_id = $UserTypes->id;

        //     $UserPermissions->permissions = $Permission;
        //     $UserPermissions->save();


        //     $UserSubPermissions = new UserSubPermissions();

        //     $UserSubPermissions->user_permission_id = $UserPermissions->id;

        //     $view_permission = $request->view_permission;



        //         foreach($view_permission as $view){

        //             // $UserPermissions->user_type_id = $UserTypes->id;
        //             $UserSubPermissions->view = $view;
        //         }

        //     $add_permission = $request->add_permission;
        //     foreach($add_permission as $add){
        //         // $UserPermissions->user_type_id = $UserTypes->id;
        //         $UserSubPermissions->add = $add;
        //     }

        // $edit_permission = $request->edit_permission;
        //     foreach($edit_permission as $edit){
        //         // $UserPermissions->user_type_id = $UserTypes->id;
        //         $UserSubPermissions->edit = $edit;
        //     }

        //     // echo json_encode($edit_permission);
        //     // exit;
        // $delete_permission = $request->delete_permission;
        //     foreach($delete_permission as $delete){
        //         // $UserPermissions->user_type_id = $UserTypes->id;
        //         $UserSubPermissions->delete = $delete;
        //     }
        //     $UserSubPermissions->save();


        // }

        // return redirect()->back()->with('message','UserTypes Added Successfully');
    }

    public function store_permissions(Request $request){

        switch ($request->action) {
            case 'add_permission':
                $UserPermissions = new UserPermissions();

                $UserPermissions->user_type_id = $request->Usertypeid;
                $UserPermissions->permissions = $request->modules;
                $UserPermissions->view_only = $request->view_only;
                $UserPermissions->add_only = $request->add_only;
                $UserPermissions->edit_only = $request->edit_only;
                $UserPermissions->delete_only = $request->delete_only;

                $UserPermissions->save();

                return redirect()->back()->with('message','User Permission Added Successfully');

            case 'update_permission':
                $id = $request->id;

                $UserPermissions = UserPermissions::where('id', $id)->first();

                $UserPermissions->permissions = $request->modules;
                $UserPermissions->view_only = $request->view_only;
                $UserPermissions->add_only = $request->add_only;
                $UserPermissions->edit_only = $request->edit_only;
                $UserPermissions->delete_only = $request->delete_only;

                // echo json_encode($request->delete_only);
                // exit;

                $UserPermissions->save();

                return redirect()->back()->with('message','User Permission Updated Successfully');
            break;
        }
    }


    public function update_permissions(Request $request){

        switch ($request->action) {
            case 'add_permission':
                $UserPermissions = new UserPermissions();

                $UserPermissions->user_type_id = $request->Usertypeid;
                $UserPermissions->permissions = $request->modules;
                $UserPermissions->view_only = $request->view_only;
                $UserPermissions->add_only = $request->add_only;
                $UserPermissions->edit_only = $request->edit_only;
                $UserPermissions->delete_only = $request->delete_only;

                $UserPermissions->save();

                return redirect()->back()->with('message','User Permission Added Successfully');

            case 'update_permission':
                $id = $request->id;

                $UserPermissions = UserPermissions::where('id', $id)->first();

                $UserPermissions->permissions = $request->modules;
                $UserPermissions->view_only = $request->view_only;
                $UserPermissions->add_only = $request->add_only;
                $UserPermissions->edit_only = $request->edit_only;
                $UserPermissions->delete_only = $request->delete_only;

                echo json_encode($request->delete_only);
                exit;

                $UserPermissions->save();

                return redirect()->back()->with('message','User Permission Updated Successfully');
            break;
        }
    }

    public function delete_user_type($id)
{

    $UserTypes = UserTypes::where('id', $id)->delete();

    $UserPermissions = UserPermissions::where('user_type_id', $id)->get();

    return redirect()->back()->with('message','UserTypes Deleted Successfully');
}


public function delete_permission($id)
{

    $UserPermissions = UserPermissions::where('id', $id)->delete();

    return redirect()->back()->with('message','User Permission Deleted Successfully');
}


    public function getpermission($id){
        $UserPermissions = UserPermissions::where('id', $id)
        ->first();

        return response()->json(array(
            "error"=>False,
            "data"=> $UserPermissions
        ));
    }
}
