<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Users;

use App\Models\UI\Videos;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class VideosController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('guest:super_admin')->except('dashboard');
        if(Auth::guard('super_admin')->check()){
            $this->middleware('guest:super_admin');
        }elseif(Auth::guard('teacher')->check()){
            $this->middleware('guest:teacher');
        }
    }

    public function videos_list(){
        $title = "Videos List";
        $Videos = Videos::select('sample_videos.*', 'users.first_name', 'users.last_name')
                        ->join('users', 'sample_videos.teacher_id', 'users.id')
                        ->orderBy('sample_videos.created_at', 'DESC')->get();
        return view('UI.admin.sample_videos.videos_list', compact('title', 'Videos'));
    }


    public function add_sample_videos(){
        $title = "Add Sample Videos";
        $Teachers = Users::where('user_main_type', 1)->get();
        return view('UI.admin.sample_videos.add_video', compact('title', 'Teachers'));
    }

    public function edit_sample_videos($id){
        $title = "Edit Sample Videos";
        $Videos = Videos::where('id', $id)->first();
        $Teachers = Users::where('user_main_type', 1)->get();
        return view('UI.admin.sample_videos.edit_video', compact('title', 'Videos', 'Teachers'));    }

    public function store_sample_videos(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $Videos = new Videos();

        $Videos->file = $request->file;
        $Videos->teacher_id = $request->teacher_id;
        $Videos->status = 1;

        $AddVideos = $Videos->save();

        return redirect()->back()->with('message','Videos Added Successfully');
    }


    public function update_sample_videos(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $Id = $request->id;

        $Videos = Videos::where('id', $Id)->first();

        $Videos->file = $request->file;
        $Videos->teacher_id = $request->teacher_id;
        $Videos->status = 1;

        $AddVideos = $Videos->save();

        return redirect()->back()->with('message','Videos Updated Successfully');
    }


    public function delete_sample_videos($id)
{

    $Videos = Videos::where('id', $id)->delete();

    return redirect()->back()->with('message','Videos Deleted Successfully');
}
}
