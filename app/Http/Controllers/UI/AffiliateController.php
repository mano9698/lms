<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\PaymentCollection;
use App\Models\UI\PaymentTransferred;
use App\Models\UI\AffiliateProfile;

use Illuminate\Support\Facades\Auth;
use Session;

class AffiliateController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('guest:super_admin')->except('dashboard');
        if(Auth::guard('super_admin')->check()){
            $this->middleware('guest:super_admin');
        }elseif(Auth::guard('teacher')->check()){
            $this->middleware('guest:teacher');
        }
    }

    public function dashboard(){
        $title = "Dashboard";
        return view('UI.affiliate.dashboard', compact('title'));
    }

    public function payment_collection_list(){
        $title = "Payment Collection List";
        // $UserId = Session::get('AffiliateId');
        $UserId = Session::get('TeacherId');
        $PaymentCollection = PaymentCollection::where('affiliate', $UserId)->orderBy('created_at', 'DESC')->get();
        return view('UI.affiliate.payment_collection_list', compact('title', 'PaymentCollection'));
    }

    public function payment_transferred_list(){
        $title = "Payment Transferred List";
        $UserId = Session::get('TeacherId');
        $PaymentTransferred = PaymentTransferred::where('affiliate', $UserId)->orderBy('created_at', 'DESC')->get();
        return view('UI.affiliate.payment_transferred_list', compact('title', 'PaymentTransferred'));
    }

    public function edit_profile(){
        $title = "Edit Profile";
        $UserId = Session::get('TeacherId');

        $AffiliateProfile = AffiliateProfile::where('affiliate_id', $UserId)->first();

        // echo json_encode($StudentsProfile->profile_pic);
        // exit;

        return view('UI.affiliate.profile.edit_profile', compact('title', 'AffiliateProfile'));
    }


    public function update_affiliate_profile(Request $request){
        $UserId = Session::get('TeacherId');

        // echo json_encode($request->info);
        // exit;

        $CheckAffiliateProfile = AffiliateProfile::where('affiliate_id' ,$UserId)->first();

        if($CheckAffiliateProfile != null){
            $CheckAffiliateProfile->affiliate_id = $UserId;

            if($request->hasfile('profile_pic')){
                $extension = $request->file('profile_pic')->getClientOriginalExtension();
                $dir = 'UI/affiliate_profile_pic/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('profile_pic')->move($dir, $filename);

                $CheckAffiliateProfile->profile_pic = $filename;
            }else{
                $CheckAffiliateProfile->profile_pic = $CheckAffiliateProfile->profile_pic;
            }

            $CheckAffiliateProfile->gender = $request->gender;
            $CheckAffiliateProfile->qualification = $request->qualification;

            $AddCheckAffiliateProfile = $CheckAffiliateProfile->save();

            return redirect()->back()->with('message','Profile Updated Successfully');

        }else{

            $AffiliateProfile = new AffiliateProfile();

            $AffiliateProfile->affiliate_id = $UserId;

            if($request->hasfile('profile_pic')){
                $extension = $request->file('profile_pic')->getClientOriginalExtension();
                $dir = 'UI/affiliate_profile_pic/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('profile_pic')->move($dir, $filename);

                $AffiliateProfile->profile_pic = $filename;
            }

            $AffiliateProfile->gender = $request->gender;
            $AffiliateProfile->qualification = $request->qualification;

            $AddAffiliateProfile = $AffiliateProfile->save();



            return redirect()->back()->with('message','Profile Updated Successfully');
        }
    }


}
