<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Models\UI\Uploads;
use App\Models\UI\Users;
use App\Models\UI\MediaStudentsList;
use App\Models\UI\MediaGroupStudentsList;
use App\Models\UI\Groups;

use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Facades\Auth;
use Session;

use Youtube;

class UploadsController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('guest:super_admin')->except('dashboard');
        if(Auth::guard('super_admin')->check()){
            $this->middleware('guest:super_admin');
        }
    }

    // public function store(Request $request)
    // {
    //     $video = Youtube::upload($request->file('video')->getPathName(), [
    //         'title'       => $request->input('title'),
    //         'description' => $request->input('description')
    //     ]);

    //     return "Video uploaded successfully. Video ID is ". $video->getVideoId();
    // }

    public function uploads_list(){
        if(Auth::guard('super_admin')->check()){
            $title = "Uploads List";
            $Uploads = Uploads::orderBy('created_at', 'DESC')->get();
        }else{
            $UserId = Session::get('TeacherId');

            $title = "Uploads List";
            $Uploads = Uploads::where('user_id', $UserId)->orderBy('created_at', 'DESC')->get();
        }


        return view('UI.teachers.uploads.uploads_list', compact('title', 'Uploads'));
    }

    public function add_uploads(){
        $title = "Add Uploads";
        return view('UI.teachers.uploads.new_upload', compact('title'));
    }

    public function edit_uploads($id){
        $title = "Edit Uploads";
        $Uploads = Uploads::where('id', $id)->first();
        return view('UI.teachers.uploads.edit_upload', compact('title', 'Uploads'));
    }

    public function store_uploads(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $Uploads = new Uploads();

        $Uploads->user_id = $UserId;
        $Uploads->file_type = $request->file_type;

        // echo $request->video_link;
        // exit;

        if($request->file_type == 1){
            $Uploads->file_name = $request->file_names;

            if($request->hasfile('upload_file')){
                $extension = $request->file('upload_file')->getClientOriginalExtension();
                $dir = 'UI/uploads/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('upload_file')->move($dir, $filename);

                $Uploads->files = $filename;
            }


        }else if($request->file_type == 2){
            $Uploads->file_name = $request->file_name;

            // $video = Youtube::upload($request->file('upload_video')->getPathName(), [
            //     'title'       => $request->title,
            //     'description' => "test content"
            // ]);


            if($request->video_type == 1){
            //     echo "dsada";
            //  exit;

                // if($request->hasfile('upload_video')){
                //     $extension = $request->file('upload_video')->getClientOriginalExtension();
                //     $dir = 'UI/uploads/';
                //     $filename1 = uniqid() . '_' . time() . '.' . $extension;
                //     $request->file('upload_video')->move($dir, $filename1);

                //     $Uploads->files = $filename1;
                // }
                // $Uploads->files = $video->getVideoId();

                $path = $request->file('upload_videos')->store('videos', 's3');

            // return Storage::disk('public')->get('videos/Sys6d40ISKWO2SsGmy92uds6p04kTyLR6Ikr4jt7.mp4');
            // Storage::disk('google')->put('test.txt', 'Hello');
            Storage::disk('s3')->setVisibility($path, 'public');
                $Uploads->files = Storage::disk('s3')->url($path);

            }else if($request->video_type == 2){
                $Uploads->files = $request->video_link;
            }
        }


        // return "Video uploaded successfully. Video ID is ". $video->getVideoId();

        $Uploads->video_type = $request->video_type;

        $Uploads->status = 1;
        $AddUploads = $Uploads->save();

        // return redirect()->back()->with('message','File Uploaded Successfully');
        return redirect('teachers/uploads/add_students/'.$Uploads->id);
        // return response()->json(array(
        //     "error"=>FALSE,
        //     "message"=>"updated successfully"
        // ));
    }


    public function update_uploads(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $id = $request->id;

        $Uploads = Uploads::where('id', $id)->first();

        $Uploads->user_id = $UserId;
        $Uploads->file_type = $request->file_type;

        if($request->file_type == 1){
            $Uploads->file_name = $request->file_names;

            if($request->hasfile('upload_file')){
                $extension = $request->file('upload_file')->getClientOriginalExtension();
                $dir = 'UI/uploads/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('upload_file')->move($dir, $filename);

                $Uploads->files = $filename;
            }else{
                $Uploads->files= $Uploads->files;
            }


        }else if($request->file_type == 2){
            $Uploads->file_name = $request->file_name;


            // $video = Youtube::upload($request->file('upload_video')->getPathName(), [
            //     'title'       => $request->title,
            //     'description' => "test content"
            // ]);

            // Storage::disk('google')->allFiles($path);

            if($request->video_type == 1){


                // if($request->hasfile('upload_video')){
                //     $extension = $request->file('upload_video')->getClientOriginalExtension();
                //     $dir = 'UI/uploads/';
                //     $filename1 = uniqid() . '_' . time() . '.' . $extension;
                //     $request->file('upload_video')->move($dir, $filename1);

                //     $Uploads->files = $filename1;
                // }
                // $Uploads->files = $video->getVideoId();
                $path = $request->file('upload_videos')->store('videos', 's3');

            Storage::disk('s3')->setVisibility($path, 'public');

                $Uploads->files = Storage::disk('s3')->url($path);

            }else if($request->video_type == 2){

                $Uploads->files = $request->video_link;
            }
        }

        $Uploads->video_type = $request->video_type;

        // $AddUploads = $Uploads->save();

        return redirect()->back()->with('message','File Uploaded Successfully');
        // return redirect('teachers/uploads/list');

    }


    public function delete_uploads(Request $request)
    {
        $upload_id = $request->id;

        // echo $request->student_id;
        // exit;
        $Uploads = Uploads::where('id', $upload_id)->delete();

        $MediaGroupStudentsList = MediaGroupStudentsList::where('media_id', $upload_id)->delete();

        $MediaStudentsList = MediaStudentsList::where('media_id', $upload_id)->delete();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> "Media Removed Successfully"
                        )
        );
    }


    // Students

    public function add_students($id){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $title = "Add Students";
        $Uploads = Uploads::where('id', $id)->first();
        // $StudentsList = Users::where('user_type', 3)->orderBy('created_at', 'DESC')->get();
        $StudentsList = Users::where('user_type', 3)
                        ->select('users.*')
                        ->join('add_teachers_to_students', 'add_teachers_to_students.student_id', '=', 'users.id')
                        ->where('add_teachers_to_students.teacher_id', $UserId)
                        ->orderBy('users.created_at', 'DESC')->get();

        return view('UI.teachers.uploads.add_students', compact('title', 'StudentsList', 'Uploads'));
    }


    public function store_students_list(Request $request){

        $CourseId = $request->media_id;

        $MediaStudentsList = new MediaStudentsList();

        $MediaStudentsList->media_id = $CourseId;
        $MediaStudentsList->student_id = $request->student_id;

        $MediaStudentsList->save();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> " Student Added Successfully"
                        )
        );
    }

    public function delete_students_list(Request $request)
    {
        // \Log::info($request->all());

        $CourseId = $request->media_id;

        // echo $request->student_id;
        // exit;

        $MediaStudentsList = MediaStudentsList::where('media_id', $CourseId)
                        ->where('student_id', $request->student_id)->delete();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> " Student Removed Successfully"
                        )
        );
    }


    public function students_list($id){

        $title = "Students List";
        // $Courses = Courses::where('id', $id)->first();
        $GroupStudentsList = GroupStudentsList::join('users', 'users.id', 'group_students_list.student_id')
                                    ->where('group_students_list.group_id', $id)
                                    ->get();


        return view('UI.teachers.groups.students_list', compact('title', 'GroupStudentsList'));
    }


    public function add_groups($id){

        $title = "Add Groups";
        $Groups = Groups::get();
        $Uploads = Uploads::where('id', $id)->first();
        // $StudentsList = Users::where('user_type', 3)->orderBy('created_at', 'DESC')->get();

        return view('UI.teachers.uploads.add_groups', compact('title', 'Groups', 'Uploads'));
    }


    public function update_group_of_students(Request $request){
        $group_id = $request->group_id;
        $media_id = $request->media_id;

        MediaGroupStudentsList::where('media_id', $media_id)
                                ->delete();

        $CheckMediaGroupStudentsList = MediaGroupStudentsList::where('group_id', $group_id)
                                                            ->where('media_id', $media_id)
                                                            ->first();

        if($CheckMediaGroupStudentsList != null){
            $CheckMediaGroupStudentsList->group_id = $group_id;
            $CheckMediaGroupStudentsList->media_id = $media_id;

            $CheckMediaGroupStudentsList->save();
        }else{
            $MediaGroupStudentsList = new MediaGroupStudentsList();
            $MediaGroupStudentsList->group_id = $group_id;
            $MediaGroupStudentsList->media_id = $media_id;

            $MediaGroupStudentsList->save();
        }


        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> " Student Group Added Successfully"
                        )
        );
    }
}
