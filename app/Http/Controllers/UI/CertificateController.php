<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Certificates;
use Illuminate\Support\Facades\Auth;
use Session;
use PDF;

class CertificateController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('guest:super_admin')->except('dashboard');
        if(Auth::guard('super_admin')->check()){
            $this->middleware('guest:super_admin');
        }elseif(Auth::guard('teacher')->check()){
            $this->middleware('guest:teacher');
        }
    }

    public function certificates_list(){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
            $title = "Certificates List";
            $Certificates = Certificates::orderBy('created_at', 'DESC')->get();
        }else{
            $UserId = Session::get('TeacherId');
            $title = "Certificates List";
        $Certificates = Certificates::where('teacher_id', $UserId)
            ->select('certificate.*', 'users.first_name as Fname', 'users.last_name as Lname')
            ->join('users', 'users.id', '=', 'certificate.teacher_id')
        ->orderBy('created_at', 'DESC')->get();
        }

        return view('UI.teachers.certificates.certificates_list', compact('title', 'Certificates'));
    }

    public function add_certificates(){
        $title = "Add certificates";
        return view('UI.teachers.certificates.add_certificates', compact('title'));
    }

    public function view_certificate($id){

        $GetCertificates = Certificates::where('id', $id)->first();

        if($GetCertificates->certificate_id == 1){

            return view('UI.teachers.certificates.certificate1', compact('GetCertificates'));
        }elseif($GetCertificates->certificate_id == 2){

            return view('UI.teachers.certificates.certificate2', compact('GetCertificates'));
        }
    }

    public function store_certificates(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }
        $length = 2;
        $str = "";
        $characters = array_merge(range('A','Z'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }

        $request->session()->put('CertificateNo', $str.rand(10000,100000));

        switch ($request->action) {
            case 'save_preview':


                $CheckCertificates = Certificates::where('first_name', $request->first_name)->where('last_name', $request->last_name)->first();

                if($CheckCertificates){
                    if($request->certificate_id == 1){
                    $CheckCertificates->teacher_id = $UserId;
                    $CheckCertificates->title = $request->title;
                    $CheckCertificates->award = $request->award;
                    $CheckCertificates->first_name = $request->first_name;
                    $CheckCertificates->last_name = $request->last_name;
                    $CheckCertificates->coaching_partner_name = $request->coaching_partner_name;
                    $CheckCertificates->description = $request->description;

                    if($request->hasfile('partner_logo')){
                        $extension = $request->file('partner_logo')->getClientOriginalExtension();
                        $dir = 'UI/partner_logo/';
                        $filename = uniqid() . '_' . time() . '.' . $extension;
                        $request->file('partner_logo')->move($dir, $filename);

                        $CheckCertificates->partner_logo = $filename;
                    }
                    $CheckCertificates->certificate_id = $request->certificate_id;
                    $CheckCertificates->status = 0;
                    $AddCertificates = $CheckCertificates->save();

                    $GetCertificates = Certificates::where('first_name', Session::get('CertificateFname'))->where('last_name', Session::get('CertificateLname'))->first();

                    return view('UI.teachers.certificates.certificate1', compact('GetCertificates'));
                    }elseif($request->certificate_id == 2){
                        $CheckCertificates->teacher_id = $UserId;
                    $CheckCertificates->title = $request->title;
                    $CheckCertificates->award = $request->award;
                    $CheckCertificates->first_name = $request->first_name;
                    $CheckCertificates->last_name = $request->last_name;
                    $CheckCertificates->coaching_partner_name = $request->coaching_partner_name;
                    $CheckCertificates->description = $request->description;

                    if($request->hasfile('partner_logo')){
                        $extension = $request->file('partner_logo')->getClientOriginalExtension();
                        $dir = 'UI/partner_logo/';
                        $filename = uniqid() . '_' . time() . '.' . $extension;
                        $request->file('partner_logo')->move($dir, $filename);

                        $CheckCertificates->partner_logo = $filename;
                    }
                    $CheckCertificates->certificate_id = $request->certificate_id;
                    $CheckCertificates->status = 0;
                    $AddCertificates = $CheckCertificates->save();

                    $GetCertificates = Certificates::where('first_name', Session::get('CertificateFname'))->where('last_name', Session::get('CertificateLname'))->first();

                    return view('UI.teachers.certificates.certificate2', compact('GetCertificates'));
                    }
                }else{
                    if($request->certificate_id == 1){
                    $Certificates = new Certificates();

                    $Certificates->teacher_id = $UserId;
                    $Certificates->title = $request->title;
                    $Certificates->award = $request->award;
                    $Certificates->first_name = $request->first_name;
                    $Certificates->last_name = $request->last_name;
                    $Certificates->coaching_partner_name = $request->coaching_partner_name;
                    $Certificates->description = $request->description;

                    if($request->hasfile('partner_logo')){
                        $extension = $request->file('partner_logo')->getClientOriginalExtension();
                        $dir = 'UI/partner_logo/';
                        $filename = uniqid() . '_' . time() . '.' . $extension;
                        $request->file('partner_logo')->move($dir, $filename);

                        $Certificates->partner_logo = $filename;
                    }
                    $Certificates->certificate_id = $request->certificate_id;
                    $Certificates->certificate_no = Session::get('CertificateNo');
                    $Certificates->status = 0;
                    $AddCertificates = $Certificates->save();

                    $request->session()->put('CertificateFname', $request->first_name);
                    $request->session()->put('CertificateLname', $request->last_name);

                    $GetCertificates = Certificates::where('first_name', Session::get('CertificateFname'))->where('last_name', Session::get('CertificateLname'))->first();

                    return view('UI.teachers.certificates.certificate1', compact('GetCertificates'));
                    }elseif($request->certificate_id == 2){
                        $Certificates = new Certificates();

                        $Certificates->teacher_id = $UserId;
                        $Certificates->title = $request->title;
                        $Certificates->award = $request->award;
                        $Certificates->first_name = $request->first_name;
                        $Certificates->last_name = $request->last_name;
                        $Certificates->coaching_partner_name = $request->coaching_partner_name;
                        $Certificates->description = $request->description;

                        if($request->hasfile('partner_logo')){
                            $extension = $request->file('partner_logo')->getClientOriginalExtension();
                            $dir = 'UI/partner_logo/';
                            $filename = uniqid() . '_' . time() . '.' . $extension;
                            $request->file('partner_logo')->move($dir, $filename);

                            $Certificates->partner_logo = $filename;
                        }
                        $Certificates->certificate_id = $request->certificate_id;
                        $Certificates->certificate_no = Session::get('CertificateNo');
                        $Certificates->status = 0;
                        $AddCertificates = $Certificates->save();

                        $request->session()->put('CertificateFname', $request->first_name);
                    $request->session()->put('CertificateLname', $request->last_name);

                    $GetCertificates = Certificates::where('first_name', Session::get('CertificateFname'))->where('last_name', Session::get('CertificateLname'))->first();

                    return view('UI.teachers.certificates.certificate2', compact('GetCertificates'));
                    }
                }


                break;

            case 'confirm':
                $CheckCertificates = Certificates::where('first_name', $request->first_name)->where('last_name', $request->last_name)->first();

                if($CheckCertificates){
                    if($request->certificate_id == 1){
                    $CheckCertificates->teacher_id = $UserId;
                    $CheckCertificates->title = $request->title;
                    $CheckCertificates->award = $request->award;
                    $CheckCertificates->first_name = $request->first_name;
                    $CheckCertificates->last_name = $request->last_name;
                    $CheckCertificates->description = $request->description;

                    if($request->hasfile('partner_logo')){
                        $extension = $request->file('partner_logo')->getClientOriginalExtension();
                        $dir = 'UI/partner_logo/';
                        $filename = uniqid() . '_' . time() . '.' . $extension;
                        $request->file('partner_logo')->move($dir, $filename);

                        $CheckCertificates->partner_logo = $filename;
                    }
                    $CheckCertificates->certificate_id = $request->certificate_id;
                    $CheckCertificates->status = 1;
                    $AddCertificates = $CheckCertificates->save();

                    $GetCertificates = Certificates::where('first_name', Session::get('CertificateFname'))->where('last_name', Session::get('CertificateLname'))->first();

                    return view('UI.teachers.certificates.certificate1', compact('GetCertificates'));
                    }elseif($request->certificate_id == 2){
                        $CheckCertificates->teacher_id = $UserId;
                    $CheckCertificates->title = $request->title;
                    $CheckCertificates->award = $request->award;
                    $CheckCertificates->first_name = $request->first_name;
                    $CheckCertificates->last_name = $request->last_name;
                    $CheckCertificates->description = $request->description;

                    if($request->hasfile('partner_logo')){
                        $extension = $request->file('partner_logo')->getClientOriginalExtension();
                        $dir = 'UI/partner_logo/';
                        $filename = uniqid() . '_' . time() . '.' . $extension;
                        $request->file('partner_logo')->move($dir, $filename);

                        $CheckCertificates->partner_logo = $filename;
                    }
                    $CheckCertificates->certificate_id = $request->certificate_id;
                    $CheckCertificates->status = 1;
                    $AddCertificates = $CheckCertificates->save();

                    $GetCertificates = Certificates::where('first_name', Session::get('CertificateFname'))->where('last_name', Session::get('CertificateLname'))->first();

                    return view('UI.teachers.certificates.certificate2', compact('GetCertificates'));
                    }
                }else{
                    if($request->certificate_id == 1){
                    $Certificates = new Certificates();

                    $Certificates->teacher_id = $UserId;
                    $Certificates->title = $request->title;
                    $Certificates->award = $request->award;
                    $Certificates->first_name = $request->first_name;
                    $Certificates->last_name = $request->last_name;
                    $Certificates->description = $request->description;

                    if($request->hasfile('partner_logo')){
                        $extension = $request->file('partner_logo')->getClientOriginalExtension();
                        $dir = 'UI/partner_logo/';
                        $filename = uniqid() . '_' . time() . '.' . $extension;
                        $request->file('partner_logo')->move($dir, $filename);

                        $Certificates->partner_logo = $filename;
                    }
                    $Certificates->certificate_id = $request->certificate_id;
                    $Certificates->certificate_no = Session::get('CertificateNo');
                    $Certificates->status = 1;
                    $AddCertificates = $Certificates->save();

                    $request->session()->put('CertificateFname', $request->first_name);
                    $request->session()->put('CertificateLname', $request->last_name);

                    $GetCertificates = Certificates::where('first_name', Session::get('CertificateFname'))->where('last_name', Session::get('CertificateLname'))->first();

                    return view('UI.teachers.certificates.certificate1', compact('GetCertificates'));
                    }elseif($request->certificate_id == 2){
                        $Certificates = new Certificates();

                        $Certificates->teacher_id = $UserId;
                        $Certificates->title = $request->title;
                        $Certificates->award = $request->award;
                        $Certificates->first_name = $request->first_name;
                        $Certificates->last_name = $request->last_name;
                        $Certificates->description = $request->description;

                        if($request->hasfile('partner_logo')){
                            $extension = $request->file('partner_logo')->getClientOriginalExtension();
                            $dir = 'UI/partner_logo/';
                            $filename = uniqid() . '_' . time() . '.' . $extension;
                            $request->file('partner_logo')->move($dir, $filename);

                            $Certificates->partner_logo = $filename;
                        }
                        $Certificates->certificate_id = $request->certificate_id;
                        $Certificates->certificate_no = Session::get('CertificateNo');
                        $Certificates->status = 1;
                        $AddCertificates = $Certificates->save();

                        $request->session()->put('CertificateFname', $request->first_name);
                    $request->session()->put('CertificateLname', $request->last_name);

                    $GetCertificates = Certificates::where('first_name', Session::get('CertificateFname'))->where('last_name', Session::get('CertificateLname'))->first();

                    return view('UI.teachers.certificates.certificate2', compact('GetCertificates'));
                    }
                }
            break;
        }



        // return redirect()->back()->with('message','Certificates Added Successfully');
    }
}
