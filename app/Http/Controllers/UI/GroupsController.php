<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Groups;
use App\Models\UI\Users;
use App\Models\UI\GroupStudentsList;

use Illuminate\Support\Facades\Auth;
use Session;

class GroupsController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('guest:super_admin')->except('dashboard');
        if(Auth::guard('super_admin')->check()){
            $this->middleware('guest:super_admin');
        }
    }

    public function groups_list(){
        $title = "Groups List";
        if(Auth::guard('super_admin')->check()){
            $Groups = Groups::orderBy('created_at', 'DESC')->get();
        }else{
            $UserId = Session::get('TeacherId');

            $Groups = Groups::orderBy('created_at', 'DESC')->where('teacher_id', $UserId)->get();
        }

        return view('UI.teachers.groups.group_list', compact('title', 'Groups'));
    }


    public function add_group(){
        $title = "Add Group";
        return view('UI.teachers.groups.add_group', compact('title'));
    }

    public function edit_group($id){
        $title = "Edit Group";
        $Groups = Groups::where('id', $id)->first();
        return view('UI.teachers.groups.edit_group', compact('title', 'Groups'));
    }

    public function store_groups(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $Groups = new Groups();

        $Groups->title = $request->title;
        $Groups->teacher_id = $UserId;
        $Groups->description = $request->description;
        $Groups->status = 1;

        $AddGroups = $Groups->save();

        // return redirect()->back()->with('message','Groups Added Successfully');
        return redirect('groups/add_students/'.$Groups->id);
    }


    public function update_groups(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $Id = $request->id;

        $Groups = Groups::where('id', $Id)->first();

        $Groups->title = $request->title;
        $Groups->teacher_id = $UserId;
        $Groups->description = $request->description;

        $AddGroups = $Groups->save();

        return redirect()->back()->with('message','Groups Updated Successfully');
    }


    public function delete_groups(Request $request)
    {
        $group_id = $request->id;

        // echo $request->student_id;
        // exit;
        $groups = Groups::where('id', $group_id)->delete();

        $GroupStudentsList = GroupStudentsList::where('group_id', $group_id)->delete();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> " Groups Removed Successfully"
                        )
        );
    }

// Students

    public function add_students($id){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $title = "Add Students";
        $Groups = Groups::where('id', $id)->first();
        // $StudentsList = Users::where('user_type', 3)->orderBy('created_at', 'DESC')->get();
        $StudentsList = Users::where('user_type', 3)
                        ->select('users.*')
                        ->join('add_teachers_to_students', 'add_teachers_to_students.student_id', '=', 'users.id')
                        ->where('add_teachers_to_students.teacher_id', $UserId)
                        ->orderBy('users.created_at', 'DESC')->get();

        return view('UI.teachers.groups.add_students', compact('title', 'StudentsList', 'Groups'));
    }


    public function store_students_list(Request $request){

        $CourseId = $request->group_id;

        $GroupStudentsList = new GroupStudentsList();

        $GroupStudentsList->group_id = $CourseId;
        $GroupStudentsList->student_id = $request->student_id;

        $GroupStudentsList->save();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> " Student Added Successfully"
                        )
        );
    }

    public function delete_students_list(Request $request)
    {
    	// \Log::info($request->all());

        $CourseId = $request->group_id;

        // echo $request->student_id;
        // exit;

        $GroupStudentsList = GroupStudentsList::where('group_id', $CourseId)
                        ->where('student_id', $request->student_id)->delete();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> " Student Removed Successfully"
                        )
        );
    }


    public function students_list($id){

        $title = "Students List";
        // $Courses = Courses::where('id', $id)->first();
        $GroupStudentsList = GroupStudentsList::join('users', 'users.id', 'group_students_list.student_id')
                                    ->join('students_profile', 'users.id', 'students_profile.student_id')
                                    ->select('users.*', 'students_profile.gender')
                                    ->where('group_students_list.group_id', $id)
                                    ->get();


        return view('UI.teachers.groups.students_list', compact('title', 'GroupStudentsList'));
    }
}
