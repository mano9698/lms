<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\FeeCollection;
use App\Models\UI\Users;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class FeeController extends Controller
{
    public function fees_list(){
        $title = "Fees List";

        if(Auth::guard('super_admin')->check()){
            $FeeCollection = FeeCollection::select('users.first_name', 'users.last_name', 'fees_collection.*')->join('users', 'users.id', '=', 'fees_collection.student_id')->get();

        }elseif(Auth::guard('teacher')->check()){
            $UserId = Session::get('TeacherId');

            $FeeCollection = FeeCollection::select('users.first_name', 'users.last_name', 'fees_collection.*')->join('users', 'users.id', '=', 'fees_collection.student_id')->where('fees_collection.teacher_id', $UserId)->get();
        }



        return view('UI.admin.fees.list', compact('title', 'FeeCollection'));
    }

    public function student_home_work_list(){
        $title = "Home Work";
        $UserId = Session::get('TeacherId');

        $HomeWork = HomeWork::where('student_id', $UserId)->get();

        return view('UI.students.home_work.list', compact('title', 'HomeWork'));
    }

    public function add_fees(){
        $title = "Add Fees";

        $Users = Users::where('user_main_type', 2)->get();

        return view('UI.admin.fees.add_fees', compact('title', 'Users'));
    }

    public function edit_fees($id){
        $title = "Edit Fees";

        $FeeCollection = FeeCollection::where('id', $id)->first();

        $Users = Users::where('user_main_type', 2)->get();

        return view('UI.admin.fees.edit_fees', compact('title', 'Users', 'FeeCollection'));
    }

    public function store_fees(Request $request){

        if(Auth::guard('super_admin')){
            $UserId = Session::get('AdminId');
        }elseif(Auth::guard('teacher')){
            $UserId = Session::get('TeacherId');
        }

        $FeeCollection = new FeeCollection();

        $FeeCollection->teacher_id = $UserId;
        $FeeCollection->student_id = $request->student_id;
        $FeeCollection->amount_received = $request->amount_received;
        $FeeCollection->date_of_receipt = $request->date_of_receipt;
        $FeeCollection->mode_of_receipt = $request->mode_of_receipt;
        $FeeCollection->received_towards = $request->received_towards;

        $FeeCollection->save();

        return redirect()->back()->with('message','Fee Collection Added Successfully');
    }


    public function update_fees(Request $request){

        $id = $request->id;
        $FeeCollection = FeeCollection::where('id', $id)->first();

        $FeeCollection->student_id = $request->student_id;
        $FeeCollection->amount_received = $request->amount_received;
        $FeeCollection->date_of_receipt = $request->date_of_receipt;
        $FeeCollection->mode_of_receipt = $request->mode_of_receipt;
        $FeeCollection->received_towards = $request->received_towards;

        $FeeCollection->save();

        return redirect()->back()->with('message','Fee Collection Updated Successfully');
    }

    public function delete_fees($id)
{


    $FeeCollection = FeeCollection::where('id', $id)->delete();



    return redirect()->back()->with('message','Fee Collection Deleted Successfully');
}
}
