<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Users;

use App\Models\UI\Testimonials;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class TestimonialController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('guest:super_admin')->except('dashboard');
        if(Auth::guard('super_admin')->check()){
            $this->middleware('guest:super_admin');
        }elseif(Auth::guard('teacher')->check()){
            $this->middleware('guest:teacher');
        }
    }

    public function testimonials_list(){
        $title = "Testimonials List";
        $Testimonials = Testimonials::select('testimonials.*', 'users.first_name', 'users.last_name')
                        ->join('users', 'testimonials.teacher_id', 'users.id')
                        ->orderBy('testimonials.created_at', 'DESC')->get();
        return view('UI.admin.testimonials.testimonials_list', compact('title', 'Testimonials'));
    }


    public function add_testimonials(){
        $title = "Add Testimonials";
        $Teachers = Users::where('user_main_type', 1)->get();
        $Students = Users::where('user_main_type', 2)->get();
        return view('UI.admin.testimonials.add_testimonials', compact('title', 'Teachers', 'Students'));
    }

    public function edit_testimonials($id){
        $title = "Edit Testimonials";
        $Testimonials = Testimonials::where('id', $id)->first();
        $Teachers = Users::where('user_main_type', 1)->get();
        $Students = Users::where('user_main_type', 2)->get();
        return view('UI.admin.testimonials.edit_testimonials', compact('title', 'Testimonials', 'Teachers', 'Students'));
    }

    public function store_testimonials(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $Testimonials = new Testimonials();

        $Testimonials->teacher_id = $request->teacher_id;
        $Testimonials->student_id = $request->student_id;
        $Testimonials->testimonial = $request->testimonial;
        $Testimonials->status = 1;

        $AddTestimonials = $Testimonials->save();

        return redirect()->back()->with('message','Testimonials Added Successfully');
    }


    public function update_testimonials(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $Id = $request->id;

        $Testimonials = Testimonials::where('id', $Id)->first();

        $Testimonials->teacher_id = $request->teacher_id;
        $Testimonials->student_id = $request->student_id;
        $Testimonials->testimonial = $request->testimonial;
        $Testimonials->status = 1;

        $AddTestimonials = $Testimonials->save();

        return redirect()->back()->with('message','Testimonials Updated Successfully');
    }


    public function delete_testimonials($id)
{

    $Testimonials = Testimonials::where('id', $id)->delete();

    return redirect()->back()->with('message','Testimonials Deleted Successfully');
}

}
