<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\Questions;
use App\Models\UI\Users;
use App\Models\UI\Comments;

use Illuminate\Support\Facades\Auth;
use Session;
use Illuminate\Support\Str;

class ForumController extends Controller
{
    public function forum_list(){
        $title = "Forum List";

        if(Auth::guard('super_admin')->check()){
            $Questions = Questions::get();

        }else{
            $UserId = Session::get('TeacherId');

            $Questions = Questions::where('teacher_id', $UserId)->get();
        }


        return view('UI.teachers.forum.forum_list', compact('title', 'Questions'));
    }

    public function add_forum(){
        $title = "Add Forum";

        return view('UI.teachers.forum.add_question', compact('title'));
    }

    public function edit_forum($id){
        $title = "Edit Forum";

        $Questions = Questions::where('id', $id)->first();

        return view('UI.teachers.forum.edit_question', compact('title', 'Questions'));
    }

    public function delete_forum($id){
        $Questions = Questions::where('id', $id)->delete();

        return redirect()->back()->with('message','Question Deleted Successfully');
    }

    public function comments_list($id){
        $title = "Comments List";

        $Comments = Comments::where('question_id', $id)->get();

        return view('UI.teachers.forum.comments_list', compact('title', 'Comments'));
    }

    public function delete_comments($id){
        $title = "Comments List";

        $Comments = Comments::where('id', $id)->delete();

        return redirect()->back()->with('message','Comments Deleted Successfully');
    }


    public function store_questions(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $Questions = new Questions();

        $Questions->teacher_id = $UserId;
        $Questions->topic = $request->topic;
        $Questions->slug = Str::slug($request->topic);
        $Questions->subject = $request->subject;
        $Questions->tags = $request->tags;
        $Questions->details_answer = $request->details_answer;

        if($request->hasfile('feature_img')){
            $extension = $request->file('feature_img')->getClientOriginalExtension();
            $dir = 'UI/questions/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('feature_img')->move($dir, $filename);

            $Questions->feature_img = $filename;
        }

        $Questions->status = 1;

        $AddQuestions = $Questions->save();

        return redirect()->back()->with('message','Questions Added Successfully');
    }


    public function update_questions(Request $request){
        if(Auth::guard('super_admin')->check()){
            $UserId = Session::get('AdminId');
        }else{
            $UserId = Session::get('TeacherId');
        }

        $id = $request->id;

        $Questions = Questions::where('id', $id)->first();

        $Questions->teacher_id = $UserId;
        $Questions->topic = $request->topic;
        $Questions->slug = Str::slug($request->topic);
        $Questions->subject = $request->subject;
        $Questions->tags = $request->tags;
        $Questions->details_answer = $request->details_answer;

        if($request->hasfile('feature_img')){
            $extension = $request->file('feature_img')->getClientOriginalExtension();
            $dir = 'UI/questions/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('feature_img')->move($dir, $filename);

            $Questions->feature_img = $filename;
        }else{
            $Questions->feature_img = $Questions->feature_img;
        }


        $AddQuestions = $Questions->save();

        return redirect()->back()->with('message','Questions Updated Successfully');
    }
}
