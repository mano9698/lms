<?php

namespace App\Http\Controllers\UI\Students;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Courses;
use App\Models\UI\StudentsList;
use Session;
use App\Models\UI\Uploads;
use App\Models\UI\Conference;

use App\Models\UI\StudentsProfile;
use App\Models\UI\GroupStudentsList;
use App\Models\UI\TotalFileDownloads;
use App\Models\UI\TotalVideoViews;
use App\Models\UI\Groups;

class StudentsController extends Controller
{
    // public function __construct(){
    //     // $this->middleware('auth');
    //     // $this->middleware('guest:super_admin')->except('dashboard');
    //     $this->middleware('guest:student');

    // }

    public function dashboard(){
        $title = "Students Dashboard";
        $UserId = Session::get('TeacherId');
        $Courses = Courses::join('students_list', 'students_list.course_id', '=', 'courses.id')
                            ->join('users', 'courses.teacher_id', '=', 'users.id')
                            ->where('students_list.student_id', $UserId)
                            ->get();
        $GroupStudentsList = GroupStudentsList::where('student_id', $UserId)
                                                ->count();

        $Videos = Uploads::join('media_students_list', 'media_students_list.media_id', '=', 'uploads.id')
        ->select('uploads.*')
        ->where('media_students_list.student_id', $UserId)
        ->where('uploads.file_type', 2)
        ->count();

        $Files = Uploads::join('media_students_list', 'media_students_list.media_id', '=', 'uploads.id')
                            ->select('uploads.*')
                            ->where('media_students_list.student_id', $UserId)
                            ->where('uploads.file_type', 1)
                            ->count();

        $Conference = Conference::join('conference_students_list', 'conference_students_list.conference_id', '=', 'conference.id')
                            ->where('conference_students_list.student_id', $UserId)
                            ->count();
        return view('UI.students.dashboard', compact('title', 'Courses', 'GroupStudentsList', 'Videos', 'Files', 'Conference'));
    }

    public function edit_profile(){
        $title = "Edit Profile";
        $UserId = Session::get('TeacherId');

        $StudentsProfile = StudentsProfile::where('student_id', $UserId)->first();

        // echo json_encode($StudentsProfile->profile_pic);
        // exit;

        return view('UI.students.profile.edit_profile', compact('title', 'StudentsProfile'));
    }

    public function view_videos($id){
        $title = "View Videos";
        $UserId = Session::get('TeacherId');

        $CheckTotalVideoViews = TotalVideoViews::where('user_id', $UserId)
                                                ->where('video_id', $id)
                                                ->first();

            // echo $UserId;
            // exit;

            $TotalVideoViews = new TotalVideoViews();

            $TotalVideoViews->user_id = $UserId;
            $TotalVideoViews->video_id = $id;

            $TotalVideoViews->save();
        // if($CheckTotalVideoViews){

        //     $CheckTotalVideoViews->user_id = $UserId;
        //     $CheckTotalVideoViews->video_id = $CheckTotalVideoViews->video_id;
        //     $CheckTotalVideoViews->save();

        // }else{

        //     $TotalVideoViews = new TotalVideoViews();

        //     $TotalVideoViews->user_id = $UserId;
        //     $TotalVideoViews->video_id = $id;

        //     $TotalVideoViews->save();
        // }

        $Uploads = Uploads::where('uploads.id', $id)
                    ->select('uploads.*', 'users.first_name', 'users.last_name')
                    ->join('users', 'users.id', '=', 'uploads.user_id')
                    ->first();

        // echo json_encode($Uploads);
        // exit;

        return view('UI.students.uploads.view_videos', compact('title', 'Uploads'));
    }

    public function groups_list(){
        $title = "Groups List";
        $UserId = Session::get('TeacherId');
        $Groups = Groups::join('group_students_list', 'group_students_list.group_id', '=', 'groups.id')
                    ->select('groups.*')
                    ->where('group_students_list.student_id', $UserId)
                    ->orderBy('groups.created_at', 'DESC')->get();

        return view('UI.students.groups.groups_list', compact('title', 'Groups'));
    }

    public function students_list($id){

        $title = "Students List";
        $Groups = Groups::join('users', 'users.id', 'groups.teacher_id')
                            ->where('groups.id', $id)->first();
        $GroupStudentsList = GroupStudentsList::join('users', 'users.id', 'group_students_list.student_id')
                                    ->where('group_students_list.group_id', $id)
                                    ->get();

        return view('UI.students.groups.view_students', compact('title', 'GroupStudentsList', 'Groups'));
    }

    public function course_list(){
        $title = "Course List";
        $UserId = Session::get('TeacherId');

        // echo $UserId;
        // exit;

        $Courses = Courses::join('students_list', 'students_list.course_id', '=', 'courses.id')
                            ->join('users', 'courses.teacher_id', '=', 'users.id')
                            ->where('students_list.student_id', $UserId)
                            ->get();

        return view('UI.students.courses.my_courses_list', compact('title', 'Courses'));
    }


    public function videos_list(){
        $title = "Videos List";
        $UserId = Session::get('TeacherId');

        // echo $UserId;
        // exit;

        $Videos = Uploads::join('media_students_list', 'media_students_list.media_id', '=', 'uploads.id')
                            ->select('uploads.*')
                            ->where('media_students_list.student_id', $UserId)
                            ->where('uploads.file_type', 2)
                            ->get();

        return view('UI.students.uploads.videos', compact('title', 'Videos'));
    }

    public function files_list(){
        $title = "Files List";
        $UserId = Session::get('TeacherId');

        // echo $UserId;
        // exit;

        $Videos = Uploads::join('media_students_list', 'media_students_list.media_id', '=', 'uploads.id')
                            ->select('uploads.*')
                            ->where('media_students_list.student_id', $UserId)
                            ->where('uploads.file_type', 1)
                            ->get();

        return view('UI.students.uploads.videos', compact('title', 'Videos'));
    }


    public function conference_list(){
        $title = "Conference List";
        $UserId = Session::get('TeacherId');

        // echo $UserId;
        // exit;

        $Conference = Conference::join('conference_students_list', 'conference_students_list.conference_id', '=', 'conference.id')
                            ->where('conference_students_list.student_id', $UserId)
                            ->get();

        return view('UI.students.conference.conference_list', compact('title', 'Conference'));
    }


    public function update_student_profile(Request $request){
        $UserId = Session::get('TeacherId');

        // echo json_encode($request->info);
        // exit;

        $CheckStudentsProfile = StudentsProfile::where('student_id' ,$UserId)->first();

        if($CheckStudentsProfile != null){
            $CheckStudentsProfile->student_id = $UserId;

            if($request->hasfile('profile_pic')){
                $extension = $request->file('profile_pic')->getClientOriginalExtension();
                $dir = 'UI/students_profile_pic/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('profile_pic')->move($dir, $filename);

                $CheckStudentsProfile->profile_pic = $filename;
            }else{
                $CheckStudentsProfile->profile_pic = $CheckStudentsProfile->profile_pic;
            }

            $CheckStudentsProfile->gender = $request->gender;
            $CheckStudentsProfile->dob = $request->dob;

            $CheckStudentsProfile->age = $request->age;
            $CheckStudentsProfile->grade = $request->grade;
            $CheckStudentsProfile->school_clg_name = $request->school_clg_name;
            $CheckStudentsProfile->school_board = $request->school_board;
            $CheckStudentsProfile->language_known = $request->language_known;
            $CheckStudentsProfile->fav_subject = $request->fav_subject;
            $CheckStudentsProfile->fav_sport = $request->fav_sport;
            $CheckStudentsProfile->fav_music = $request->fav_music;

            $AddCheckStudentsProfile = $CheckStudentsProfile->save();

            return redirect()->back()->with('message','Student Profile Updated Successfully');

        }else{

            $StudentsProfile = new StudentsProfile();

            $StudentsProfile->student_id = $UserId;

            if($request->hasfile('profile_pic')){
                $extension = $request->file('profile_pic')->getClientOriginalExtension();
                $dir = 'UI/students_profile_pic/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('profile_pic')->move($dir, $filename);

                $StudentsProfile->profile_pic = $filename;
            }

            $StudentsProfile->gender = $request->gender;
            $StudentsProfile->dob = $request->dob;

            $StudentsProfile->age = $request->age;
            $StudentsProfile->grade = $request->grade;
            $StudentsProfile->school_clg_name = $request->school_clg_name;
            $StudentsProfile->school_board = $request->school_board;
            $StudentsProfile->language_known = $request->language_known;
            $StudentsProfile->fav_subject = $request->fav_subject;
            $StudentsProfile->fav_sport = $request->fav_sport;
            $StudentsProfile->fav_music = $request->fav_music;

            $AddStudentsProfile = $StudentsProfile->save();



            return redirect()->back()->with('message','Student Profile Updated Successfully');
        }
    }


    public function file_download(Request $request){

        $UserId = Session::get('TeacherId');

        $CheckTotalFileDownloads = TotalFileDownloads::where('user_id', $UserId)
                                                        ->where('file_id', $request->file_id)
                                                        ->first();


        if($CheckTotalFileDownloads){

            $CheckTotalFileDownloads->user_id = $UserId;
            $CheckTotalFileDownloads->file_id = $request->file_id;

            $CheckTotalFileDownloads->save();

        }else{
            $TotalFileDownloads = new TotalFileDownloads();

            $TotalFileDownloads->user_id = $UserId;
            $TotalFileDownloads->file_id = $request->file_id;

            $TotalFileDownloads->save();
        }


        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> "Dowloads Count Added Successfully"
                        )
        );
    }
}
