<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\UI\HomeWork;
use App\Models\UI\Users;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use Session;

class HomeWorkController extends Controller
{
    public function home_work_list(){
        $title = "Home Work";
        if(Auth::guard('super_admin')->check()){
            $HomeWork = HomeWork::select('users.first_name', 'users.last_name', 'home_work.*')
            ->join('users', 'users.id', '=', 'home_work.teacher_id')->get();

            return view('UI.students.home_work.list', compact('title', 'HomeWork'));

        }elseif(Auth::guard('teacher')->check()){
            $UserId = Session::get('TeacherId');

            $HomeWork = HomeWork::select('users.first_name', 'users.last_name', 'home_work.*')
            ->join('users', 'users.id', '=', 'home_work.student_id')->where('teacher_id', $UserId)->get();

            $Users = Users::where('id', $UserId)->where('user_main_type', 1)->get();

        return view('UI.students.home_work.list', compact('title', 'HomeWork', 'Users'));
        }


    }

    public function student_home_work_list(){
        $title = "Home Work";
        $UserId = Session::get('TeacherId');

        $HomeWork = HomeWork::select('users.first_name', 'users.last_name', 'home_work.*')
                            ->join('users', 'users.id', '=', 'home_work.teacher_id')
                            ->where('home_work.student_id', $UserId)
                            ->get();

        return view('UI.students.home_work.list', compact('title', 'HomeWork'));
    }

    public function add_home_work(){
        $title = "Add Home Work";

        $Users = Users::where('user_main_type', 1)->get();

        return view('UI.students.home_work.add_work', compact('title', 'Users'));
    }

    public function edit_home_work($id){
        $title = "Edit Home work";

        $HomeWork = HomeWork::where('id', $id)->first();

        $Users = Users::where('user_main_type', 1)->get();

        return view('UI.students.home_work.edit_work', compact('title', 'Users', 'HomeWork'));
    }

    public function store_home_work(Request $request){

        // if(Auth::guard('super_admin')){
        //     $UserId = Session::get('AdminId');
        // }elseif(Auth::guard('teacher')){
        //     $UserId = Session::get('TeacherId');
        // }

        $UserId = Session::get('TeacherId');

        $HomeWork = new HomeWork();

        $HomeWork->teacher_id = $request->teacher_id;
        $HomeWork->student_id = $UserId;
        $HomeWork->file_name = $request->file_name;

        if($request->hasfile('file')){
            $extension = $request->file('file')->getClientOriginalExtension();
            $dir = 'UI/home_work/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file')->move($dir, $filename);

            $HomeWork->file = $filename;
        }

        $HomeWork->save();

        return redirect()->back()->with('message','HomeWork Added Successfully');
    }


    public function update_home_work(Request $request){

        $id = $request->id;
        $HomeWork = HomeWork::where('id', $id)->first();

        $HomeWork->teacher_id = $request->teacher_id;
        $HomeWork->student_id = $request->student_id;
        $HomeWork->file_name = $request->file_name;

        if($request->hasfile('file')){
            $extension = $request->file('file')->getClientOriginalExtension();
            $dir = 'UI/home_work/';
            $filename = uniqid() . '_' . time() . '.' . $extension;
            $request->file('file')->move($dir, $filename);

            $HomeWork->file = $filename;
        }else{
            $HomeWork->file = $HomeWork->file;
        }

        $HomeWork->save();

        return redirect()->back()->with('message','HomeWork Updated Successfully');
    }

    public function delete_home_work($id)
{


    $HomeWork = HomeWork::where('id', $id)->delete();



    return redirect()->back()->with('message','HomeWork Deleted Successfully');
}
}
