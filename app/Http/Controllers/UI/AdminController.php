<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UI\Courses;
use App\Models\UI\Groups;
use App\Models\UI\Users;
use App\Models\UI\Partners;

use App\Models\UI\Inquiry;
use App\Models\UI\Uploads;
use App\Models\UI\Classes;
use App\Models\UI\Conference;
use App\Models\UI\RequestCallBack;

use App\Models\UI\PaymentCollection;
use App\Models\UI\PaymentTransferred;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Models\UI\AddTeachersToStudents;
use App\Models\UI\UserTypes;
use Session;

class AdminController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('guest:super_admin')->except('dashboard');
         if(Auth::guard('super_admin')->check()){
            $this->middleware('guest:super_admin');
        }elseif(Auth::guard('teacher')->check()){
            $this->middleware('guest:teacher');
        }

    }

    public function dashboard(){

        $title = "Admin Dashboard";
        $UserId = Session::get('TeacherId');
        $Courses = Courses::get();

        $Groups = Groups::count();

        $Uploads = Uploads::count();

        $Conference = Conference::count();

        $PaymentCollection = PaymentCollection::count();
        $PaymentTransferred = PaymentTransferred::count();

        return view('UI.admin.dashboard', compact('title', 'Courses', 'Groups', 'Uploads', 'Conference', 'PaymentCollection', 'PaymentTransferred'));
    }

    public function classes_list(){

        $title = "My leads";
        // $Courses = Courses::where('id', $id)->first();
        $UserId = Session::get('TeacherId');

        if(Auth::guard('super_admin')->check()){
            $Classes = Classes::select('classes.*', 'courses.title','users.first_name as Fname', 'users.last_name as Lname')->join('courses', 'courses.id', '=', 'classes.course_id')->join('users', 'users.id', '=', 'classes.teacher_id')->orderBy('created_at', 'DESC')->get();
        }else{
            $Classes = Classes::select('classes.*', 'courses.title','users.first_name as Fname', 'users.last_name as Lname')->join('courses', 'courses.id', '=', 'classes.course_id')->join('users', 'users.id', '=', 'classes.teacher_id')
            ->where('classes.teacher_id', $UserId)
            ->orderBy('created_at', 'DESC')->get();
        }


        return view('UI.admin.leads.demo_classes_list', compact('title', 'Classes'));
    }

    public function inquiries_list(){

        $title = "Inquiries List";
        // $Courses = Courses::where('id', $id)->first();
        // $Inquiry = Inquiry::orderBy('created_at', 'DESC')->get();

        if(Auth::guard('super_admin')->check()){
            $Inquiry = RequestCallBack::orderBy('created_at', 'DESC')->get();
        }else{
            $Inquiry = RequestCallBack::orderBy('created_at', 'DESC')->get();
        }


        return view('UI.admin.leads.inquiries_list', compact('title', 'Inquiry'));
    }

    public function partners_list(){

        $title = "Partners List";
        // $Courses = Courses::where('id', $id)->first();
        $Partners = Partners::orderBy('created_at', 'DESC')->get();


        return view('UI.admin.leads.partners_list', compact('title', 'Partners'));
    }

    public function all_users_list(){

        $title = "Users List";
        // $Courses = Courses::where('id', $id)->first();
        $Users = Users::get();
        $UserTypes = UserTypes::get();

        return view('UI.admin.users.users_list', compact('title', 'Users', 'UserTypes'));
    }

    public function teachers_list(){

        $title = "Teachers List";
        // $Courses = Courses::where('id', $id)->first();
        $Users = Users::where('user_type', 2)
                                    ->get();


        return view('UI.admin.teachers.list', compact('title', 'Users'));
    }

    public function affiliate_list(){

        $title = "Affiliate List";
        // $Courses = Courses::where('id', $id)->first();
        $Users = Users::where('user_type', 4)
                                    ->get();


        return view('UI.admin.affiliate.list', compact('title', 'Users'));
    }

    public function teachers_details($id){

        $title = "Teachers Details";
        // $Courses = Courses::where('id', $id)->first();
        $Users = Users::where('users.id', $id)
                ->join('teacher_personal_details', 'teacher_personal_details.teacher_id', '=', 'users.id')
                                    ->first();
        $UserTypes = UserTypes::get();
        // echo json_encode($Users);
        // exit;

        return view('UI.admin.teachers.teacher_details', compact('title', 'Users', 'UserTypes'));
    }

    public function students_list(){

        $title = "Students List";
        // $Courses = Courses::where('id', $id)->first();
        $Users = Users::where('user_type', 3)
                                    ->get();


        return view('UI.admin.students.list', compact('title', 'Users'));
    }

    public function student_details($id){

        $title = "Student Details";
        // $Courses = Courses::where('id', $id)->first();
        $Users = Users::where('users.id', $id)
                ->join('students_profile', 'students_profile.student_id', '=', 'users.id')
                                    ->first();

        // echo json_encode($Users);
        // exit;

        return view('UI.admin.students.student_details', compact('title', 'Users'));
    }

    public function add_teachers($id){

        $title = "Add Teachers";
        $Users = Users::where('id', $id)->first();
        $TeachersList = Users::where('user_type', 2)->orderBy('created_at', 'DESC')->get();

        return view('UI.admin.students.add_teachers', compact('title', 'TeachersList', 'Users'));
    }

    public function store_teachers_list(Request $request){

        $TeacherId = $request->teacher_id;

        $AddTeachersToStudents = new AddTeachersToStudents();

        $AddTeachersToStudents->teacher_id = $TeacherId;
        $AddTeachersToStudents->student_id = $request->student_id;

        $AddTeachersToStudents->save();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> "Teacher Added Successfully"
                        )
        );
    }

    public function delete_teachers_list(Request $request)
    {
    	// \Log::info($request->all());

        $TeacherId = $request->teacher_id;

        // echo $request->student_id;
        // exit;

        $AddTeachersToStudents = AddTeachersToStudents::where('teacher_id', $TeacherId)
                        ->where('student_id', $request->student_id)->delete();

        return response()->json(
                        array(
                            "error"=>FALSE,
                            "message"=> "Teacher Removed Successfully"
                        )
        );
    }

    public function delete_users_list($id)
    {
    	// \Log::info($request->all());
        $Users = Users::where('id', $id)->delete();

        return redirect()->back()->with('message','User Deleted Successfully');
    }

    public function ChangeStatus(Request $request)
    {
    	// \Log::info($request->all());
        $user = Users::find($request->id);
        $user->status = $request->status;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }


    public function ChangeUserType(Request $request)
    {
        // \Log::info($request->all());
        // echo $request->UserId;
        // exit;
        $UserTypes = UserTypes::where('id', $request->UserTypeId)->first();
        $user = Users::find($request->UserId);
        $user->user_type = $request->UserTypeId;
        $user->user_main_type = $UserTypes->roles;
        $user->save();

        return response()->json(['success'=>'Status change successfully.']);
    }
}
