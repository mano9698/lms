<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherFees extends Model
{
    use HasFactory;

    protected $table = 'teacher_fees';

    protected $fillable = ['teacher_id','grade', 'subject', 'hourly_rate', 'monthly_rate', 'yearly_rate'];
}
