<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TotalVideoViews extends Model
{
    use HasFactory;

    protected $table = 'total_video_views';

    protected $fillable = ['user_id','video_id'];
}
