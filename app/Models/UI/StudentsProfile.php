<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentsProfile extends Model
{
    use HasFactory;

    protected $table = 'students_profile';

    protected $fillable = ['student_id','profile_pic', 'gender', 'dob', 'age', 'grade', 'school_clg_name', 'school_board', 'language_known', 'fav_subject', 'fav_sport', 'fav_music'];
}
