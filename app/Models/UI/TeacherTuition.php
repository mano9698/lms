<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherTuition extends Model
{
    use HasFactory;

    protected $table = 'teacher_tuition';

    protected $fillable = ['teacher_id','days', 'time_option1', 'time_option2', 'time_option3', 'time_option4', 'time_option5'];
}
