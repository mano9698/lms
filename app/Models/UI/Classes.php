<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    use HasFactory;

    protected $table = 'classes';

    protected $fillable = ['teacher_id', 'course_id', 'first_name', 'last_name', 'email', 'contact', 'country', 'state', 'city'];
}
