<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConferenceStudentsList extends Model
{
    use HasFactory;
    protected $table = 'conference_students_list';

    protected $fillable = ['conference_id','student_id'];
}
