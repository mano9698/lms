<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediaGroupStudentsList extends Model
{
    use HasFactory;

    protected $table = 'media_group_students_list';

    protected $fillable = ['media_group_id','student_id'];
}
