<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MediaStudentsList extends Model
{
    use HasFactory;

    protected $table = 'media_students_list';

    protected $fillable = ['media_id','student_id'];
}
