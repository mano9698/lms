<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSubPermissions extends Model
{
    use HasFactory;

    protected $table = 'user_sub_permissions';

    protected $fillable = ['user_permission_id', 'view', 'add', 'edit', 'delete'];
}
