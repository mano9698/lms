<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    use HasFactory;

    protected $table = 'courses';

    protected $fillable = ['teacher_id','title', 'category', 'course_image','course_time', 'location', 'price', 'course_description', 'course_date', 'duration', 'who_should_join', 'status'];
}
