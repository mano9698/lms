<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForumLikes extends Model
{
    use HasFactory;

    protected $table = 'forum_likes';

    protected $fillable = ['question_id', 'ip_address'];
}
