<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeachersPersonalDetails extends Model
{
    use HasFactory;

    protected $table = 'teacher_personal_details';

    protected $fillable = ['teacher_id','profile_pic', 'gender', 'qualification', 'tutor_type', 'teaching_subjects', 'teaching_language', 'no_of_student_present', 'website_link', 'instagram_link', 'facebook_link', 'youtube_channel', 'school_clg_boards', 'student_grades_studying', 'student_age', 'logo', 'total_no_of_students', 'about_me', 'status'];
}
