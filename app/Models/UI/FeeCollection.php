<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FeeCollection extends Model
{
    use HasFactory;

    protected $table = 'fees_collection';

    protected $fillable = ['teacher_id', 'student_id', 'amount_received', 'date_of_receipt', 'mode_of_receipt', 'received_towards'];
}
