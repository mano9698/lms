<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentCollection extends Model
{
    use HasFactory;

    protected $table = 'payment_collection';

    protected $fillable = ['first_name', 'last_name', 'email', 'contact', 'course_name', 'amount_collected', 'collection_mode', 'collection_ref_no', 'collection_date', 'affiliate', 'status'];
}
