<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    use HasFactory;

    protected $table = 'users';

    protected $fillable = ['first_name','last_name', 'email', 'mobile', 'password', 'user_type', 'user_main_type', 'country', 'state', 'city', 'area', 'pincode', 'status'];
}
