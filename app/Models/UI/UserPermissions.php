<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPermissions extends Model
{
    use HasFactory;

    protected $table = 'user_permissions';

    protected $fillable = ['user_type_id', 'permissions','view', 'add', 'edit', 'delete'];

}
