<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TotalFileDownloads extends Model
{
    use HasFactory;

    protected $table = 'total_file_downloads';

    protected $fillable = ['user_id','file_id'];
}
