<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddTeachersToStudents extends Model
{
    use HasFactory;

    protected $table = 'add_teachers_to_students';

    protected $fillable = ['teacher_id','student_id'];
}
