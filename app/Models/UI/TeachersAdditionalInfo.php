<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeachersAdditionalInfo extends Model
{
    use HasFactory;

    protected $table = 'teachers_additional_info';

    protected $fillable = ['teacher_id','first_name', 'last_name', 'gender', 'experience', 'subjects'];
}
