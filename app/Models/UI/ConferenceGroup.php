<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConferenceGroup extends Model
{
    use HasFactory;

    protected $table = 'conference_group_students_list';

    protected $fillable = ['group_id','conference_id'];
}
