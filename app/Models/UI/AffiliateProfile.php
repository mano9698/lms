<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AffiliateProfile extends Model
{
    use HasFactory;

    protected $table = 'affiliate_profile';

    protected $fillable = ['affiliate_id','profile_pic','gender', 'qualification'];
}
