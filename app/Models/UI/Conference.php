<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conference extends Model
{
    use HasFactory;

    protected $table = 'conference';

    protected $fillable = ['teacher_id','name', 'date', 'duration', 'start_time', 'description', 'meeting_link', 'status'];
}
