<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HomeWork extends Model
{
    use HasFactory;

    protected $table = 'home_work';

    protected $fillable = ['student_id', 'teacher_id', 'file', 'file_name'];
}
