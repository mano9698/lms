<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RequestCallBack extends Model
{
    use HasFactory;

    protected $table = 'request_call_back';

    protected $fillable = ['course_name', 'requirement', 'first_name', 'last_name', 'email', 'contact', 'country', 'state', 'city'];
}
