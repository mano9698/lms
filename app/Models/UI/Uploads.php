<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Uploads extends Model
{
    use HasFactory;

    protected $table = 'uploads';

    protected $fillable = ['user_id','file_type','file_name', 'files', 'status'];
}
