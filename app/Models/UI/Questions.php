<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Questions extends Model
{
    use HasFactory;

    protected $table = 'questions';

    protected $fillable = ['teacher_id', 'topic', 'slug', 'subject', 'feature_img', 'tags', 'details_answer', 'status'];
}
