<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentsList extends Model
{
    use HasFactory;

    protected $table = 'students_list';

    protected $fillable = ['course_id','student_id'];
}
