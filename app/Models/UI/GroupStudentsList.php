<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GroupStudentsList extends Model
{
    use HasFactory;

    protected $table = 'group_students_list';

    protected $fillable = ['group_id','student_id'];
}
