<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Certificates extends Model
{
    use HasFactory;
    protected $table = 'certificate';

    protected $fillable = ['teacher_id', 'title', 'award', 'first_name', 'last_name', 'partner_logo', 'description', 'certificate_id', 'certificate_no', 'coaching_partner_name', 'status'];
}
