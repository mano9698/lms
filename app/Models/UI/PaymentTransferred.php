<?php

namespace App\Models\UI;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentTransferred extends Model
{
    use HasFactory;

    protected $table = 'payment_transferred';

    protected $fillable = ['affiliate', 'commision_month', 'total_collection', 'commission_due_date', 'commission_paid_amount', 'paid_date', 'payment_mode', 'payment_ref_no', 'status'];
}
