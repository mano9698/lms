<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentTransferredTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_transferred', function (Blueprint $table) {
            $table->id();
            $table->string('affiliate');
            $table->string('commision_month');
            $table->string('total_collection');
            $table->string('commission_due_date');
            $table->string('commission_paid_amount');
            $table->string('paid_date');
            $table->string('payment_mode');
            $table->string('payment_ref_no');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_transferred');
    }
}
