<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherTuitionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_tuition', function (Blueprint $table) {
            $table->id();
            $table->string('teacher_id');
            $table->string('days');
            $table->string('time_option1');
            $table->string('time_option2');
            $table->string('time_option3');
            $table->string('time_option4');
            $table->string('time_option5');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_tuition');
    }
}
