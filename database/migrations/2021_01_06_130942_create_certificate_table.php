<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCertificateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certificate', function (Blueprint $table) {
            $table->id();
            $table->string('teacher_id');
            $table->string('title');
            $table->string('award');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('partner_logo');
            $table->string('description');
            $table->string('certificate_id');
            $table->string('certificate_no');
            $table->string('coaching_partner_name');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certificate');
    }
}
