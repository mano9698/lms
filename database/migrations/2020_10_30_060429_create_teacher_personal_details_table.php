<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherPersonalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_personal_details', function (Blueprint $table) {
            $table->id();
            $table->string('teacher_id');
            $table->string('profile_pic');
            $table->string('gender');
            $table->string('qualification');
            $table->string('tutor_type');
            $table->string('teaching_subjects');
            $table->string('teaching_language');
            $table->string('no_of_student_present');
            $table->string('website_link');
            $table->string('instagram_link');
            $table->string('facebook_link');
            $table->string('youtube_channel');
            $table->string('school_clg_boards');
            $table->string('student_grades_studying');
            $table->string('student_age');
            $table->string('logo');
            $table->string('total_no_of_students');
            $table->longText('about_me');
            $table->boolean('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_personal_details');
    }
}
