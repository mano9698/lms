<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeesCollectionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fees_collection', function (Blueprint $table) {
            $table->id();
            $table->string('teacher_id');
            $table->string('student_id');
            $table->string('amount_received');
            $table->string('date_of_receipt');
            $table->string('mode_of_receipt');
            $table->string('received_towards');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fees_collection');
    }
}
