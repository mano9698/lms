<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_fees', function (Blueprint $table) {
            $table->id();
            $table->string('teacher_id');
            $table->string('grade');
            $table->string('subject');
            $table->string('hourly_rate');
            $table->string('monthly_rate');
            $table->string('yearly_rate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_fees');
    }
}
