<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsProfileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students_profile', function (Blueprint $table) {
            $table->id();
            $table->string('student_id');
            $table->string('profile_pic');
            $table->string('gender');
            $table->string('dob');
            $table->string('age');
            $table->string('grade');
            $table->string('school_clg_name');
            $table->string('school_board');
            $table->string('language_known');
            $table->string('fav_subject');
            $table->string('fav_sport');
            $table->string('fav_music');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students_profile');
    }
}
