$.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});


function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#img_preview').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }
  
  $("#ChangeImg").change(function() {
    readURL(this);
  });


  $('.Add_Students').click(function(){
    var student_id = $(this).data('id'); 
    var course_id = $('#course_id').val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/courses/store_students_list',
        data: {'student_id': student_id, 'course_id': course_id},
        success: function(data){
            alert(data.message);
            location.reload();
        }
    });

  });

  $('.Remove_Students').click(function(){
    var student_id = $(this).data('id'); 
    var course_id = $('#course_id').val();
        
    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/courses/delete_students_list',
        data: {'student_id': student_id, 'course_id': course_id},
        success: function(data){
            alert(data.message);
            location.reload();
        }
    });

  });



// Group Students
  
  $('.Add_Group_Students').click(function(){
    var student_id = $(this).data('id'); 
    var group_id = $('#group_id').val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/groups/store_students_list',
        data: {'student_id': student_id, 'group_id': group_id},
        success: function(data){
            alert(data.message);
            location.reload();
        }
    });

  });

  $('.Remove_Group_Students').click(function(){
    var student_id = $(this).data('id'); 
    var group_id = $('#group_id').val();
        
    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/groups/delete_students_list',
        data: {'student_id': student_id, 'group_id': group_id},
        success: function(data){
            alert(data.message);
            location.reload();
        }
    });

  });



  // Media Students
  
  $('.Add_Media_Students').click(function(){
    var student_id = $(this).data('id'); 
    var media_id = $('#media_id').val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/teachers/uploads/store_students_list',
        data: {'student_id': student_id, 'media_id': media_id},
        success: function(data){
            alert(data.message);
            location.reload();
        }
    });

  });

  $('.Remove_Media_Students').click(function(){
    var student_id = $(this).data('id'); 
    var media_id = $('#media_id').val();
        
    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/teachers/uploads/delete_students_list',
        data: {'student_id': student_id, 'media_id': media_id},
        success: function(data){
            alert(data.message);
            location.reload();
        }
    });

  });

// Media Group Students

  $('#Add_Group_Uploads').click(function(){
    var group_id = $('#group_id').val();
    var media_id = $('#media_id').val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/teachers/uploads/update_group_of_students',
        data: {'group_id': group_id, 'media_id': media_id},
        success: function(data){
            alert(data.message);
            location.reload();
        }
    });

  });

//   $('.delete_students').click(function(){
//     var answer = confirm ("Do you want to delete?");
//     if (answer)
//     {
//         var id = $(this).data('id'); 
        
//         $.ajax({
//             type: "POST",
//             dataType: "json",
//             url: '/courses/delete_students_list',
//             data: {'id': id},
//             success: function(data){
//             console.log(data.success)
//                 alert(data.success);
//                 location.reload();
//             }
//         });
//     }
// });



// Conference Students
  
$('.Add_Conference_Students').click(function(){
  var student_id = $(this).data('id'); 
  var conference_id = $('#conference_id').val();

  $.ajax({
      type: "POST",
      dataType: "json",
      url: '/teachers/conference/store_students_list',
      data: {'student_id': student_id, 'conference_id': conference_id},
      success: function(data){
          alert(data.message);
          location.reload();
      }
  });

});

$('.Remove_Conference_Students').click(function(){
  var student_id = $(this).data('id'); 
  var conference_id = $('#conference_id').val();
      
  $.ajax({
      type: "POST",
      dataType: "json",
      url: '/teachers/conference/delete_students_list',
      data: {'student_id': student_id, 'conference_id': conference_id},
      success: function(data){
          alert(data.message);
          location.reload();
      }
  });

});



// Delete Course
$('.delete_course').click(function(){

  var answer = confirm ("Do you want to delete? That Students data's also will be deleted!");
  if (answer)
  {
      var id = $(this).data('id'); 
      
      $.ajax({
          type: "POST",
          dataType: "json",
          url: '/courses/delete_course',
          data: {'id': id},
          success: function(data){
          console.log(data.message)
              alert(data.message);
              location.reload();
          }
      });
  }
});


// Delete Groups
$('.delete_groups').click(function(){

  var answer = confirm ("Do you want to delete? That Students data's also will be deleted!");
  if (answer)
  {
      var id = $(this).data('id'); 
      
      $.ajax({
          type: "POST",
          dataType: "json",
          url: '/groups/delete_groups',
          data: {'id': id},
          success: function(data){
          console.log(data.message)
              alert(data.message);
              location.reload();
          }
      });
  }
});


// Delete Uploads
$('.delete_uploads').click(function(){

  var answer = confirm ("Do you want to delete? That Students data's also will be deleted!");
  if (answer)
  {
      var id = $(this).data('id'); 
      
      $.ajax({
          type: "POST",
          dataType: "json",
          url: '/teachers/uploads/delete_uploads',
          data: {'id': id},
          success: function(data){
          console.log(data.message)
              alert(data.message);
              location.reload();
          }
      });
  }
});



// Delete Conference
$('.delete_conference').click(function(){

  var answer = confirm ("Do you want to delete? That Students data's also will be deleted!");
  if (answer)
  {
      var id = $(this).data('id'); 
      
      $.ajax({
          type: "POST",
          dataType: "json",
          url: '/teachers/conference/delete_conference',
          data: {'id': id},
          success: function(data){
          console.log(data.message)
              alert(data.message);
              location.reload();
          }
      });
  }
});



// Delete Teacher Fees
$('.delete_fees').click(function(){

  var answer = confirm ("Do you want to delete?");
  if (answer)
  {
      var id = $(this).data('id'); 
      
      $.ajax({
          type: "POST",
          dataType: "json",
          url: '/teachers/delete_fees',
          data: {'id': id},
          success: function(data){
          console.log(data.message)
              alert(data.message);
              location.reload();
          }
      });
  }
});


// Delete Teacher Tuition
$('.delete_tuition').click(function(){

  var answer = confirm ("Do you want to delete?");
  if (answer)
  {
      var id = $(this).data('id'); 
      
      $.ajax({
          type: "POST",
          dataType: "json",
          url: '/teachers/delete_tuition',
          data: {'id': id},
          success: function(data){
          console.log(data.message)
              alert(data.message);
              location.reload();
          }
      });
  }
});


// Delete Teacher Info
$('.delete_info').click(function(){

  var answer = confirm ("Do you want to delete?");
  if (answer)
  {
      var id = $(this).data('id'); 
      
      $.ajax({
          type: "POST",
          dataType: "json",
          url: '/teachers/delete_info',
          data: {'id': id},
          success: function(data){
          console.log(data.message)
              alert(data.message);
              location.reload();
          }
      });
  }
});



// Conference Group Students

$('#Add_Group_Conference').click(function(){
  var group_id = $('#group_id').val();
  var conference_id = $('#conference_id').val();

  $.ajax({
      type: "POST",
      dataType: "json",
      url: '/teachers/conference/update_group_of_students',
      data: {'group_id': group_id, 'conference_id': conference_id},
      success: function(data){
          alert(data.message);
          location.reload();
      }
  });

});





// Add Teachers

$('.Add_Teachers').click(function(){
  var teacher_id = $(this).data('id'); 
  var student_id = $('#student_id').val();

  $.ajax({
      type: "POST",
      dataType: "json",
      url: '/admin/store_teachers_list',
      data: {'student_id': student_id, 'teacher_id': teacher_id},
      success: function(data){
          alert(data.message);
          location.reload();
      }
  });

});

$('.Remove_Teachers').click(function(){
  var teacher_id = $(this).data('id'); 
  var student_id = $('#student_id').val();
      
  $.ajax({
      type: "POST",
      dataType: "json",
      url: '/admin/delete_teachers_list',
      data: {'student_id': student_id, 'teacher_id': teacher_id},
      success: function(data){
          alert(data.message);
          location.reload();
      }
  });

});




// Add File Downloads

$('.AddFileDownloads').click(function(){
  var file_id = $(this).data('id'); 
      
  $.ajax({
      type: "POST",
      dataType: "json",
      url: '/students/file_download',
      data: {file_id: file_id},
      success: function(data){
          // alert(data.message);
          // location.reload();
      }
  });

});


  $(document).ready(function(){
  $("#ChangeImg").cropzee({startSize: [85, 85, '%'],});
  
  $("#AddChangeImg").cropzee({startSize: [85, 85, '%'],});

  $("#profile_pic").cropzee({startSize: [85, 85, '%'],});

  $("#logo").cropzee({startSize: [85, 85, '%'],});

  $("#student_profile_pic").cropzee({startSize: [85, 85, '%'],});
    
  });